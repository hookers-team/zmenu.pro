//
//  AppCoordinator.swift
//  ZMenu
//
//  Created by Kirill Sokolov on 24.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit
import Alamofire

final class AppCoordinator: Coordinator {

    private(set) unowned var window: UIWindow
    private var root: UINavigationController!
    
    // coordinators
    private var tabBarCoordinator: TabBarCoordinator!
    private var employeeCoordinator: EmployeeCoordinator!
    private var enterCoordinator: EnterCoordinator?
    private var deviceToken: String?
    
    private(set) var remoteNotificationHandler: RemoteNotificationHandler?
    private(set) var userNotificationCenterHelper = UserNotificationCenterHelper()
    
    private var userSessionService = UserSessionService()
    
    init(window: UIWindow) {
        self.window = window
        
        let networkService = HTTPNetworkService(baseURL: NetworkConstants.devServerBaseURL, requestExecutor: NetworkRequestExecutor())
        
        let locationPermissionService = LocationPermissionService()
        let notificationService = NotificationService()
        let cameraService = CameraService()
        let photoLibraryService = PhotoLibraryService()
        
        let permissionService = PermissionService(locationService: locationPermissionService, notificationService: notificationService, cameraService: cameraService, photoLibraryService: photoLibraryService)
        
        super.init(context: CoordinatingContext(dispatcher: DefaultDispatcher(), styleguide: DarkThemeDesignStyleGuide(), networkService: networkService, permissionService: permissionService))
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        prepareForStart()
        
        if let launchOptions = launchOptions,
            let userInfo = launchOptions[UIApplication.LaunchOptionsKey.remoteNotification] as? [String : Any] {
            
            remoteNotificationHandler?.handleRemoteNotification(userInfo)
        }
        
        return true
    }
    
    override func prepareForStart() {
        super.prepareForStart()
        
        remoteNotificationHandler = RemoteNotificationHandler(dispatcher: context.dispatcher)
        
        register()
        
        //KS: TODO: Maybe need to use different userId, for separeting user information.
        let userDefaultStorage = ZMenuStorageFactory.makeUserDefaultsStorage(forUserId: "DefaultUser")
        
        userSessionService.userDefaultStorage = userDefaultStorage
        
        if let sessionModel = userSessionService.accountModel {
            updateNetworkService(for: sessionModel)
            setupRootViewController()
        } else {
            setupEnterViewController()
        }
    }

    private func setupEnterViewController() {
        enterCoordinator = EnterCoordinator(context: context)
        enterCoordinator?.delegate = self
        enterCoordinator?.prepareForStart()
        
        let rootViewController = enterCoordinator?.createFlow()

        window.rootViewController = rootViewController
    }
    
    private func setupRootViewController() {
        employeeCoordinator = EmployeeCoordinator(context: context)
        employeeCoordinator.userSessionService = userSessionService
        employeeCoordinator.prepareForStart()
        
        if let deviceToken = deviceToken {
            employeeCoordinator.deviceTokenUpdate(deviceToken: deviceToken)
        }
        
        window.rootViewController = employeeCoordinator.createFlow()
    }
    
    private func updateNetworkService(for user: SessionModel) {
        let userRequestParametrs = UserRequestParametrs(userId: user.userId, token: user.token)
        
        context.networkService = HTTPNetworkService(baseURL: NetworkConstants.devServerBaseURL, requestExecutor: NetworkRequestExecutor(), parameterProviders: [userRequestParametrs], additionalURLParameterProviders: [userRequestParametrs])
    }
    
    func deviceTokenUpdate(deviceToken: String) {
        if employeeCoordinator == nil {
            self.deviceToken = deviceToken
        } else {
            employeeCoordinator.deviceTokenUpdate(deviceToken: deviceToken)
        }
    }
    
}

extension AppCoordinator: EnterCoordinatorDelegate {
    
    func enterCoordintator(_ enterCoordinator: EnterCoordinator, didFinishAccountAuthentification accountModel: SessionModel) {
        updateNetworkService(for: accountModel)
        
        userSessionService.saveSessionModel(accountModel)
        
        self.enterCoordinator = nil
        
        setupRootViewController()
    }
    
}

extension AppCoordinator {
    
    func register() {
        registerShowingSusscesAlert()
        registerShowingErrorAlert()
        registerLogout()
    }
    
    func registerShowingSusscesAlert() {
        context.dispatcher.register(type: AlertEvent.ShowSuccessAlert.self) { result, _ in
            switch result {
            case .success(let box):
                if let topVC = UIApplication.topViewController() {
                    topVC.presentSuccess(message: box.message)
                }
            case .failure(_):
                break
            }
        }
    }
    
    func registerShowingErrorAlert() {
        context.dispatcher.register(type: AlertEvent.ShowErrorAlert.self) { result, _ in
            
            switch result {
            case .success(let box):
                if let topVC = UIApplication.topViewController() {
                    topVC.presentError(message: box.message)
                }
            case .failure(_):
                break
            }
        }
    }
    
    func registerLogout() {
        context.dispatcher.register(type: UserEvent.System.Logout.self) { [weak self] result, _ in
            
            switch result {
            case .success(_):
                self?.userSessionService.removeSession()
                self?.setupEnterViewController()
                self?.employeeCoordinator = nil
                self?.tabBarCoordinator = nil
            case .failure(_):
                break
            }
        }
    }
    
}

