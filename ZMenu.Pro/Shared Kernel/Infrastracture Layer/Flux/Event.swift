//
//  Event.swift
//  ZMenu.Pro
//
//  Created by Sokolov Kirill on 12/7/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import Foundation

protocol Event {
    
    associatedtype Payload
//    associatedtype Error
    
}
