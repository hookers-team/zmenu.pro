//
//  ResponseMappingError.swift
//  ZMenu.Pro
//
//  Created by Sokolov Kirill on 5/29/17.
//  Copyright © 2017 ZMenu.Pro. All rights reserved.
//

import Foundation

enum ResponseMappingError: Error {
    
    case invalid
    
}
