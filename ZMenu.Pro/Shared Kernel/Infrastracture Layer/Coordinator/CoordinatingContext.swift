//
//  CoordinatingContext.swift
//  ZMenu.Pro
//
//  Created by Sokolov Kirill on 5/2/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import Foundation

final class CoordinatingContext {
    
    let dispatcher: Dispatcher
    var dataBaseService: CoreDataDBService?
    let styleguide: DesignStyleGuide
    var networkService: HTTPNetworkService
    let permissionService: PermissionService
    
    
    init(dispatcher: Dispatcher, styleguide: DesignStyleGuide, networkService: HTTPNetworkService, permissionService: PermissionService) {
        self.styleguide = styleguide
        self.dispatcher = dispatcher
        self.networkService = networkService
        self.permissionService = permissionService
    }
    
}
