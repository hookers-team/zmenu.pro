//
//  RemoteNotificationHandler.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 06.03.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol DeepLinkScreenOpener: class {
    
//    func handleRemoteNotification(_ remoteNotificationHandler: RemoteNotificationHandler, handleMessageNotification networkMsgPush: MsgData)
    
}

public class RemoteNotificationHandler: NSObject {
    
    private let dispatcher: Dispatcher
    
    // MARK: - Public function
    
    init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }
    
    @objc public func handleRemoteNotification(_ userInfo: [String : Any]) {
        
        guard let userInfoData = userInfo[NotificationConstants.Keys.data] as? [String : Any],
            let action = userInfoData[NotificationConstants.Keys.action] as? String
             else { return } //let networkMsgPush = makeNetworkMsgPush(data: data)
        
        let isKnownType = isRemoteNotificationOfKnownAction(action) // check if push is of known type
        //let isAuthorizedUser = UserSession.userIsAuthorized() // no need to display push is user isn't loginned

        if isKnownType { //isAuthorizedUser &&
            if let data = try? JSONSerialization.data(withJSONObject: userInfoData, options: []), let notificationResponse = try? JSONDecoder().decode(RemoteNotificationResponse.self, from: data) {
                switch notificationResponse.action {
                case .orderInfo:
                    let value = OrdersEvent.NavigationEvent.OrderInfo.Value(workPlace: notificationResponse.workPlace)
                    
                    dispatcher.dispatch(type: OrdersEvent.NavigationEvent.OrderInfo.self, result: Result(value: value))
                }
            }
        }
    }

    // MARK: - Private functions

    fileprivate func isRemoteNotificationOfKnownAction(_ action: String) -> Bool {
        switch action {
        case NotificationType.orderInfo.rawValue:
            return true
        default:
            return false
        }
    }

}
