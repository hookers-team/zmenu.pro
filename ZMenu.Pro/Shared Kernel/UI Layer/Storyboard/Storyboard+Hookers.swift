//
//  Storyboard+ZMenu.swift
//  ZMenu
//
//  Created by Kirill Sokolov on 24.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

import UIKit

fileprivate enum ZMenuStoryboardControllerId: String {
    
    case restaurant = "RestaurantStoryboardControllerId"
    
}


