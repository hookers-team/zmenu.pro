//
//  CALayer.swift
//  ZMenu.Pro
//
//  Created by Sokolov Kirill on 4/16/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import UIKit

extension CALayer {
    
    func applyShadow(color: UIColor = .black, alpha: Float = 0.1, x: CGFloat = 0, y: CGFloat = 3, blur: CGFloat = 30,
                     spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
}

extension CALayer {
    
    func applyDash( with color: UIColor) {
        let shapelayer = CAShapeLayer()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: self.bounds.origin.x, y: self.bounds.origin.y))
        path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        
        shapelayer.strokeColor = color.cgColor
        shapelayer.lineWidth = 1.0 / UIScreen.main.scale
        shapelayer.lineJoin = CAShapeLayerLineJoin.miter
        shapelayer.lineDashPattern = [6,3]
        //[NSNumber(value: Double(1.0 / UIScreen.main.scale)), 3]
        shapelayer.path = path.cgPath
        addSublayer(shapelayer)
    }
    
    func applySketchShadow(color: UIColor = .black, alpha: Float = 0.5, x: CGFloat = 0, y: CGFloat = 2, blur: CGFloat = 4, spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
}

