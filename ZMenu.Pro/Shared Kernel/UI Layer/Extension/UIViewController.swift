//
//  UIViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 05.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

protocol StatusPresenting {
    
    func presentError(message: String)
    func presentError(withTitle title: String, subtitle: String)
    
    func presentSuccess(message: String)
    func presentSuccess(withTitle title: String, subtitle: String)
    
    func presentStatus(message: String)
    func presentStatus(withTitle title: String, subtitle: String)
    
}

extension UIViewController: StatusPresenting {
    
    func presentError(message: String) {
        ApplicationAlert().show(with: .error, title: nil, subtitle: message)
    }
    
    func presentError(withTitle title: String, subtitle: String) {
        ApplicationAlert().show(with: .error, title: title, subtitle: subtitle)
    }
    
    func presentSuccess(message: String) {
        ApplicationAlert().show(with: .success, title: nil, subtitle: message)
    }
    
    func presentSuccess(withTitle title: String, subtitle: String) {
        ApplicationAlert().show(with: .success, title: title, subtitle: subtitle)
    }
    
    func presentStatus(message: String) {
        ApplicationAlert().show(with: .info, title: nil, subtitle: message)
    }
    
    func presentStatus(withTitle title: String, subtitle: String) {
        ApplicationAlert().show(with: .info, title: title, subtitle: subtitle)
    }
    
}



extension UIViewController {
    
    var isModal: Bool {
        return presentingViewController != nil ||
            navigationController?.presentingViewController?.presentedViewController === navigationController ||
            tabBarController?.presentingViewController is UITabBarController
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}


extension UIViewController: SpinnerPresenting {
    
    func showSpinner(message: String? = nil, animated: Bool = true, blockUI: Bool = true) {
        ApplicationSpinner().show(on: view, text: message, animated: animated, blockUI: blockUI)
    }
    
    func hideSpinner(animated: Bool = true) {
        ApplicationSpinner().hide(from: view, animated: animated)
    }
    
}

extension UIViewController {
    
    var iPhoneModelXBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            guard isIPhoneModelX else { return 0 }
            
            return CGFloat(34)
        }
        
        return 0
    }
    
    var isIPhoneModelX: Bool {
        let deviceHeight = UIScreen.main.nativeBounds.height
        let iphoneXandXSHeight = CGFloat(2436)
        let iphoneXrHeight = CGFloat(1792)
        let iphoneXsMaxHeight = CGFloat(2688)
        let iPhoneModelXHeights = [iphoneXandXSHeight, iphoneXrHeight, iphoneXsMaxHeight]
        
        return iPhoneModelXHeights.contains(deviceHeight)
    }
    
    var tabBarVisibleHeight: CGFloat {
        if isIPhoneModelX {
            return tabBarController?.tabBar.isHidden == false ? bottomLayoutGuide.length : iPhoneModelXBottomInset
        } else {
            return tabBarController?.tabBar.isHidden == false ? bottomLayoutGuide.length : 0
        }
    }
    
}

extension UIViewController {
    
    func addObserverForNotification(_ notificationName: Notification.Name, actionBlock: @escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(forName: notificationName, object: nil, queue: OperationQueue.main, using: actionBlock)
    }
    
    func removeObserver(_ observer: AnyObject, notificationName: Notification.Name) {
        NotificationCenter.default.removeObserver(observer, name: notificationName, object: nil)
    }
}

//MARK: - Keyboard handling
extension UIViewController {
    
    typealias KeyboardHeightClosure = (CGFloat) -> ()
    
    func addKeyboardChangeFrameObserver(willShow willShowClosure: KeyboardHeightClosure?,
                                        willHide willHideClosure: KeyboardHeightClosure?) {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil, queue: OperationQueue.main, using: { [weak self](notification) in
                                                if let userInfo = notification.userInfo,
                                                    let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
                                                    let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double,
                                                    let c = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt,
                                                    let kFrame = self?.view.convert(frame, from: nil),
                                                    let kBounds = self?.view.bounds {
                                                    
                                                    let animationType = UIView.AnimationOptions(rawValue: c)
                                                    let kHeight = kFrame.size.height
                                                    UIView.animate(withDuration: duration, delay: 0, options: animationType, animations: {
                                                        if kBounds.intersects(kFrame) { // keyboard will be shown
                                                            willShowClosure?(kHeight)
                                                        } else { // keyboard will be hidden
                                                            willHideClosure?(kHeight)
                                                        }
                                                    }, completion: nil)
                                                } else {
                                                    print("Invalid conditions for UIKeyboardWillChangeFrameNotification")
                                                }
        })
    }
    
    func removeKeyboardObserver() {
        removeObserver(self, notificationName: UIResponder.keyboardWillChangeFrameNotification)
    }
    
    var isSpinerPresented: Bool {
        return !view.subviews.filter{$0 is MBProgressHUD}.isEmpty
    }
    
}

extension UIViewController {
    
    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
            textField.keyboardType = .default
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField = alert.textFields?.first, let text = textField.text, !text.isEmpty else {
                
                return
            }
            actionHandler?(text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
