//
//  DesignTheme.swift
//  ZMenu.Pro
//
//  Created by ZMenu.Pro on 4/25/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import Foundation

@objc public enum DesignTheme: Int {
    
    case light
    case dark
    
}
