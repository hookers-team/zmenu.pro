//
//  DesignStyleGuide.swift
//  ZMenu.Pro
//
//  Created by ZMenu.Pro on 4/25/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import Foundation
import UIKit

@objc
protocol DesignStyleGuide: ExternalStyleguide, AdditionalColorStyleGuide, AdditionalFontStyleGuide, StatusBarStyleGuide, KeyboardAppearanceStyleGuide { }

// KS: TODO: move methods of this protocol to common FontStyleGuide
@objc
protocol AdditionalFontStyleGuide {
    
    func semiboldFont(ofSize fontSize: CGFloat) -> UIFont
    
}

// KS: TODO: move methods of this protocol to common ColorStyleGuide
@objc
protocol AdditionalColorStyleGuide {
    
    var senderTextColor: UIColor { get }
    
}

@objc
protocol StatusBarStyleGuide {
    
    var statusBarStyle: UIStatusBarStyle { get }
    
}

@objc
protocol KeyboardAppearanceStyleGuide {
    
    var keyboardAppearance: UIKeyboardAppearance { get }
    
}

