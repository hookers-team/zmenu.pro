//
//  CTAContainerViewSupporting.swift
//  ZMenu.Pro
//
//  Created by Sokolov Kirill on 12/20/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import UIKit

protocol CTAContainerViewSupporting {
    
    var ctaContainerViewBottomInset: CGFloat { get }
    
}

extension CTAContainerViewSupporting where Self: UIViewController {
    
    var ctaContainerViewBottomInset: CGFloat {
        return tabBarVisibleHeight - CTAButtonContainerView.bottomInsetHiddenBehindSafeArea
    }
    
}
