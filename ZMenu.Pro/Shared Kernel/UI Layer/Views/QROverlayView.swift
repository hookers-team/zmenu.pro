//
//  QROverlayView.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 01.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation
import UIKit

final class QROverlayView: UIView {
    
    @IBInspectable
    var sizeMultiplier: CGFloat = 0.2 {
        didSet{
            draw(bounds)
        }
    }
    
    @IBInspectable
    var lineWidth: CGFloat = 5 {
        didSet{
            draw(bounds)
        }
    }
    
    @IBInspectable
    var lineColor: UIColor = UIColor.white.withAlphaComponent(0.7) {
        didSet{
            draw(bounds)
        }
    }
    
    override func draw(_ rect: CGRect) {
        let currentContext = UIGraphicsGetCurrentContext()
        
        currentContext?.setLineWidth(lineWidth)
        currentContext?.setStrokeColor(lineColor.cgColor)
        
        //top left corner
        currentContext?.beginPath()
        currentContext?.move(to: CGPoint(x: lineWidth, y: bounds.size.height * sizeMultiplier))
        currentContext?.addCurve(to: CGPoint(x: bounds.size.width * sizeMultiplier, y: lineWidth), control1: CGPoint(x: lineWidth, y: 0), control2: CGPoint(x: 0, y: lineWidth))
        currentContext?.strokePath()
        
        //top right corner
        currentContext?.beginPath()
        currentContext?.move(to: CGPoint(x: bounds.size.width - bounds.size.width * sizeMultiplier, y: lineWidth))
        
        currentContext?.addCurve(to: CGPoint(x: bounds.size.width - lineWidth, y: bounds.size.height * sizeMultiplier), control1: CGPoint(x: bounds.size.width, y: lineWidth), control2: CGPoint(x: bounds.size.width - lineWidth, y: 0))
        currentContext?.strokePath()
        
        //bottom right corner
        currentContext?.beginPath()
        currentContext?.move(to: CGPoint(x: bounds.size.width - lineWidth, y: bounds.size.height - bounds.size.height * sizeMultiplier))
        currentContext?.addCurve(to: CGPoint(x: bounds.size.width - bounds.size.width * sizeMultiplier, y: bounds.size.height - lineWidth), control1: CGPoint(x: bounds.size.width - lineWidth, y: bounds.size.height), control2: CGPoint(x: bounds.size.width, y: bounds.size.height - lineWidth))
        currentContext?.strokePath()
        
        //bottom left corner
        currentContext?.beginPath()
        currentContext?.move(to: CGPoint(x: bounds.size.width * sizeMultiplier, y: bounds.size.height - lineWidth))
        currentContext?.addCurve(to: CGPoint(x: lineWidth, y: bounds.size.height - bounds.size.height * sizeMultiplier), control1: CGPoint(x: 0, y: bounds.size.height - lineWidth), control2: CGPoint(x: lineWidth, y: bounds.size.height))
        currentContext?.strokePath()
    }
    
}
