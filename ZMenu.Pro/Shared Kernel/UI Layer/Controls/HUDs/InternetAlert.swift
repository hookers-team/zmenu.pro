//
//  InternetAlert.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 10/18/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import UIKit

enum InternetAlertStyle {
    
    case noConnection, connecting, connected
    
}

protocol InternetAlert {
    
    func show(on window: UIWindow,
              with style: InternetAlertStyle)
    
}
