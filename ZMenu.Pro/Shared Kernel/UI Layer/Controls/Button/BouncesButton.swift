//
//  BouncesButton.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 21.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

import UIKit

typealias AnimationCallBack = (() -> Void)

final class BouncesButton: UIButton {
    
    private struct Constants {
        
        static let transformationDuration: Double = 0.2
        static let transformationDefaultValue: CGFloat = 1
        static let transformationMaxValue: CGFloat = 1.3
        
    }
    
    var allowsAnimation: Bool = true
    var animationCallBack: AnimationCallBack?
    
    override var isHighlighted: Bool {
        set {
            if newValue != isHighlighted {
                super.isHighlighted = newValue
                performAnimation()
            }
        } get {
            return super.isHighlighted
        }
    }
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.adjustsImageWhenHighlighted = false
    }
    
    // MARK: - Animation
    
    func performAnimation() {
        if allowsAnimation == true && isHighlighted == true {
            allowsAnimation = false
            UIView.animate(
                withDuration: Constants.transformationDuration,
                animations: {
                    self.transform = CGAffineTransform(scaleX: Constants.transformationMaxValue, y: Constants.transformationMaxValue)
            }, completion: { _ in
                UIView.animate(
                    withDuration: Constants.transformationDuration,
                    animations: {
                        self.transform = CGAffineTransform(
                            scaleX: Constants.transformationDefaultValue,
                            y: Constants.transformationDefaultValue
                        )
                }, completion: { _ in
                    self.animationCallBack?()
                })
                self.allowsAnimation = true
            })
        }
    }
    
}
