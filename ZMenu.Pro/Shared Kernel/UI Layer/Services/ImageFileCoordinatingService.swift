//
//  ImageFileCoordinatingService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 18.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

import Foundation
import UIKit

enum ImageType: String {
    
    case original
    case preview
    
    var description : String {
        get {
            return self.rawValue
        }
    }
    
}

enum ImageDataError: Error {
    
    case invalidBitmapFormat
    
}

final class ImageFileCoordinatingService {
    
    @discardableResult
    func saveImageToDisk(_ image: UIImage, fileName: String, fileExtension: String, imageType: ImageType = .original, compressionQuality: CGFloat = 0.5) -> (Error?, URL?, Data?) {
        if let imageData = image.jpegData(compressionQuality: compressionQuality) {
            let fileName = "\(fileName)_\(imageType.description)"
            let result = FileManager.saveDataToDisk(imageData, fileName: fileName, fileExtension: fileExtension)
            
            return (result.0, result.1, imageData)
        }
        
        return (ImageDataError.invalidBitmapFormat, nil, nil)
    }
    
    @discardableResult
    func removeImageFromDisk(_ fileName: String, fileExtension: String, imageType: ImageType = .original) -> Error? {
        let fileName = "\(fileName)_\(imageType.description)"
        
        return FileManager.removeFileFromDisk(fileName, fileExtension: fileExtension)
    }
    
    func imageData(_ fileName: String, fileExtension: String, imageType: ImageType = .original) -> Data? {
        let fileName = "\(fileName)_\(imageType.description)"
        
        return FileManager.fileData(fileName, fileExtension: fileExtension)
    }
    
    func getImagePath(_ fileName: String, fileExtension: String, imageType: ImageType = .original) -> String {
        let fileName = "\(fileName)_\(imageType.description)"
        
        return FileManager.getFilePath(fileName, fileExtension: fileExtension)
    }
    
}
