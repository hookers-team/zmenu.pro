//
//  LogoutResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 04.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct SuccessMessageResponse: Decodable {
    
    struct Data: Decodable {
        
        let message: String
        
    }
    
    let data: Data
    
}
