//
//  AccountModel.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

enum AccountType: String {
    
    case owner
    case employee
    
}

final class SessionModel: NSObject, NSCoding, Decodable {
    
    let userId: String
    let token: String
    let type: AccountType.RawValue
    let name: String?
    let phone: String
    
    required init(coder decoder: NSCoder) {
        name = decoder.decodeObject(forKey: "name") as? String? ?? nil
        phone = decoder.decodeObject(forKey: "phone") as? String ?? ""
        userId = decoder.decodeObject(forKey: "userId") as? String ?? ""
        token = decoder.decodeObject(forKey: "token") as? String ?? ""
        type = decoder.decodeObject(forKey: "type") as? String ?? ""
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(token, forKey: "token")
    }
    
}
