//
//  GeneralNetworkErrorPresenting.swift
//  ZMenu.Pro
//
//  Created by Maxim Letushov on 6/6/17.
//  Copyright © 2017 ZMenu.Pro. All rights reserved.
//

import Foundation

protocol GeneralNetworkErrorPresenting {
    
    func presentGeneralNetworkError(_ error: GeneralNetworkPresentingError)
    
}
