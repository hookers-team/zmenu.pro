//
//  NetworkErrorHandling.swift
//  ZMenu.Pro
//
//  Created by Maxim Letushov on 6/2/17.
//  Copyright © 2017 ZMenu.Pro. All rights reserved.
//

import Foundation

struct GeneralNetworkError: Error {}

protocol NetworkErrorHandling: class {
    
    func handleErrorInNetworkResponse(_ response: NetworkErrorResponse?)
    
    func isInternetConnectionErrorInResponse(_ response: NetworkErrorResponse?) -> Bool
    
    func isTimedOutInResponse(_ response: NetworkErrorResponse?) -> Bool
    
    func errorInNetworkResponse(_ response: NetworkErrorResponse?) -> Error?
    
}

extension NetworkErrorHandling {
    
    func isInternetConnectionErrorInResponse(_ response: NetworkErrorResponse?) -> Bool {
        if let urlError = response?.error as? URLError {
            switch urlError.code {
            case .notConnectedToInternet,
                 .networkConnectionLost:
                return true
            default:
                break
            }
        }
        
        return false
    }
    
    func isTimedOutInResponse(_ response: NetworkErrorResponse?) -> Bool {
        if let urlError = response?.error as? URLError {
            return urlError.code == .timedOut
        }
        
        return false
    }
    
    func errorInNetworkResponse(_ response: NetworkErrorResponse?) -> Error? {
        guard let response = response else { return GeneralNetworkError() }
        return response.result == .ok ? nil : response.error ?? GeneralNetworkError()
    }
    
}
