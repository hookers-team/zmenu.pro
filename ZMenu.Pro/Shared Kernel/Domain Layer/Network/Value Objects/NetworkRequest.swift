//
//  NetworkRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 24.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

typealias NetworkResponseCompletion<T: Decodable> = (NetworkErrorResponse?, T?) -> Void

struct NetworkRequest<T: Decodable> {
    
    let endpoint: String?
    var method: HTTPMethod = .get
    var parameters: Encodable? = nil
    var headers: HTTPHeaders? = nil
    let completion: NetworkResponseCompletion<T>
    
}
