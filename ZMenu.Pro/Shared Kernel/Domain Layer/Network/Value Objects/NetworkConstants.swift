//
//  NetworkConstants.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct NetworkConstants {
    
    static var devServerBaseURL: URL {
        return NetworkConstants.nikaNgrokURL
    }

    static let kirillNgrokURL = URL(string: "https://889bf10c.ngrok.io/")!
    static let stasNgrokURL = URL(string: "https://71d12d34.ngrok.io/")!
    static let nikaNgrokURL = URL(string: "https://49fadb89.ngrok.io/")!
    static let localHostUrl = URL(string: "http://localhost:8081/")!
    static let prodServerURL = URL(string:"https://skaix.com/")!
    
}
