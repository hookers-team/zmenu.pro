//
//  UserStore.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 13.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

fileprivate struct Keys {
    static let userData = "UserData"
}

//KS: TODO: Thinking how to correct handnling accountModel and User's information.
final class UserSessionService {
    
    var userDefaultStorage: Storage!

    var isNeedUserAuthorization: Bool {
        return userDefaultStorage.data(forKey: Keys.userData) == nil
    }
    
    lazy var accountModel: SessionModel? = {
        let data = userDefaultStorage.data(forKey: Keys.userData) ?? Data()
        let accountModel = NSKeyedUnarchiver.unarchiveObject(with: data) as? SessionModel
        
        return accountModel
    }()
    
    func saveSessionModel(_ accountModel: SessionModel) {
        self.accountModel = accountModel
        
        let data = NSKeyedArchiver.archivedData(withRootObject: accountModel)
        
        userDefaultStorage.set(data: data, forKey: Keys.userData)
    }
    
    func removeSession() {
        userDefaultStorage.set(data: nil, forKey: Keys.userData)
    }
    
}
