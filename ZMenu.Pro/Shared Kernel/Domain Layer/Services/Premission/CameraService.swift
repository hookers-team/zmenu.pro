//
//  CameraService.swift
//  ZMenu.Pro
//
//  Created by Sokolov Kirill on 10/10/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import Foundation
import Permission

protocol CameraServiceProviding: PermissionServiceInterface {
    
    //KS: TODO: implement as needed
    
}

final class CameraService: CameraServiceProviding {
    
    var permission: Permission
    
    init(permission: Permission = .camera) {
        self.permission = .camera
    }
    
}
