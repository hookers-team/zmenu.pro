//
//  PhotoLibraryService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 10/10/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import Foundation
import Permission

protocol PhotoLibraryServiceProviding: PermissionServiceInterface {
    
    //KS: TODO: implement as needed
    
}

final class PhotoLibraryService: PhotoLibraryServiceProviding {
    
    var permission: Permission
    
    init() {
        permission = .photos
    }
    
}
