//
//  PremissionService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 21.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation
import Permission

protocol PermissionServiceInterface: class {
    
    var permission: Permission { get }
    var permissionStatus: PermissionAuthorizationStatus { get }
    
    func requestAuthorization(completion: @escaping (PermissionStatus) -> Void)
    
}

extension PermissionServiceInterface {
    
    var permissionStatus: PermissionAuthorizationStatus {
        let status = permission.status
        
        switch status {
        case .notDetermined:
            return .notDetermined
        case .denied:
            return .denied
        case .disabled:
            return .disabled
        case .authorized:
            return .authorized
        }
    }
    
    func requestAuthorization(completion: @escaping (PermissionStatus) -> Void) {
        permission.request({ status in
            completion(status)
        })
    }
    
}

enum PermissionAuthorizationStatus {
    
    case authorized
    case denied
    case disabled
    case notDetermined
    
}

private struct LocalizedStringConstants {
    
    static var needPermission: String { return "Необходимо разрешение".localized() }
    static var launchPhotoLibraryPermission: String { return "Чтобы перейти в галерею необходимо разрешение на использование медиа файлов".localized() }
    static var cancel: String { return "Отмена".localized() }
    static var settings: String { return "Настройки".localized() }
    static var launchCameraPermission: String { return "Чтобы сделать фото необходимо разрешение на использование камеры".localized() }
    static var launchLocationPermission: String { return "Для использования сервиса необходимо разрешение на определение местоположения".localized() }
    static var launchNotificationPermission: String { return "Для использования сервиса необходимо разрешение на отправку уведомлений".localized() }
    
}

final class PermissionService {
    
    private(set) var locationService: LocationServiceProviding
    private(set) var notificationService: NotificationServiceProviding
    private(set) var cameraService: CameraServiceProviding
    private(set) var photoLibraryService: PhotoLibraryServiceProviding
    
    init(locationService: LocationServiceProviding, notificationService: NotificationServiceProviding, cameraService: CameraServiceProviding, photoLibraryService: PhotoLibraryServiceProviding) {
        self.locationService = locationService
        self.notificationService = notificationService
        self.cameraService = cameraService
        self.photoLibraryService = photoLibraryService
    }
    
    func requestNotificationAndLocationPermissions() {
        //KS: TODO: think what to do inside completions below. It might be usefull later
        let requestNotificationIfNeeded = {
            if self.notificationService.permissionStatus == .notDetermined {
                self.notificationService.requestAuthorization(completion: { _ in })
            }
        }
        
        if locationService.permissionStatus == .notDetermined {
            locationService.requestAuthorization(completion: { _ in
                requestNotificationIfNeeded()
            })
        } else {
            requestNotificationIfNeeded()
        }
    }
    
}

extension PermissionService {
    
    func setupPermissionService(withPermissionType type: PermissionType) {
        switch type {
        case .locationWhenInUse:
            if let locationService = locationService as? LocationPermissionService {
                setupDeniedAlert(permission: locationService.permission, title: LocalizedStringConstants.needPermission, message: LocalizedStringConstants.launchLocationPermission, cancel: LocalizedStringConstants.cancel, settings: LocalizedStringConstants.settings)
            }
        case .notifications:
            if let notificationService = notificationService as? NotificationService {
                setupDeniedAlert(permission: notificationService.permission, title: LocalizedStringConstants.needPermission, message: LocalizedStringConstants.launchNotificationPermission, cancel: LocalizedStringConstants.cancel, settings: LocalizedStringConstants.settings)
            }
        case .camera:
            setupDeniedAlert(permission: cameraService.permission, title: LocalizedStringConstants.needPermission, message: LocalizedStringConstants.launchCameraPermission, cancel: LocalizedStringConstants.cancel, settings: LocalizedStringConstants.settings)
        case .photos:
            setupDeniedAlert(permission: photoLibraryService.permission, title: LocalizedStringConstants.needPermission, message: LocalizedStringConstants.launchPhotoLibraryPermission, cancel: LocalizedStringConstants.cancel, settings: LocalizedStringConstants.settings)
        default:
            break
        }
    }
    
    private func setupDeniedAlert(permission: Permission, title: String, message: String, cancel: String, settings: String) {
        let permissionDeniedAlert = permission.deniedAlert
        
        permissionDeniedAlert.title = title
        permissionDeniedAlert.message = message
        permissionDeniedAlert.cancel = cancel
        permissionDeniedAlert.settings = settings
    }
    
}
