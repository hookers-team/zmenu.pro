//
//  NotificationService.swift
//  ZMenu.Pro
//
//  Created by Sokolov Kirill on 10/10/18.
//  Copyright © 2018 ZMenu.Pro. All rights reserved.
//

import Foundation
import Permission

protocol NotificationServiceProviding: PermissionServiceInterface {
    
    //KS: TODO: fill in and user as needed
    
}

final class NotificationService: NotificationServiceProviding {
    
    var permission: Permission
    
    init(permission: Permission = .notifications) {
        self.permission = .notifications
    }
    
}
