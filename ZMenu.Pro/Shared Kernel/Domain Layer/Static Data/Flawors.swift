//
//  Flawors.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 12.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct FlaworsSort: Decodable {
    
    let enName: String
    let name: String
    
}

struct Flawors: Decodable {
    
    let flaworBrand: String
    let flaworSorts: [FlaworsSort]
    
    static func getTobaccos() -> [Flawors] {
        //KS: TODO: Write without force unwrap
        
        let path = Bundle.main.path(forResource: "flawors", ofType: "json")
        let jsonData = try? NSData(contentsOfFile: path!, options: NSData.ReadingOptions.mappedIfSafe)
        
        let flawors = try? JSONDecoder().decode([Flawors].self, from: jsonData! as Data)
        
        return flawors!
    }
    
}
