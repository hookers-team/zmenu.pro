//
//  AlertEvent.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 24.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct AlertEvent {
    
    struct ShowSuccessAlert: Event {
        typealias Payload = Value
        
        struct Value {
            let message: String
        }
        
    }
    
    struct ShowErrorAlert: Event {
        typealias Payload = Value
        
        struct Value {
            let message: String
        }
        
    }
    
}
