//
//  OrdersListCoordinator.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class OrdersListCoordinator: TabBarEmbedCoordinator {
    
    fileprivate var root: UINavigationController!
    fileprivate var ordersStore: OrdersListStore!
    
    private var workPlace: UserInfo.WorkPlace
    private var dispatcher: Dispatcher {
        return context.dispatcher
    }
    
    init(context: CoordinatingContext, workPlace: UserInfo.WorkPlace) {
        let servicesImage = UIImage(named: "services")
        let servicesActiveImage = UIImage(named: "services_active")
        
        let tabItemInfo = TabBarItemInfo(
            title: nil,
            icon: servicesImage,
            highlightedIcon: servicesActiveImage)
        
        self.workPlace = workPlace
        
        super.init(context: context, tabItemInfo: tabItemInfo)
    }
    
    override func prepareForStart() {
        super.prepareForStart()
        
        ordersStore = makeOrdersStore()
        openOrdersListViewController()
        register()
    }
    
    override func createFlow() -> UIViewController {
        return root
    }
    
}

extension OrdersListCoordinator {
    
    func makeOrdersStore() -> OrdersListStore {
        let restaurantNetwork = OrdersNetworkService(networkService: context.networkService)
        
        return OrdersListStore(networkService: restaurantNetwork, dispatcher: context.dispatcher, workPlace: workPlace)
    }
    
}


//MARK: Open View Controllers
extension OrdersListCoordinator {
    
    private func openOrdersListViewController() {
        let ordersListViewController = UIStoryboard.OrdersList.ordersListViewController
        
        ordersListViewController.dispatcher = context.dispatcher
        ordersListViewController.styleguide = context.styleguide
        ordersListViewController.ordersListStore = ordersStore
        
        root = UINavigationController(rootViewController: ordersListViewController)
    }
    
}

extension OrdersListCoordinator {
    
    private func register() {
        registerCloseScreen()
    }
    
    private func registerCloseScreen() {
        dispatcher.register(type: OrdersEvent.NavigationEvent.CloseScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                if self?.root.presentedViewController != nil {
                    self?.root.dismiss(animated: box.animated, completion: nil)
                } else {
                    self?.root.popViewController(animated: true)
                }
            case .failure(_):
                break
            }
        }
    }
    
}

