//
//  OrderListTableViewCell.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 12.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol OrderListTableViewCellDelegate: class {
    
    func orderListTableViewCell(_ cell: OrderListTableViewCell, wantsToCancelOrder order: Order)
    func orderListTableViewCell(_ cell: OrderListTableViewCell, changeConditionForOrder order: Order, condition: OrderCondition)
    func orderListTableViewCell(_ cell: OrderListTableViewCell, wantsToReservTableForOrder order: Order)
    
}

private struct LocalizedStringConstants {
    
    static var onTheTable: String { return "За столом: ".localized() }
    
}

final class OrderListTableViewCell: UITableViewCell, NibReusable {

    @IBOutlet weak var peopleCountLabel: UILabel!
    @IBOutlet weak var cancelButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var clientName: UITextViewZeroInsets!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentTopConstraint: NSLayoutConstraint!
    
    var tableViewService: PreviewMixesTableViewService!
    weak var delegate: OrderListTableViewCellDelegate?
    private var order: Order!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setup(with order: Order, delegate: OrderListTableViewCellDelegate?, styleguide: DesignStyleGuide) {
        self.order = order
        
        if tableViewService == nil {
            tableViewService = PreviewMixesTableViewService(tableView: tableView, styleguide: styleguide)
            tableViewService.configurate()
            tableViewService.setCellsConfiguration(leadingImageConstraint: 5)
            
             clientName.linkTextAttributes = [ kCTUnderlineStyleAttributeName : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor.rawValue: UIColor.white] as? [NSAttributedString.Key : Any]
            
            actionButton.addTarget(self, action: #selector(orderAction), for: .touchUpInside)
            cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        }
        
        if let comment = order.comment {
            commentLabel.text = "Комментарий: ".localized() + comment
            commentTopConstraint.constant = 5
        } else {
            commentTopConstraint.constant = 0
        }
        
        peopleCountLabel.text = "Кол-во человек: ".localized() + order.peopleCount
        tableViewService.amount = order.amount
        tableViewService.dueTime = "Заказ на ".localized() + Date.displayableSectionDate(from: Date(seconds: Int64(order.dueDate)))
        
        tableViewHeightConstraint.constant = CGFloat(order.previewMixes.count) * PreviewMixesTableViewService.Constants.cellHeight + PreviewMixesTableViewService.Constants.footerHeight
        
        configurate(with: order, styleguide: styleguide)
        tableViewService.updateOrderedMixes(order.previewMixes)
        refreshUI(withStyleguide: styleguide)
        
        self.delegate = delegate
    }
    
    private func configurate(with order: Order, styleguide: DesignStyleGuide) {
        
        switch order.buttonType {
        case .waitingForReserving?:
            actionButton.isEnabled = true
            cancelButton.isEnabled = true
            
            actionButton.tintColor = styleguide.labelTextColor
            actionButton.setTitle("Принять 📝".localized(), for: .normal)
            actionButton.tintColor = styleguide.primaryColor
            
            cancelButton.setTitle("Отменить ❌".localized(), for: .normal)
            cancelButton.tintColor = styleguide.badgeColor
            
            placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            clientName.text = order.client.name + " +" + order.client.phone
            
            cancelButtonHeightConstraint.constant = 44
        case .workingOff?:
            actionButton.isEnabled = false
            cancelButton.isEnabled = true
            
            actionButton.setTitle("В работу 🕗".localized(), for: .disabled)
            actionButton.tintColor = styleguide.senderTextColor
            
            cancelButton.setTitle("Отменить ❌".localized(), for: .normal)
            cancelButton.tintColor = styleguide.badgeColor
            
            placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            clientName.text = order.client.name + " +" + order.client.phone
            
            cancelButtonHeightConstraint.constant = 44
        case .workingOn?:
            actionButton.isEnabled = true
            cancelButton.isEnabled = true
            
            actionButton.setTitle("В работу 💡".localized(), for: .normal)
            actionButton.tintColor = styleguide.primaryLightColor
            
            cancelButton.setTitle("Отменить ❌".localized(), for: .normal)
            cancelButton.tintColor = styleguide.badgeColor
            
             placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            clientName.text = order.client.name + " +" + order.client.phone
            
            cancelButtonHeightConstraint.constant = 44
        case .rating?:
            cancelButton.isEnabled = true
            actionButton.isEnabled = true
            
            actionButton.tintColor = styleguide.warningColor
            actionButton.setTitle("Узнать ⭐️".localized(), for: .normal)
            
            cancelButton.setTitle("Отменить ❌".localized(), for: .normal)
            cancelButton.tintColor = styleguide.badgeColor
            
            clientName.text = order.client.name + " +" + order.client.phone + " 💡"
            
            cancelButtonHeightConstraint.constant = 44
        case .canceled?:
            actionButton.isEnabled = false
            cancelButton.isEnabled = false
            
            actionButton.setTitle("Отменен 🤷‍♂️".localized(), for: .disabled)
            actionButton.tintColor = styleguide.senderTextColor
            
            cancelButton.setTitle("", for: .disabled)
            
            clientName.text = "❌ " + order.client.name
            
            placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            
            cancelButtonHeightConstraint.constant = 0
        case .rated?:
            actionButton.isEnabled = false
            cancelButton.isEnabled = false
            
            let title: String
            
            if let rating = order.rating {
                let ceilRating = rating.rounded(.down)
                let part = rating - ceilRating
                let ceilStars = String(repeating: "⭐️", count: Int(ceilRating))
                let halfStar = part > 0 ? String(repeating: "✨", count: 1) : ""
                
                title = ceilStars + halfStar
            } else {
                title = "Оценен".localized()
            }
            
            actionButton.setTitle(title, for: .disabled)
            actionButton.tintColor = styleguide.warningColor
            
            cancelButton.setTitle("", for: .disabled)
            
            placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            clientName.text = "✅ " + order.client.name
            
            cancelButtonHeightConstraint.constant = 0
        case .notRated?:
            actionButton.isEnabled = false
            cancelButton.isEnabled = false
            
            actionButton.setTitle("Без оценки 😒".localized(), for: .disabled)
            actionButton.tintColor = styleguide.senderTextColor
            
            clientName.text = "✅ " + order.client.name
            
            cancelButtonHeightConstraint.constant = 0
        case .waitingForRating?:
            cancelButton.isEnabled = false
            actionButton.isEnabled = false
            
            cancelButton.setTitle("", for: .disabled)
            actionButton.setTitle("Ожидает ⭐️".localized(), for: .disabled)
            
            placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            clientName.text = "✅ " + order.client.name
            
            cancelButtonHeightConstraint.constant = 0
        case .inProcess?:
            actionButton.isEnabled = false
            cancelButton.isEnabled = true
            
            actionButton.setTitle("В работе 💡".localized(), for: .disabled)
            actionButton.tintColor = styleguide.senderTextColor
            
            cancelButton.setTitle("Отменить ❌".localized(), for: .normal)
            cancelButton.tintColor = styleguide.badgeColor
            
            placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            clientName.text = order.client.name + " +" + order.client.phone
            
            cancelButtonHeightConstraint.constant = 44
        case .waitingForWorkingOn?:
            actionButton.isEnabled = false
            cancelButton.isEnabled = true
            
            actionButton.setTitle("Ожидает 💡".localized(), for: .disabled)
            actionButton.tintColor = styleguide.senderTextColor
            
            cancelButton.setTitle("Отменить ❌".localized(), for: .normal)
            cancelButton.tintColor = styleguide.badgeColor
            
            placeLabel.text = LocalizedStringConstants.onTheTable + (order.place ?? "")
            clientName.text = order.client.name + " +" + order.client.phone
            
            cancelButtonHeightConstraint.constant = 44

        case .none:
            actionButton.isEnabled = false
            cancelButton.isEnabled = true
            
            actionButton.setTitle("", for: .disabled)
            actionButton.tintColor = styleguide.senderTextColor
            
            cancelButton.setTitle("Отменить ❌".localized(), for: .normal)
            cancelButton.tintColor = styleguide.badgeColor
            
            clientName.text = order.client.name + " +" + order.client.phone
        }
        
    }
    
    @objc private func orderAction() {
        switch order.buttonType {
        case .waitingForReserving?:
            delegate?.orderListTableViewCell(self, wantsToReservTableForOrder: order)
        case .workingOn?:
            delegate?.orderListTableViewCell(self, changeConditionForOrder: order, condition: OrderCondition.workingOn)
        case .rating?:
            delegate?.orderListTableViewCell(self, changeConditionForOrder: order, condition: OrderCondition.rating)
        default:
            break
        }
    }
    
    @objc private func cancelAction() {
        delegate?.orderListTableViewCell(self, wantsToCancelOrder: order)
    }
    
}

extension OrderListTableViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        containerView.layer.cornerRadius = styleguide.cornerRadius
        containerView.backgroundColor = styleguide.bubbleColor
        tableView.backgroundView = nil
        tableView.backgroundColor = .clear
        tableView.layer.cornerRadius = styleguide.cornerRadius
    }
    
}
