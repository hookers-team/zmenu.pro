//
//  OrdersNetworkService.swift
//  ZMenu.Pro
//
//  Created by Chelak Stas on 11/18/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

protocol OrdersNetwork {
    
    func getOrdersList(restaurantId: String, role: UserRole, completion: @escaping ((NetworkErrorResponse?, NetworkOrdersListResponse?) -> Void))
    func changeOrderCondition(orderId: String, newCondition: String?, completion: @escaping ((NetworkErrorResponse?, ChangeOrderConditionResponse?) -> Void))
    func reserveTable(orderId: String, place: String, with completion: @escaping ((NetworkErrorResponse?, ReserveTableResponse?) -> Void))
}

final class OrdersNetworkService: BaseNetworkService, OrdersNetwork {
    
    let networkService: HTTPNetworkService
    
    init(networkService: HTTPNetworkService) {
        self.networkService = networkService
    }
    
    func reserveTable(orderId: String, place: String, with completion: @escaping ((NetworkErrorResponse?, ReserveTableResponse?) -> Void)) {
        
        let data = ReserveTableRequest.Data(orderId: orderId, place: place)
        let request = ReserveTableRequest(data: data)
        
        networkService.executeRequest(endpoint: "tableReserve", method: .post, parameters: request.dictionary, headers: nil, completion: completion)
    }
    
    func changeOrderCondition(orderId: String, newCondition: String?, completion: @escaping ((NetworkErrorResponse?, ChangeOrderConditionResponse?) -> Void)) {
        
        let data = ChangeOrderConditionRequest.Data(newCondition: newCondition, orderId: orderId)
        let request = ChangeOrderConditionRequest(data: data)
        
        networkService.executeRequest(endpoint: "changeOrderCondition", method: .post, parameters: request.dictionary, completion: completion)
    }
    
    func getOrdersList(restaurantId: String, role: UserRole, completion: @escaping ((NetworkErrorResponse?, NetworkOrdersListResponse?) -> Void)) {
        let data = OrderListRequest.Data(role: role, restaurantId: restaurantId)
        let request = OrderListRequest(data: data)

        networkService.executeRequest(endpoint: "ordersList", method: .post, parameters: request.dictionary, headers: nil, completion: completion)
    }
    
}
