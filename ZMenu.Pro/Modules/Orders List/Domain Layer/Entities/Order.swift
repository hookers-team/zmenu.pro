//
//  Order.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 22.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

enum OrderCondition: String, Codable {
    
    case new
    case workingOn
    case rating
    case rated
    case notRated
    case canceled
    
}

struct Order: Codable {
    
    let orderId: String
    let dueDate: Int
    let condition: OrderCondition
    let peopleCount: String
    let amount: String
    let place: String?
    let restaurantName: String
    let client: Client
    let previewMixes: [PreviewMix]
    let rating: Double?
    let comment: String?
    var buttonType: ButtonType?
    
    
    
    enum ButtonType: String, Codable {
        
        case waitingForReserving
        case waitingForWorkingOn
        case workingOff
        case workingOn
        case inProcess
        case rating
        case waitingForRating
        
        case rated
        case notRated
        
        case canceled
        
    }
    
}
