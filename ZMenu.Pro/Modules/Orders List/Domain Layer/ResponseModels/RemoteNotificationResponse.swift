//
//  RemoteNotificationResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 19.02.2019.
//  Copyright © 2019 Kirill Sokolov. All rights reserved.
//

import Foundation

struct RemoteNotificationResponse: Codable {
    
    let workPlace: UserInfo.WorkPlace
    let action: NotificationType
    
}
