//
//  ReserveTableResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 19.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct ReserveTableResponse: Codable {
    
    struct Data: Codable {
        
        let orderId: String
        let place: String
        
    }
    
    let data: Data
    
}
