//
//  OrderListRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 15.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct OrderListRequest: Codable {
    
    struct Data: Codable {
        
        let role: UserRole
        let restaurantId: String
        
    }
    
    let data: Data
    
}
