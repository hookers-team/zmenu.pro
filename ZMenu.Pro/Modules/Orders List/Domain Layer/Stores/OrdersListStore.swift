//
//  OrdersStore.swift
//  ZMenu.Pro
//
//  Created by Chelak Stas on 11/18/18.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct OrdersListStoreStateChange: OptionSet, DataStateChange {
    
    let rawValue: Int
    
    static let isLoadingState = OrdersListStoreStateChange(rawValue: Int.bit1)
    static let orders = OrdersListStoreStateChange(rawValue: Int.bit2)
    static let reloadOrdersList = OrdersListStoreStateChange(rawValue: Int.bit3)
    
}

final class OrdersListStore: DomainModel {
    
    private(set) var networkService: OrdersNetwork
    private(set) var dispatcher: Dispatcher
    private(set) var workPlace: UserInfo.WorkPlace
    
    private(set) var orders: [Order]?
    private(set) var isLoading = false
    
    init(networkService: OrdersNetwork, dispatcher: Dispatcher, workPlace: UserInfo.WorkPlace) {
        self.networkService = networkService
        self.dispatcher = dispatcher
        self.workPlace = workPlace
        
        super.init()
    }
    
}

extension OrdersListStore {
    
    func addPlace(_ place: String, orderId: String) {
        startLoading()
     
        taskDispatcher.dispatch { [weak self] in
            self?.changeDataStateOf([.isLoadingState] as OrdersListStoreStateChange, work: {
                
                self?.networkService.reserveTable(orderId: orderId, place: place, with: { (networkErrorResponse, reserveTableResponse) in
                    
                    if networkErrorResponse == nil {
                        self?.changeDataStateOf([.reloadOrdersList, .isLoadingState] as OrdersListStoreStateChange, work: {
                            self?.showSuccsessMessage("Вы успешно забронировали стол".localized())
                            self?.isLoading = false
                        })
                        
                    } else {
                        self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                    }
                })
                
            })
        }
    }
    
    func changeOrderCondition(newCondition: String? = nil, orderId: String) {
        
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            
            self?.networkService.changeOrderCondition(orderId: orderId, newCondition: newCondition, completion: { (networkErrorResponse, changeOrderConditionResponse) in
                if changeOrderConditionResponse != nil {
                    self?.changeDataStateOf([.reloadOrdersList, .isLoadingState] as OrdersListStoreStateChange, work: {
                        self?.isLoading = false
                    })
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
        
    }
    
    func getOrdersList() {
        startLoading()

        taskDispatcher.dispatch { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.networkService.getOrdersList(restaurantId: strongSelf.workPlace.restaurant.restaurantId, role: strongSelf.workPlace.role, completion: { (networkResponse, ordersListResponse) in
                if let ordersListResponse = ordersListResponse {
                    strongSelf.changeDataStateOf([.orders, .isLoadingState] as OrdersListStoreStateChange, work: {
                        strongSelf.isLoading = false
                        
                        strongSelf.orders = ordersListResponse.data.ordersList
                    })
                } else {
                    strongSelf.showErrorMessage()
                }
            })
        }
    }
    
}

extension OrdersListStore {
    
    func startLoading() {
        changeDataStateOf(OrdersListStoreStateChange.isLoadingState) {
            isLoading = true
        }
    }
    
    func showSuccsessMessage(_ message: String) {
        let value = AlertEvent.ShowSuccessAlert.Value(message: message)
        
        dispatcher.dispatch(type: AlertEvent.ShowSuccessAlert.self, result: Result(value: value))
    }
    
    func showErrorMessage(_ message: String? = nil) {
        changeDataStateOf(OrdersListStoreStateChange.isLoadingState) {
            isLoading = false
            
            let value = AlertEvent.ShowErrorAlert.Value(message: message ?? "Проверьте интернет подключение и попробуйте снова 👾".localized())
            
            dispatcher.dispatch(type: AlertEvent.ShowErrorAlert.self, result: Result(value: value))
        }
    }
    
}
