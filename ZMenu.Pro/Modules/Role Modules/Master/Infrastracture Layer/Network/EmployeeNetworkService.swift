//
//  EmployeeNetwork.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 16.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import Alamofire

protocol EmployeeNetwork {
    
    func startWorkDay(restaurantId: String, role: UserRole, endTime: String, with completion: @escaping ((NetworkErrorResponse?, NetworkStartWorkDayResponse?) -> Void))
    
    func endWorkDay(with completion: @escaping ((NetworkErrorResponse?, SuccessMessageResponse?) -> Void))
    
    func createOrUpdateMix(mix: MixRequest, with completion: @escaping ((NetworkErrorResponse?, CreateOrUpdateMixResponse?) -> Void))
    func getMenu(byRestaurantId restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, MenuResponse?) -> Void))
    
    func removeMix(mixId: String, imageUrl: String, with completion: @escaping ((NetworkErrorResponse?, DeleteMixResponse?) -> Void))
    
    func editProfile(name: String, instagram: String?, address: Address?, schedule: Schedule?, description: String?, imageURL: String?, with completion: @escaping ((NetworkErrorResponse?, EditUserProfileNetworkResponse?) -> Void))
    
    func addEmployee(employeeUserId: String, restaurantId: String, role: String, with completion: @escaping ((NetworkErrorResponse?, AddEmployeeeResponse?) -> Void))
    
    func deleteEmployee(role: String, employeeId: String?, restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, DeleteEmployeeResponse?) -> Void))
    
    func editRestaurantProfile(restaurantId: String, name: String, instagram: String?, address: Address?, schedule: Schedule?, description: String?, photos: [String], with completion: @escaping ((NetworkErrorResponse?, EditRestaurantProfileResponse?) -> Void))
    
    func getUserInfo(with completion: @escaping ((NetworkErrorResponse?, UserInfoNetworkResponse?) -> Void))
    
    func editRestaurantPhoto(imageFile: Data?, oldImageURL: String?, restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, EditRestaurantProfileResponse?) -> Void))
    
    func createRestaurant(withName name: String, address: Address?, with completion: @escaping ((NetworkErrorResponse?, UserInfoNetworkResponse?) -> Void))
    
    func getEmployeeList(restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, EmployeeListRepsonse?) -> Void))
    
    func editEmployeePhoto(imageFile: Data?, oldImageURL: String?, with completion: @escaping ((NetworkErrorResponse?, EditUserProfileNetworkResponse?) -> Void))
    
    func deviceTokenUpdate(deviceToken: String, with completion: @escaping ((NetworkErrorResponse?, DeviceTokenUpdateResponse?) -> Void))
    
    func logout(with completion: @escaping ((NetworkErrorResponse?, SuccessMessageResponse?) -> Void))
  
}

final class EmployeeNetworkService: BaseNetworkService, EmployeeNetwork {
    
    let networkService: HTTPNetworkService
    
    init(networkService: HTTPNetworkService) {
        self.networkService = networkService
    }
    
    func deviceTokenUpdate(deviceToken: String, with completion: @escaping ((NetworkErrorResponse?, DeviceTokenUpdateResponse?) -> Void)) {
        
        let parametrs = [ "deviceToken" : deviceToken ] as Parameters
        
        networkService.executeRequest(endpoint: "deviceTokenUpdate", method: .post, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func getMenu(byRestaurantId restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, MenuResponse?) -> Void)) {
        let parametrs = [ "restaurantId" : restaurantId ] as Parameters
        
        networkService.executeRequest(endpoint: "menu", method: .get, parameters: parametrs, headers: nil, completion: completion)
    }
    
    
    func getEmployeeList(restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, EmployeeListRepsonse?) -> Void)) {
        let parametrs = ["restaurantId" : restaurantId] as Parameters
        
        networkService.executeRequest(endpoint: "employeesList", method: .post, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func deleteEmployee(role: String, employeeId: String?, restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, DeleteEmployeeResponse?) -> Void)) {
        
        let data = DeleteEmployeeRequest.Data(employeeId: employeeId, role: role, restaurantId: restaurantId)
        let request = DeleteEmployeeRequest(data: data)
        
        networkService.executeRequest(endpoint: "deleteEmployee", method: .post, parameters: request.dictionary, headers: nil, completion: completion)
    }
    
    func addEmployee(employeeUserId: String, restaurantId: String, role: String, with completion: @escaping ((NetworkErrorResponse?, AddEmployeeeResponse?) -> Void)) {
        
        let data = AddEmployeeRequest.Data(restaurantId: restaurantId, employeeUserId: employeeUserId, role: role)
        let request = AddEmployeeRequest(data: data)
        
        networkService.executeRequest(endpoint: "employeeAdding", method: .post, parameters: request.dictionary, headers: nil, completion: completion)
    }
    
    func editRestaurantProfile(restaurantId: String, name: String, instagram: String?, address: Address?, schedule: Schedule?, description: String?, photos: [String], with completion: @escaping ((NetworkErrorResponse?, EditRestaurantProfileResponse?) -> Void)) {
        let reqId = UUID().uuidString
        let restauant = RestaurantModel(restaurantId: restaurantId, name: name, rating: nil, address: address, instagram: instagram, schedule: schedule, description: description, photos: photos)
    
        let data = EditRestaurantProfileRequest.Data(restaurant: restauant)
        let request = EditRestaurantProfileRequest(reqId: reqId, action: "editRestaurant", data: data)
        
        networkService.executeRequest(endpoint: "editRestaurant", method: .post, parameters: request.dictionary, headers: nil, completion: completion)
    }
    
    func getUserInfo(with completion: @escaping ((NetworkErrorResponse?, UserInfoNetworkResponse?) -> Void)) {
        networkService.executeRequest(endpoint: "userInfo", method: .post, parameters: nil, headers: nil, completion: completion)
    }
    
    func editProfile(name: String, instagram: String?, address: Address?, schedule: Schedule?, description: String?, imageURL: String?, with completion: @escaping ((NetworkErrorResponse?, EditUserProfileNetworkResponse?) -> Void)) {
        
        let profile = UserInfo.Profile(schedule: schedule, address: address, name: name, rating: nil, description: description, instagram: instagram, imageURL: instagram, averageRate: nil, ordersCount: nil)
        
        let data = EditProfileNetworkRequest.Data(profile: profile)
        let request = EditProfileNetworkRequest(data: data)
        
        networkService.executeRequest(endpoint: "editProfile", method: .post, parameters: request.dictionary, headers: nil, completion: completion)
    }
    
    func removeMix(mixId: String, imageUrl: String, with completion: @escaping ((NetworkErrorResponse?, DeleteMixResponse?) -> Void)) {
        
        let data = DeleteMixRequest.Data(imageURL: imageUrl, mixId: mixId)
        let deleteMixRequest = DeleteMixRequest(data: data)
        
        networkService.executeRequest(endpoint: "deleteMix", method: .post, parameters: deleteMixRequest.dictionary, headers: nil, completion: completion)
    }
    
    func createRestaurant(withName name: String, address: Address?, with completion: @escaping ((NetworkErrorResponse?, UserInfoNetworkResponse?) -> Void)) {
        
        let action = "createRestaurant"
        let reqId = UUID().uuidString
        let restauant = CreateRestaurantRequest.Data.Restaurant(address: address, name: name)
        let data = CreateRestaurantRequest.Data(restaurant: restauant)
        
        let request = CreateRestaurantRequest(action: action, reqId: reqId, data: data)
        
        networkService.executeRequest(endpoint: action, method: .post, parameters: request.dictionary, headers: nil, completion: completion)
        
    }
    
    func editEmployeePhoto(imageFile: Data?, oldImageURL: String?, with completion: @escaping ((NetworkErrorResponse?, EditUserProfileNetworkResponse?) -> Void)) {
        let multipartFormData: (MultipartFormData) -> Void = { (multipartFormData) in
            if let imageURLData = oldImageURL?.data(using: .utf8) {
                multipartFormData.append(imageURLData, withName: "imageURL")
            }
            
            if let imageData = imageFile {
                multipartFormData.append(imageData, withName: "file", fileName: "photo.png", mimeType:  "image/png")
            }
        }
        
        
        networkService.executeRequestWithMultipartData(multipartFormData, endpoint: "profileImage", method: .post, completion: completion)
    }
    
    func editRestaurantPhoto(imageFile: Data?, oldImageURL: String?, restaurantId: String, with completion: @escaping ((NetworkErrorResponse?, EditRestaurantProfileResponse?) -> Void)) {
         let multipartFormData: (MultipartFormData) -> Void = { (multipartFormData) in
            if let restaurantIdData = restaurantId.data(using: .utf8) {
                
                multipartFormData.append(restaurantIdData, withName: "restaurantId")
                
                if let imageURLData = oldImageURL?.data(using: .utf8) {
                    multipartFormData.append(imageURLData, withName: "imageURL")
                }
                
                if let imageData = imageFile {
                    multipartFormData.append(imageData, withName: "file", fileName: "photo.png", mimeType:  "image/png")
                }
            }
           
        }
        
         networkService.executeRequestWithMultipartData(multipartFormData, endpoint: "restaurantImage", method: .post, completion: completion)
        
    }
    
    func createOrUpdateMix(mix: MixRequest, with completion: @escaping ((NetworkErrorResponse?, CreateOrUpdateMixResponse?) -> Void)) {

        let encoder = JSONEncoder()
        
        let multipartFormData: (MultipartFormData) -> Void = { (multipartFormData) in
            if let nameData = mix.name.data(using: .utf8),
                let categoryIdData = mix.categoryId.data(using: .utf8),
                let restaurantIdData = mix.restaurantId.data(using: .utf8),
                let strengthData = mix.strength.rawValue.data(using: .utf8),
                let flaworData = try? encoder.encode(mix.flawor),
                let descriptionData = mix.description.data(using: .utf8),
                let bowlData = mix.bowl?.data(using: .utf8),
                let fillingData = mix.filling?.data(using: .utf8),
                let priceData = mix.price.data(using: .utf8) {
                
                if let mixIdData = mix.mixId?.data(using: .utf8) {
                    multipartFormData.append(mixIdData, withName: "mixId")
                }
                
                multipartFormData.append(nameData, withName: "name")
                multipartFormData.append(categoryIdData, withName: "categoryId")
                multipartFormData.append(restaurantIdData, withName: "restaurantId")
                multipartFormData.append(priceData, withName: "price")
                multipartFormData.append(strengthData, withName: "strength")
                
                multipartFormData.append(withUnsafeBytes(of: mix.likes, { Data($0) }), withName: "likes")
                
                multipartFormData.append(flaworData, withName: "flawor")
                multipartFormData.append(descriptionData, withName: "description")
                multipartFormData.append(bowlData, withName: "bowl")
                multipartFormData.append(fillingData, withName: "filling")
                
                if let imageURLData = mix.imageURL?.data(using: .utf8) {
                       multipartFormData.append(imageURLData, withName: "imageURL")
                }
                
                if let imageData = mix.imageFile {
                     multipartFormData.append(imageData, withName: "file", fileName: "photo.png", mimeType:  "image/png")
                }
            }
        }
        
        networkService.executeRequestWithMultipartData(multipartFormData, endpoint: NetworkWorkingDayEndpoints.mix, method: .post, completion: completion)
        
    }
    
    func startWorkDay(restaurantId: String, role: UserRole, endTime: String, with completion: @escaping ((NetworkErrorResponse?, NetworkStartWorkDayResponse?) -> Void)) {
        
        let startWorkDayRequest = NetworkStartWorkDayRequest.Data(restaurantId: restaurantId, role: role.rawValue, endTime: endTime)
        
        let request = NetworkStartWorkDayRequest.init(reqId: UUID().uuidString, action: NetworkWorkingDayEndpoints.startWorkingDay, data: startWorkDayRequest)
        
        networkService.executeRequest(endpoint: "startWorkingDay", method: .post, parameters: request.dictionary, headers: nil, completion: completion)
    }
    
    func endWorkDay(with completion: @escaping ((NetworkErrorResponse?, SuccessMessageResponse?) -> Void)) {
        
        networkService.executeRequest(endpoint: "endWorkingDay", method: .post, parameters: nil, headers: nil, completion: completion)
        
    }
    
    func logout(with completion: @escaping ((NetworkErrorResponse?, SuccessMessageResponse?) -> Void)) {
         networkService.executeRequest(endpoint: "logout", method: .post, parameters: nil, headers: nil, completion: completion)
    }

}
