//
//  EmployeeStore.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 21.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation


struct EmployeeStoreStateChange: OptionSet, DataStateChange {
    
    let rawValue: Int
    
    static let isLoadingState = EmployeeStoreStateChange(rawValue: Int.bit1)
    static let workDay = EmployeeStoreStateChange(rawValue: Int.bit2)
    static let menu = EmployeeStoreStateChange(rawValue: Int.bit3)
    static let userProfile = EmployeeStoreStateChange(rawValue: Int.bit4)
    static let editRestaurantProfile = EmployeeStoreStateChange(rawValue: Int.bit5)
    
    static let userInfo = EmployeeStoreStateChange(rawValue: Int.bit6)
    static let logout = EmployeeStoreStateChange(rawValue: Int.bit7)
    static let leaveRestaurant = EmployeeStoreStateChange(rawValue: Int.bit8)
    
}

enum EmployeeStoreStateChangeWithData: DataStateChange {
    
    case removeMix(categoryId: String, mixId: String)
    case createOrUpdate(_ mix: Mix)
    case employeeList(_ employeeList: EmployeeList)
    case deleteEmployee(_ deletedEmployee: DeletedEmployee)
    
}

final class EmployeeStore: DomainModel {
    
    private(set) var isLoading = false
    private(set) var menuData: MenuResponse.Data?
    
    let networkService: EmployeeNetwork
    let dispatcher: Dispatcher
    let userSessionService: UserSessionService
    
    private(set)var userInfo: UserInfo?
    
    let permissionService: PermissionService
    
    //KS: TODO: Maybe need to separate it in different stores.
    private(set) var ownerRestaurants: [RestaurantModel]?
    
    var photoLibraryPermissionStatus: PermissionAuthorizationStatus {
        return permissionService.photoLibraryService.permissionStatus
    }
    
    var cameraPermissionStatus: PermissionAuthorizationStatus {
        return permissionService.cameraService.permissionStatus
    }
    
    init(networkService: EmployeeNetwork, dispatcher: Dispatcher, userSessionService: UserSessionService, permissionService: PermissionService) {
        self.networkService = networkService
        self.dispatcher = dispatcher
        self.userSessionService = userSessionService
        self.permissionService = permissionService
    }
    
}

extension EmployeeStore {
    
    func deleteEmployee(role: String, employeeId: String?, restaurantId: String) {
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.deleteEmployee(role: role, employeeId: employeeId, restaurantId: restaurantId, with: { (networkErrorResponse, deleteEmployeeResponse) in
                if let deleteEmployeeResponse = deleteEmployeeResponse {
                    self?.changeDataStateOf(EmployeeStoreStateChange.isLoadingState, work: {
                        self?.changeDataStateOf(EmployeeStoreStateChangeWithData.deleteEmployee(deleteEmployeeResponse.data.deletedEmployee), work: {
                            self?.isLoading = false
                            let roleLocalized = deleteEmployeeResponse.data.deletedEmployee.role == .admin ? "Администратор".localized() : "Кальянщик".localized()
                            self?.showSuccsessMessage("\(roleLocalized) уволен 😔".localized())
                        })
                    })
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
                
            })
        }
    }
    
    func leaveRestaurant(role: String, restaurantId: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.deleteEmployee(role: role, employeeId: nil, restaurantId: restaurantId, with: { (networkErrorResponse, deleteEmployeeResponse) in
                if let deleteEmployeeResponse = deleteEmployeeResponse {
                    self?.changeDataStateOf([.isLoadingState, .leaveRestaurant] as EmployeeStoreStateChange, work: {
                            self?.isLoading = false
                            let roleLocalized = deleteEmployeeResponse.data.deletedEmployee.role == .admin ? "администратор".localized() : "кальянщик".localized()
                            self?.showSuccsessMessage("Вы больше не \(roleLocalized) в этом заведении 👾".localized())
                    })
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
                
            })
        }
        
    }
    
    func addEmployee(employeeUserId: String, role: QRService.Role, restaurantId: String) {        
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.addEmployee(employeeUserId: employeeUserId, restaurantId: restaurantId, role: role.rawValue, with: { (error, addEmployeeResponse) in
                if let addEmployeeResponse = addEmployeeResponse {
                    self?.changeDataStateOf(EmployeeStoreStateChange.isLoadingState, work: {
                        self?.isLoading = false
                        let roleLocalized = role == .admin ? "Администратор".localized() : "Кальянщик".localized() + " \(addEmployeeResponse.data.profile.name ?? "был" )"
                        self?.showSuccsessMessage("\(roleLocalized) успешно добавлен!💫 ".localized())
                    })
                } else {
                    self?.showErrorMessage(error?.serverError?.errorText)
                }
            })
        }
    }
    
    func getUserInfo() {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.getUserInfo(with: { (networkErrorResponse, userInfoNetworkResponse) in
                if let userInfoNetworkResponse = userInfoNetworkResponse {
                    self?.changeDataStateOf([.isLoadingState, .userInfo] as EmployeeStoreStateChange) {
                        self?.userInfo = userInfoNetworkResponse.data.userInfo
                        self?.isLoading = false
                        
                        if self?.userInfo?.workPlaces.isEmpty ?? true {
                        self?.showErrorMessage("Вас пока не добавили ни в одно заведение 🤷‍♂️".localized())
                        }
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
        
    }
    
    func getEmployeeList(restaurantId: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            
            self?.networkService.getEmployeeList(restaurantId: restaurantId, with: { (networkErrorResponse, employeeListResponse) in
                
                if let employeeListResponse = employeeListResponse {
                    self?.changeDataStateOf(EmployeeStoreStateChange.isLoadingState) {
                        self?.changeDataStateOf(EmployeeStoreStateChangeWithData.employeeList(employeeListResponse.data.employeesList), work: {
                            self?.isLoading = false
                        })
                    }
                    
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
            
        }
    }
    
    func endWorkDay() {
        
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.endWorkDay(with: { [weak self] (networkErrorResponse, endWorkDayResponse) in
                
                if let startWorkDayResponse = endWorkDayResponse {
                    self?.changeDataStateOf([.workDay, .isLoadingState] as EmployeeStoreStateChange, work: {
                        self?.isLoading = false
                        self?.userInfo?.workDay = nil
                        self?.showSuccsessMessage(startWorkDayResponse.data.message)
                        
                    })
                } else {
                   self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
        
    }
    
    func startWorkDay(restaurantId: String, role: UserRole, endTime: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.startWorkDay(restaurantId: restaurantId, role: role, endTime: endTime, with: { [weak self] (networkErrorResponse, startWorkDayResponse) in
                if let startWorkDayResponse = startWorkDayResponse {
                    self?.changeDataStateOf([.workDay, .isLoadingState] as EmployeeStoreStateChange, work: {
                            self?.isLoading = false
                            self?.userInfo?.workDay = startWorkDayResponse.data.workDay
                        
                        self?.showSuccsessMessage("Теперь вы будете получать уведомления о новых заказах👏 \nХорошего, рабочего дня, \(self?.userInfo?.profile?.name ?? "")! 😊".localized())
                        })
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
    
    }
    
    func deviceTokenUpdate(deviceToken: String) {
        taskDispatcher.dispatch { [weak self] in
            
            self?.networkService.deviceTokenUpdate(deviceToken: deviceToken, with: { (networkErrorResponse, deviceTokenUpdateResponse) in
                
                //KS: Do nothing
                
            })
            
        }
    }
    
    func createOrUpdateMix(_ mix: MixRequest) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.createOrUpdateMix(mix: mix) { [weak self] (networkErrorResponse, mixResponse) in
               
                if let mixResponse = mixResponse {
                    self?.changeDataStateOf(EmployeeStoreStateChange.isLoadingState) {
                        self?.changeDataStateOf(EmployeeStoreStateChangeWithData.createOrUpdate(mixResponse.data.mix), work: {
                        self?.isLoading = false
                        })
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            }
        }
    }
    
    func getMenu(byRestaurantId restaurantId: String) {
        startLoading()

        taskDispatcher.dispatch { [weak self] in
            self?.networkService.getMenu(byRestaurantId: restaurantId) { (networkErrorResponse, menuResponse) in
                
                if let menuResponse = menuResponse {
                    self?.changeDataStateOf([.menu, .isLoadingState] as EmployeeStoreStateChange, work: {
                        self?.isLoading = false
                        
                        self?.menuData = menuResponse.data
                    })
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            }
        }
    }

    
    func editRestaurantPhoto(imageFile: Data?, oldImageURL: String?, restaurantId: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.editRestaurantPhoto(imageFile: imageFile, oldImageURL: oldImageURL, restaurantId: restaurantId, with: { (networkErrorResponse, editRestaurantProfileResponse) in
                
                if let editRestaurantProfileResponse = editRestaurantProfileResponse {
                    self?.changeDataStateOf([.editRestaurantProfile, .isLoadingState] as EmployeeStoreStateChange) {
                        self?.isLoading = false
                        
                        let editedWorkPlaces = self?.userInfo?.workPlaces.filter{$0.restaurant.restaurantId == editRestaurantProfileResponse.data.restaurant.restaurantId} //if we have two differt roles in the one restaurant, we should update each workPlace with the same restaurant.
                        
                        editedWorkPlaces?.forEach{
                            let workPlace = $0
                            
                            workPlace.restaurant = editRestaurantProfileResponse.data.restaurant
                        }
                        self?.showSuccsessMessage("Фото заведения успешно обновлено 📸".localized())
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
    }
    
    func createRestaurant(withName name: String, address: Address) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.createRestaurant(withName: name, address: address, with: { (networkErrorResponse, employeeInfoNetworkResponse) in
                if let employeeInfoNetworkResponse = employeeInfoNetworkResponse {
                    self?.changeDataStateOf([.isLoadingState, .userInfo] as EmployeeStoreStateChange) {
                        self?.isLoading = false
                        self?.userInfo = employeeInfoNetworkResponse.data.userInfo
                        self?.showSuccsessMessage("Заведение успешно создано 👍 ".localized())
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
    }
    
    func editProfile(name: String, instagram: String? = nil, address: Address?, scedule: Schedule? = nil, description: String? = nil, imageURL: String?
    ) {
        startLoading()
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.editProfile(name: name, instagram: instagram, address: address, schedule: scedule, description: description, imageURL: imageURL) { (networkErrorResponse, editProfileResponse) in
                
                if let editProfileResponse = editProfileResponse {
                    self?.changeDataStateOf([.isLoadingState, .editRestaurantProfile] as EmployeeStoreStateChange) {
                        self?.isLoading = false
                        self?.userInfo?.profile = editProfileResponse.data.profile
                        self?.showSuccsessMessage("Ваш профиль успешно обновлен 💫".localized())
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
                
            }
        }
        
    }
    
    func removeMix(categoryId: String, mixId: String, imageUrl: String) {
        startLoading()
        
        networkService.removeMix(mixId: mixId, imageUrl: imageUrl, with: { (networkErrorResponse, deleteMixResponse) in
            
            if let deleteMixResponse = deleteMixResponse {
                self.changeDataStateOf(EmployeeStoreStateChangeWithData.removeMix(categoryId: categoryId, mixId: deleteMixResponse.data.mixId), work: {
                    self.changeDataStateOf(EmployeeStoreStateChange.isLoadingState) {
                        self.isLoading = false
                    }
                })
            } else {
                self.showErrorMessage(networkErrorResponse?.serverError?.errorText)
            }
        })
    }
    
    func logout() {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.logout(with: { (networkErrorResponse, logoutResponse) in
                
                if let logoutResponse = logoutResponse {
                    self?.changeDataStateOf([.isLoadingState, .logout] as EmployeeStoreStateChange) {
                        self?.isLoading = false
                        self?.showSuccsessMessage(logoutResponse.data.message)
                        let value = UserEvent.System.Logout.Value(animated: true)
                        
                        self?.dispatcher.dispatch(type:  UserEvent.System.Logout.self, result: Result(value: value))
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
        
    }
    
}

extension EmployeeStore {
    
    //Restauant's requests
    
    func editRestaurant(restaurantId: String, name: String, instagram: String? = nil, address: Address?, schedule: Schedule? = nil, description: String? = nil, photos: [String]) {
        startLoading()
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.editRestaurantProfile(restaurantId: restaurantId, name: name, instagram: instagram, address: address, schedule: schedule, description: description, photos: photos, with: { (networkErrorResponse, editRestaurantProfileResponse) in

                if let editRestaurantProfileResponse = editRestaurantProfileResponse {
                    self?.changeDataStateOf([.isLoadingState, .editRestaurantProfile] as EmployeeStoreStateChange) {
                        self?.isLoading = false
                        
                        let editedWorkPlaces = self?.userInfo?.workPlaces.filter{$0.restaurant.restaurantId == editRestaurantProfileResponse.data.restaurant.restaurantId} //if we have two differt roles in the one restaurant, we should update each workPlace with the same restaurant.
                        
                        editedWorkPlaces?.forEach{
                            let workPlace = $0
                            
                            workPlace.restaurant = editRestaurantProfileResponse.data.restaurant
                        }
                        self?.showSuccsessMessage("Профиль заведения успешно обновлен 💫".localized())
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
        
    }
    
    func editEmployeePhoto(imageFile: Data?, oldImageURL: String?) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.editEmployeePhoto(imageFile: imageFile, oldImageURL: oldImageURL, with: { (networkErrorResponse, userProfileNetworkResponse) in
                
                if let userProfileNetworkResponse = userProfileNetworkResponse {
                    self?.changeDataStateOf([.isLoadingState, .userProfile] as EmployeeStoreStateChange) {
                        self?.isLoading = false
                        self?.userInfo?.profile = userProfileNetworkResponse.data.profile
                        self?.showSuccsessMessage("Фото вашего профиля успешно обновлено 📸".localized())
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
                
            })
            
        }
    }
    
}

extension EmployeeStore {
    
    func startLoading() {
        changeDataStateOf(EmployeeStoreStateChange.isLoadingState) {
            isLoading = true
        }
    }
    
    func showSuccsessMessage(_ message: String) {
        let value = AlertEvent.ShowSuccessAlert.Value(message: message)
        
        dispatcher.dispatch(type: AlertEvent.ShowSuccessAlert.self, result: Result(value: value))
    }
    
    func showErrorMessage(_ message: String? = nil) {
        changeDataStateOf(EmployeeStoreStateChange.isLoadingState) {
            isLoading = false
            
            let value = AlertEvent.ShowErrorAlert.Value(message: message ?? LocalizedAlertStringConstants.defaultErrorMessage)
            
            dispatcher.dispatch(type: AlertEvent.ShowErrorAlert.self, result: Result(value: value))
        }
    }
    
}
