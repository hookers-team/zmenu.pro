//
//  User.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

enum UserRole: String, Codable {
    
    case owner
    case master
    case admin
    case uknown
    
    static func displayableRole(_ role: UserRole?) -> String {
        
        switch role {
        case .owner?:
            return "Создатель".localized()
        case .master?:
            return "Кальянщик".localized()
        case .admin?:
            return "Администратор".localized()
        default:
            return "Сотрудник".localized()
        }
        
        
    }
    
}

final class UserInfo: Decodable {
    
    var profile: Profile?
    var workPlaces: [WorkPlace] = []
    var workDay: WorkDay?
    
}

extension UserInfo {
    
    final class Profile: Codable {
        
        let schedule: Schedule?
        let address: Address?
        let name: String
        let rating: String?
        let averageRate: String?
        let ordersCount: String?
        let description: String?
        let instagram: String?
        let imageURL: String?
        
        init(schedule: Schedule?, address: Address?, name: String, rating: String?, description: String?, instagram: String?, imageURL: String?, averageRate: String?, ordersCount: String?) {
            self.schedule = schedule
            self.address = address
            self.name = name
            self.description = description
            self.instagram = instagram
            self.imageURL = imageURL
            self.rating = rating
            self.averageRate = averageRate
            self.ordersCount = ordersCount
        }
        
    }
    
    final class WorkPlace: Codable {
        
        var restaurant: RestaurantModel
        let role: UserRole
        
        init(restaurant: RestaurantModel, role: UserRole) {
            self.restaurant = restaurant
            self.role = role
        }
        
    }
    
    final class WorkingTime: Codable {
        
        let startTime: String
        let endTime: String
        
        init(start: String, end: String) {
            self.startTime = start
            self.endTime = end
        }
    }
    
    final class WorkDay: Codable {
        
        let workPlace: WorkPlace
        let workingTime: WorkingTime
        
        init(place: WorkPlace, time: WorkingTime) {
            self.workPlace = place
            self.workingTime = time
        }
        
    }
    
}



