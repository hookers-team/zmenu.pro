//
//  Mix.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 16.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import Alamofire

struct MixRequest: Codable {
    
    struct Master: Codable {
        
        let masterId: String
        let name: String
        let imageURL: String?
        
    }
    
    var mixId: String?
    var name: String
    var imageURL: String?
    var price: String
    var categoryId: String
    var restaurantId: String
    var strength: MixStrenghtLevel
    var likes: String 
    var flawor: [Mix.Flawor]
    var description: String
    var bowl: String?
    var filling: String?
    var imageFile: Data?

}
