//
//  EmployeeListResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 03.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct EmployeeListRepsonse: Decodable {
    
    struct Data: Decodable {
        
        let employeesList: EmployeeList
        
    }
    
    let data: Data
    
}

final class EmployeeInfo: NSObject, Decodable {
    
    let employeeId: String
    let profile: UserInfo.Profile
    let workingTime: UserInfo.WorkingTime?

    init(employeeId: String, profile: UserInfo.Profile, workingTime: UserInfo.WorkingTime?) {
        self.employeeId = employeeId
        self.profile = profile
        self.workingTime = workingTime
    }
    
}

final class EmployeeList: NSObject, Decodable {
    
    let admin: [EmployeeInfo]
    let master: [EmployeeInfo]
    
    init(admin: [EmployeeInfo], master: [EmployeeInfo]) {
        self.admin = admin
        self.master = master
    }
    
}
