//
//  UserInfoNetworkResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct UserInfoNetworkResponse: Decodable {
    
    struct Data: Decodable {
        
        let userInfo: UserInfo
        
    }
    
    let reqId: String
    let action: String
    let data: Data
    
}
