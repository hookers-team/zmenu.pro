//
//  EditRestaurantProfileResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct EditRestaurantProfileResponse: Codable {
    
    struct Data: Codable {
        
        let restaurant: RestaurantModel
        
    }
    
    let action: String
    let reqId: String
    let data: Data
    
}
