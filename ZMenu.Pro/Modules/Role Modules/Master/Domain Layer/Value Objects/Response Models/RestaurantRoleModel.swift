//
//  RestaurantRoleModel.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 15.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

final class RestaurantModel: Codable {
    
    let restaurantId: String
    var name: String = "Лучший в городе".localized()
    var rating: String?
    let address: Address?
    let instagram: String?
    let schedule: Schedule?
    let description: String?
    var photos: [String]? = []
    
    init(restaurantId: String, name: String, rating: String?, address: Address?, instagram: String?, schedule: Schedule?, description: String?, photos: [String]) {
        
        self.restaurantId = restaurantId
        self.name = name
        self.address = address
        self.instagram = instagram
        self.schedule = schedule
        self.description = description
        self.photos = photos
        self.rating = rating
        
    }
    
    static func makeTestRestaurants() -> [RestaurantModel] {
        
        //        let rest1 = NetworkRestaurant(restaurantId: 1, name: " Place", likes: "4.73", workTimeString: "11:00 - 23:00", description: "Описание", photos: ["rest_default"], distanse: "11")
        //
        //        let rest2 = NetworkRestaurant(restaurantId: "2", name: "Хабл Бабл", likes: "4.66", workTimeString: "11:00 - 02:00", description: "Описание", photos: ["habl"], distanse: "19")
        //
        //        let rest3 = NetworkRestaurant(restaurantId: "3", name: "Голый Шеф", likes: "4.61", workTimeString: "11:00 - 23:00", description: "Описание", photos: ["goliyshef"], distanse: "9")
        //
        //        let rest4 = NetworkRestaurant(restaurantId: "4", name: "Паровоз", likes: "4.54", workTimeString: "11:00 - 00:00", description: "Описание", photos: ["parovoz"], distanse: "14")
        //
        //        let rest5 = NetworkRestaurant(restaurantId: "5", name: "Четверги", likes: "4.38", workTimeString: "19:00 - 06:00", description: "Описание", photos: ["chetvergi"], distanse: "6")
        //
        //
        //        return [rest1, rest2, rest3, rest4, rest5]
        
        return []
    }
    
}
