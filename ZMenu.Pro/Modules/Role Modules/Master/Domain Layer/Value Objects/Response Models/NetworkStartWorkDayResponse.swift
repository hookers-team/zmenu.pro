//
//  NetworkStartWorkDayResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 21.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct NetworkStartWorkDayResponse: Decodable {
    
    struct Data: Decodable {
        
        let workDay: UserInfo.WorkDay
        
    }
    
    let reqId: String
    let action: String
    let data: NetworkStartWorkDayResponse.Data
    
}
