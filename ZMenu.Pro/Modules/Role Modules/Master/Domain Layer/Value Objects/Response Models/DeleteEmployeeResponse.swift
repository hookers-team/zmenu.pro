//
//  DeleteEmployeeResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 06.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct DeleteEmployeeResponse: Decodable {
    
    struct Data: Decodable {
        
        let deletedEmployee: DeletedEmployee
        
    }
    
    let data: Data
    
}

struct DeletedEmployee: Decodable {
    
    let employeeId: String
    let role: UserRole
    
}
