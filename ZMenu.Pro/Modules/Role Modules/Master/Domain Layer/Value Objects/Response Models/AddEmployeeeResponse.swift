//
//  AddEmployeeeNetworkResponse.swift
//  ZMenu.Pro
//
//  Created by Stas Chelak on 1/21/19.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct AddEmployeeeResponse: Decodable {
    
    struct Data: Decodable {
        
        struct Profile: Decodable {
            let name: String?
        }
        
        let profile: Profile
    }
    
    let reqId: String
    let action: String
    let data: AddEmployeeeResponse.Data
    
}
