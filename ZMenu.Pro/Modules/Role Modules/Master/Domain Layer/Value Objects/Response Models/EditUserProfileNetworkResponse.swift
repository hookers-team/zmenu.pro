//
//  EditUserProfileNetworkResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 29.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct EditUserProfileNetworkResponse: Decodable {
    
    struct Data: Decodable {
        
        let profile: UserInfo.Profile
        
    }
    
    let reqId: String
    let action: String
    let data: Data
    
}
