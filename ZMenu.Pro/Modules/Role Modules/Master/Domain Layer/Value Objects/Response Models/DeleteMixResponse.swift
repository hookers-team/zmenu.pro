//
//  DeleteMixResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct DeleteMixResponse: Decodable {
    
    struct Data: Decodable {
        
        let mixId: String
        
    }
    
    let action: String
    let data: Data
    
}
