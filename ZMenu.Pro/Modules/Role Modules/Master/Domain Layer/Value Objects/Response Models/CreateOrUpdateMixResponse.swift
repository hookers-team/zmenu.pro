//
//  CreateOrUpdateMixResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 16.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct CreateOrUpdateMixResponse: Codable {
    
    struct Data: Codable {
        
        let mix: Mix
        
    }
    
    let action: String
    let reqId: String
    let data: Data
    
}
