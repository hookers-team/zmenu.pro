//
//  CreateRestaurantRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 29.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct CreateRestaurantRequest: Codable {
    
    struct Data: Codable {
        
        struct Restaurant: Codable {
            
            let address: Address?
            let name: String
            
        }
        
        let restaurant: Restaurant
        
    }
    
    let action: String
    let reqId: String
    let data: Data
    
}
