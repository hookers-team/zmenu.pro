//
//  DeleteEmployeeRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 06.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct DeleteEmployeeRequest: Encodable {
    
    struct Data: Encodable {
        
        let employeeId: String?
        let role: String
        let restaurantId: String
        
    }
    
    let data: Data
    
}
