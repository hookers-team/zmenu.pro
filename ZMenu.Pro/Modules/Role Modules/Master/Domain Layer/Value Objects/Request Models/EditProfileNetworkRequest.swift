//
//  EditProfileNetworkRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct EditProfileNetworkRequest: Encodable {
    
    struct Data: Encodable {
        
        let profile: UserInfo.Profile
        
    }
    
    let data: Data
    
}
