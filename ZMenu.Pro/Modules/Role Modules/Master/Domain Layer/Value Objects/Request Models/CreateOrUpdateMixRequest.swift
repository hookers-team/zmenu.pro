//
//  CreateOrUpdateMixRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 16.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct CreateOrUpdateMixRequest: Codable {
    
    struct Data: Codable {
        
        let mix: MixRequest
        
    }
    
    let reqId: String
    let action: String
    let data: Data
    
}
