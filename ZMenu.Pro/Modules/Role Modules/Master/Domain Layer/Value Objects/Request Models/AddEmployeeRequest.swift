//
//  AddEmployeeRequestModel.swift
//  ZMenu.Pro
//
//  Created by Chelak Stas on 2/1/19.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct AddEmployeeRequest: Codable {
    
    struct Data: Codable {
        
        let restaurantId: String
        let employeeUserId: String
        let role: String
        
    }
    
    let data: Data
    
}
