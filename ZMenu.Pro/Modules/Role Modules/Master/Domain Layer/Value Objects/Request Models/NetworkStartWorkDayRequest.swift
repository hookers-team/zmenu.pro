//
//  NetworkStartWorkDayRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 21.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct NetworkStartWorkDayRequest: Encodable {
    
    struct Data: Encodable {
        
        let restaurantId: String
        let role: UserRole.RawValue
        let endTime: String
        
    }
    
    let reqId: String
    let action: String
    let data: Data
    
}
