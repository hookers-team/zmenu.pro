//
//  UserRequestParametrs.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct UserRequestParametrs: NetworkRequestAdditionalURLParametersProviding, NetworkRequestAdditionalParametersProviding {
    
    let userId: String
    let token: String
 
    var parameters: Parameters {
        return ["userId": userId, "token" : token]
    }
    
}
