//
//  DeleteMixRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 23.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation


struct DeleteMixRequest: Codable {
    
    struct Data: Codable {
        
        let imageURL: String
        let mixId: String
        
    }
    
    let data: Data
    
}
