//
//  ScheduleModel.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation


struct Schedule: Codable {
        
    let monday: String?
    let tuesday: String?
    let wednesday: String?
    let thursday: String?
    let friday: String?
    
    let saturday: String?
    let sunday: String?

}
