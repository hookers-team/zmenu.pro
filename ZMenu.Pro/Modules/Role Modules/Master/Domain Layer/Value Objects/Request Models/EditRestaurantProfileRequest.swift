//
//  EditRestaurantProfileRequest.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct EditRestaurantProfileRequest: Codable {
    
    struct Data: Codable {
        
        let restaurant: RestaurantModel
        
    }
    
    let reqId: String
    let action: String
    let data: Data
    
}
