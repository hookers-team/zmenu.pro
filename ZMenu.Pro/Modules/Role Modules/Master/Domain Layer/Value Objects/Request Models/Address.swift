//
//  AddressModel.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct Address: Codable {

    let city: String
    let country: String
    let street: String
    let house: String
    let floor: String?
    
}
