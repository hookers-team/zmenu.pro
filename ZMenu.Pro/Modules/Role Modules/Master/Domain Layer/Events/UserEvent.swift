//
//  UserEvent.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 04.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct UserEvent {
    
    struct System {
        
        struct Logout: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
    }
    
}
