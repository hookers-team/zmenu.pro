//
//  MasterEvents.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 24.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

struct MasterEvents {
    
    struct NavigationEvent {
        
        struct CloseScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenMakeMixScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: UserInfo.WorkPlace
                let animated: Bool
            }
        }
        
        struct OpenMasterMainScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: UserInfo.WorkPlace
                let animated: Bool
            }
        }
        
        struct OpenEditMenuScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: UserInfo.WorkPlace
                let animated: Bool
            }
        }
        
        struct OpenEditMixScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: UserInfo.WorkPlace
                let mix: Mix
            }
            
        }
        
        struct OpenStartWorkDayScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
                let workPlace: UserInfo.WorkPlace
            }
        }
        
        struct OpenEditProfileScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenSelectWorkingPlaceScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenMyQrScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
    }
    
}
