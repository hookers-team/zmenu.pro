//
//  OwnerEvents.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct OwnerEvents {
    
    struct NavigationEvent {
        
        struct CloseScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenCreateRestaurantScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenMakeMixScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenEditMenuScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenEditMixScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let mix: Mix
            }
            
        }
        
        struct OpenWorkPlaceMainScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: UserInfo.WorkPlace
                let animated: Bool
            }
        }
        
        struct OpenEditProfileScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: UserInfo.WorkPlace
                let animated: Bool
            }
        }
        
        struct OpenWorkingTimeScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: RestaurantModel
                let animated: Bool
            }
        }
        
        struct OpenAddressScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let workPlace: RestaurantModel
                let animated: Bool
            }
        }
        
        struct OpenQrScannerScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
                let role: QRService.Role
                let restaurantId: String
            }
        }
        
        struct OpenEmployeeList: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
                let workPlace: UserInfo.WorkPlace
            }
        }
        
    }
    
}
