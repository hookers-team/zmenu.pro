//
//  EmployeeCoordinator.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class EmployeeCoordinator: TabBarEmbedCoordinator {

    fileprivate var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    private var ordersListCoordinator: OrdersListCoordinator?
    
    var userSessionService: UserSessionService!
    
    fileprivate var dispatcher: Dispatcher {
        return context.dispatcher
    }
    
    fileprivate var root: UINavigationController!
    
    init(context: CoordinatingContext) {
        let homeImage = UIImage(named: "home")
        let homeImageActive = UIImage(named: "home_active")
        let tabItemInfo = TabBarItemInfo(
            title: nil,
            icon: homeImage,
            highlightedIcon: homeImageActive)
        
        super.init(context: context, tabItemInfo: tabItemInfo)
    }
    
    override func prepareForStart() {
        super.prepareForStart()
        
        register()
        
        let employeeNetworkService = EmployeeNetworkService(networkService: context.networkService)
        employeeStore = EmployeeStore(networkService: employeeNetworkService, dispatcher: context.dispatcher, userSessionService: userSessionService, permissionService: context.permissionService)
        
        //KS: TODO: Should move on appCoordinator level and managing the modules.
        if let user = userSessionService.accountModel, user.type == AccountType.employee.rawValue {
            openSelectWorkingPlaceViewController(animated: true)
        } else {
            openOwnerWorkPlacesViewController(animated: true)
        }
    }
  
    override func createFlow() -> UIViewController {
        return root
    }
    
}

extension EmployeeCoordinator {
    
    private func openStartWorkDayViewController(animated: Bool, workPlace: UserInfo.WorkPlace) {
        let startWorkDayViewController = UIStoryboard.MasterFlow.startWorkDayViewController
        
        startWorkDayViewController.employeeStore = employeeStore
        startWorkDayViewController.styleguide = context.styleguide
        startWorkDayViewController.dispatcher = context.dispatcher
        startWorkDayViewController.currentWorkPlace = workPlace
        
        let navBarOnModal: UINavigationController = UINavigationController(rootViewController: startWorkDayViewController)
        
        root.present(navBarOnModal, animated: true, completion: nil)
    }
    
    private func openEmployeeMainViewController(animated: Bool, workPlace: UserInfo.WorkPlace) {
        let controller = UIStoryboard.MasterFlow.employeeMainViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.currentWorkPlace = workPlace
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openEditMenuViewController(animated: Bool, currentWorkPlace: UserInfo.WorkPlace) {
        let controller = UIStoryboard.MasterFlow.menuViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.currentWorkPlace = currentWorkPlace
        
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openEmployeeListViewController(animated: Bool, currentWorkPlace: UserInfo.WorkPlace) {
        let controller = UIStoryboard.MasterFlow.employeeListViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.currentWorkPlace = currentWorkPlace
        
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openMakeMixViewController(animated: Bool, currentWorkPlace: UserInfo.WorkPlace) {
        let controller = UIStoryboard.MasterFlow.makeMixViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.currentWorkPlace = currentWorkPlace.restaurant
        
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openSelectWorkingPlaceViewController(animated: Bool) {
        let controller = UIStoryboard.MasterFlow.selectWorkingPlaceViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        
        root = UINavigationController(rootViewController: controller)
        root.tabBarController?.tabBar.isHidden = true
    }
    
    private func openEditEmployeeProfileViewController(animated: Bool) {
        let controller = UIStoryboard.MasterFlow.editEmployeeProfileViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openEditMixViewController(with mix: Mix, currentWorkPlace: UserInfo.WorkPlace) {
        let controller = UIStoryboard.MasterFlow.makeMixViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.editedMix = mix
        controller.currentWorkPlace = currentWorkPlace.restaurant
        
        
        root.pushViewController(controller, animated: true)
    }
    
    private func openCreatedMixViewController(with mix: Mix) {
        let controller = UIStoryboard.MasterFlow.createdMixViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.createdMix = mix
        controller.employeeStore = employeeStore
        
        
        root.pushViewController(controller, animated: true)
    }
    
}

extension EmployeeCoordinator {
    //KS: TODO: Move into another module
    
    private func openEditRestaurantProfileViewController(animated: Bool, workPlace: UserInfo.WorkPlace) {
        let controller = UIStoryboard.MasterFlow.editRestaurantProfileViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.workPlace = workPlace
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openRestaurantMainViewController(animated: Bool, selectedWorkPlace: UserInfo.WorkPlace) {
        let controller = UIStoryboard.MasterFlow.restaurantMainViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.currentWorkPlace = selectedWorkPlace
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openAddressViewController(animated: Bool, workPlace: RestaurantModel) {
        let controller = UIStoryboard.MasterFlow.addressViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.workPlace = workPlace
        
        let navBarOnModal: UINavigationController = UINavigationController(rootViewController: controller)
        
        
        root.present(navBarOnModal, animated: true, completion: nil)
    }
    
    private func openWorkingTimeViewController(animated: Bool, workPlace: RestaurantModel) {
        let controller = UIStoryboard.MasterFlow.workingTimeViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        controller.workPlace = workPlace
        
        let navBarOnModal: UINavigationController = UINavigationController(rootViewController: controller)
    
        
        root.present(navBarOnModal, animated: true, completion: nil)
    }
    
    private func openQrCodeScannerViewController(animated: Bool, role: QRService.Role, restaurantId: String) {
        let controller = UIStoryboard.MasterFlow.qrScannerViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.service = QRService(role: role, employeeStore: employeeStore, restaurantId: restaurantId)
        
        let navBarOnModal: UINavigationController = UINavigationController(rootViewController: controller)
        
        root.present(navBarOnModal, animated: true, completion: nil)
    }
    
    private func openMyQrCodeViewController(animated: Bool) {
        let controller = UIStoryboard.MasterFlow.qrViewController
        
        controller.dispatcher = context.dispatcher
        controller.employeeStore = employeeStore
        controller.styleguide = context.styleguide
        
        let nav = UINavigationController(rootViewController: controller)
        
        root.present(nav, animated: true, completion: nil)
    }
    
    private func openCreateRestaurantViewController(animated: Bool) {
        let controller = UIStoryboard.MasterFlow.createRestaurantViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        
        let navBarOnModal: UINavigationController = UINavigationController(rootViewController: controller)
        
        root.present(navBarOnModal, animated: true, completion: nil)
    }
    
    private func openOwnerWorkPlacesViewController(animated: Bool) {
        let controller = UIStoryboard.MasterFlow.ownerWorkPlacesViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.employeeStore = employeeStore
        
        root = UINavigationController(rootViewController: controller)
    }
    
}


extension EmployeeCoordinator {
    
    func registerOrderInfo() {
        context.dispatcher.register(type: OrdersEvent.NavigationEvent.OrderInfo.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.showOrderListFlow(workPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenOrderListScreen() {
        context.dispatcher.register(type: OrdersEvent.NavigationEvent.ShowOrderList.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.showOrderListFlow(workPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func showOrderListFlow(workPlace: UserInfo.WorkPlace, navigationController: UINavigationController? = nil) {
        ordersListCoordinator = OrdersListCoordinator(context: context, workPlace: workPlace)
        ordersListCoordinator?.prepareForStart()
        
        guard let serviceViewController = ordersListCoordinator?.createFlow() else { return }
        
        root.showDetailViewController(serviceViewController, sender: nil)
    }
    
}

extension EmployeeCoordinator {
    
    func deviceTokenUpdate(deviceToken: String) {
        employeeStore.deviceTokenUpdate(deviceToken: deviceToken)
    }
    
}


extension EmployeeCoordinator {
    
    private func register() {
        registerCloseScreenOwner()
        registerCloseScreen()
        registerOpenMakeMixScreen()
        registerOpenStartWorkDayScreen()
        registerOpenEditMenuScreen()
        registerOpenEditMixScreen()
        registerOpenEditEmployeeProfileScreen()
        registerOpenMasterMainScreen()
        
        registerOpenAddressScreen()
        registerOpenWorkingTimeScreen()
        registerOpenRestaurantMainScreen()
        registerEditRestorantProfileScreen()
        registerOpenEmployeeList()
        
        registerOpenQrScannerScreen()
        registerOpenMyQrScreen()
        
        registerOpenCreateRestaurantScreen()
        registerOpenOrderListScreen()
        
        registerOrderListScreen()
        registerOrderInfo()
    }
    
    private func registerOrderListScreen() {
        dispatcher.register(type: OrdersEvent.NavigationEvent.CloseScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                if self?.root.presentedViewController != nil {
                    self?.root.dismiss(animated: box.animated, completion: nil)
                } else {
                    self?.root.popViewController(animated: true)
                }
            case .failure(_):
                break
            }
        }
    }
    
    private func registerCloseScreenOwner() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.CloseScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                if self?.root.presentedViewController != nil {
                    self?.root.dismiss(animated: box.animated, completion: nil)
                } else {
                    self?.root.popViewController(animated: true)
                }
            case .failure(_):
                break
            }
        }
    }
    
    private func registerCloseScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.CloseScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                if self?.root.presentedViewController != nil {
                    self?.root.dismiss(animated: box.animated, completion: nil)
                } else {
                    self?.root.popViewController(animated: true)
                }
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenMasterMainScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.OpenMasterMainScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openEmployeeMainViewController(animated: box.animated, workPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenEditMenuScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.OpenEditMenuScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openEditMenuViewController(animated: box.animated, currentWorkPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenEmployeeList() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.OpenEmployeeList.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openEmployeeListViewController(animated: box.animated, currentWorkPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenStartWorkDayScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.OpenStartWorkDayScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openStartWorkDayViewController(animated: box.animated, workPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenEditMixScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.OpenEditMixScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openEditMixViewController(with: box.mix, currentWorkPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenMakeMixScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.OpenMakeMixScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openMakeMixViewController(animated: box.animated, currentWorkPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenEditEmployeeProfileScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.OpenEditProfileScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openEditEmployeeProfileViewController(animated: box.animated)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenMyQrScreen() {
        dispatcher.register(type: MasterEvents.NavigationEvent.OpenMyQrScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openMyQrCodeViewController(animated: box.animated)
                
            case .failure(_):
                break
            }
        }
    }
    
}

extension EmployeeCoordinator {
    
    private func registerOpenRestaurantMainScreen() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.OpenWorkPlaceMainScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openRestaurantMainViewController(animated: box.animated, selectedWorkPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerEditRestorantProfileScreen() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.OpenEditProfileScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openEditRestaurantProfileViewController(animated: box.animated, workPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenWorkingTimeScreen() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.OpenWorkingTimeScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openWorkingTimeViewController(animated: box.animated, workPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenAddressScreen() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.OpenAddressScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openAddressViewController(animated: box.animated, workPlace: box.workPlace)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenQrScannerScreen() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.OpenQrScannerScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openQrCodeScannerViewController(animated: box.animated, role: box.role, restaurantId: box.restaurantId)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenCreateRestaurantScreen() {
        dispatcher.register(type: OwnerEvents.NavigationEvent.OpenCreateRestaurantScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openCreateRestaurantViewController(animated: box.animated)
            case .failure(_):
                break
            }
        }
    }
    
}

extension EmployeeCoordinator: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
        
        if let change = change as? EmployeeStoreStateChangeWithData {
            employeeStoreStateChangeWithData(change: change)
        }
    }
    
    private func employeeStoreStateChangeWithData(change: EmployeeStoreStateChangeWithData) {
        switch change {
        case .createOrUpdate(let mix):
            root.popViewController(animated: false)
            
            self.openCreatedMixViewController(with: mix)
        default: break
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            if let topViewController = root.topViewController, !(topViewController is CTAButtonConfiguring) {
                
                employeeStore.isLoading ? root.topViewController?.showSpinner() : root.topViewController?.hideSpinner()
            }
        }
        
    }
    
}
