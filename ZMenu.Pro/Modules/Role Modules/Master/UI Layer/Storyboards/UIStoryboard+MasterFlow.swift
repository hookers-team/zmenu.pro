//
//  UIStoryboard+Restaurants.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

fileprivate enum MasterStoryboardControllerId: String {
    
    case startWorkDay = "StartWorkDayViewControllerStoryboarId"
    case masterMain = "EmployeeMainViewControllerStoryboardId"
    case makeMix = "MakeMixViewControllerStoryboarId"
    case menuViewController = "MenuViewControllerStoryboarId"
    case createdMixViewController = "CreatedMixViewControllerStoryboardId"
    case selectWorkingPlace = "SelectWorkingPlaceViewControllerStoryboardId"
    case editEmployeeProfile = "EditEmployeeProfileViewControllerStoryboardId"
    
    case restaurantMain = "RestaurantMainViewControllerStoryboardId"
    case editRestorantProfile = "EditRestorantProfileViewControllerStoryboardId"
    case workingTimeViewController = "WorkingTimeViewControllerStoryboardId"
    case address = "AddressViewControllerStoryboardId"
    
    case qrScanner = "QRScannerControllerStoryboardId"
    case qr = "QRViewControllerStoryboardId"
    
    case createRestaurant = "CreateRestaurantViewControllerStoryboardId"
    case ownerWorkPlaces = "OwnerWorkPlacesViewControllerStoryboardId"
    case employeeList = "EmployeeListViewControllerStoryboardId"

    
    case masterInfoViewController = "MasterInfoViewControllerStoryboarId"
    case restaurantInfoViewController = "RestaurantInfoViewControllerStoryboarId"
    
}

extension UIStoryboard {
    
    static var masterFlowStoryboard: UIStoryboard {
        return UIStoryboard(name: "MasterFlow", bundle: nil)
    }
    
    struct MasterFlow {
        
        static var selectWorkingPlaceViewController: SelectWorkingPlaceViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.selectWorkingPlace.rawValue) as! SelectWorkingPlaceViewController
        }
        
        static var qrViewController: QRViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.qr.rawValue) as! QRViewController
        }
        
        static var startWorkDayViewController: StartWorkDayViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.startWorkDay.rawValue) as! StartWorkDayViewController
        }
        
        static var employeeMainViewController: EmployeeMainViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.masterMain.rawValue) as! EmployeeMainViewController
        }
        
        static var menuViewController: MenuViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.menuViewController.rawValue) as! MenuViewController
        }
        
        static var makeMixViewController: MakeMixViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.makeMix.rawValue) as! MakeMixViewController
        }
        
        static var editEmployeeProfileViewController: EditEmployeeProfileViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.editEmployeeProfile.rawValue) as! EditEmployeeProfileViewController
        }
        
        static var createdMixViewController: CreatedMixViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.createdMixViewController.rawValue) as! CreatedMixViewController
        }
        
        
        //KS: TODO: Should move it to enother module
        
        static var ownerWorkPlacesViewController: OwnerWorkPlacesViewController {
             return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.ownerWorkPlaces.rawValue) as! OwnerWorkPlacesViewController
        }
        
        static var editRestaurantProfileViewController: EditRestaurantProfileViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.editRestorantProfile.rawValue) as! EditRestaurantProfileViewController
        }
        
        static var workingTimeViewController: WorkingTimeViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.workingTimeViewController.rawValue) as! WorkingTimeViewController
        }
        
        static var addressViewController: AddressViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.address.rawValue) as! AddressViewController
        }
        
        static var createRestaurantViewController: CreateRestaurantViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.createRestaurant.rawValue) as! CreateRestaurantViewController
        }
        
        static var restaurantMainViewController: RestaurantMainViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.restaurantMain.rawValue) as! RestaurantMainViewController
        }
        
        static var qrScannerViewController: QRScannerViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.qrScanner.rawValue) as! QRScannerViewController
        }
        
        static var employeeListViewController: EmployeeListViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.employeeList.rawValue) as! EmployeeListViewController
        }
        
        static var masterInfoViewController: MasterInfoViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.masterInfoViewController.rawValue) as! MasterInfoViewController
        }
        
        static var restaurantInfoViewController: RestaurantInfoViewController {
            return UIStoryboard.masterFlowStoryboard.instantiateViewController(withIdentifier: MasterStoryboardControllerId.restaurantInfoViewController.rawValue) as! RestaurantInfoViewController
        }
        
    }
    
}
