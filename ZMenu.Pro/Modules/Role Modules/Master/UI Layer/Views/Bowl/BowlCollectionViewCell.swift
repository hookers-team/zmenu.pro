//
//  BowlCollectionViewCell.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 27.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class BowlCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabel.addDefaultSoftShadow()
        containerView.layer.cornerRadius = 12
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: CollectionViewTransformConstants.scaleFactor, y: CollectionViewTransformConstants.scaleFactor)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 1, y: 1)
                }, completion: nil)
            }
        }
    }
    
}

struct CollectionViewTransformConstants {
    
    static let scaleFactor = CGFloat(1.1)
    
}
