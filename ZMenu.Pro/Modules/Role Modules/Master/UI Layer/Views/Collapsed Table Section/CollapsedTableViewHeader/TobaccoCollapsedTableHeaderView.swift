//
//  TobaccoCollapsedTableHeaderView.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 11.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol TobaccoCollapsedTableHeaderViewDelegate: class {
    
    func toggleSection(_ header: TobaccoCollapsedTableHeaderView, section: Int)
    
}


final class TobaccoCollapsedTableHeaderView: UITableViewHeaderFooterView, NibReusable {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var arrowLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    weak var delegate: TobaccoCollapsedTableHeaderViewDelegate? {
        didSet {
            addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TobaccoCollapsedTableHeaderView.tapHeader(_:))))
        }
    }
    
    var section: Int = 0
    
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? TobaccoCollapsedTableHeaderView else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        arrowLabel.rotate(collapsed ? 0.0 : .pi )
    }

}
