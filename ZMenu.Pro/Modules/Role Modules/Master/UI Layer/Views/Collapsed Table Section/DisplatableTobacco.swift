//
//  DisplatableTobacco.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 13.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

final class DisplayableTobacco: NSCopying {
    
    var name: String
    var detail: String
    var brand: String
    
    public init(name: String, detail: String, sort: String) {
        self.name = name
        self.detail = detail
        self.brand = sort
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = DisplayableTobacco(name: name, detail: detail, sort: brand)
        
        return copy
    }
    
}

final class DisplayableTobaccoSort: NSCopying {
    
    var name: String
    var items: [DisplayableTobacco]
    var collapsed: Bool
    
    public init(name: String, items: [DisplayableTobacco], collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = DisplayableTobaccoSort(name: name, items: items, collapsed: collapsed)
        
        return copy
    }
    
}
