//
//  EmployeeTableViewCell.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 03.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

protocol EmployeeTableViewCellDelegate: class {
    func employeeTableViewCellDidTapRemoveButton(_ cell: EmployeeTableViewCell)
}

final class EmployeeTableViewCell: UITableViewCell, NibReusable {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    weak var delegate: EmployeeTableViewCellDelegate?
    

    @IBAction func remove(_ sender: Any) {
        delegate?.employeeTableViewCellDidTapRemoveButton(self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
