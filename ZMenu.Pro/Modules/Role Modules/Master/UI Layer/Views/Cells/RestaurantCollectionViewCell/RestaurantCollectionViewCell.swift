//
//  RestaurantCollectionViewCell.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 14.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit


protocol RestaurantCollectionViewCellDelegate: class {
    
    func didTapRestaurantInfoButton(on cell: UICollectionViewCell)
    func didSelectRestautant(cell: UICollectionViewCell)
}

final class RestaurantCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet private weak var infoButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var infoButtonBlurView: UIVisualEffectView!
    @IBOutlet weak var distanceBlurView: UIVisualEffectView!
    @IBOutlet weak var likesCountBlurView: UIVisualEffectView!
    @IBOutlet weak var nameBlurView: UIVisualEffectView!
    
    weak var delegate: RestaurantCollectionViewCellDelegate!
    weak var styleguide: DesignStyleGuide!
    private var photos: [String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameBlurView.layer.cornerRadius = 6
        distanceBlurView.layer.cornerRadius = 6
        likesCountBlurView.layer.cornerRadius = 6
        infoButtonBlurView.layer.cornerRadius = 0.5 * infoButton.bounds.size.width
        
        containerView.layer.borderWidth = 1
        
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
        photosCollectionView.registerReusableCell(cellType: RestaurantImageCollectionViewCell.self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        photosCollectionView.contentOffset.x = 0
        pageControl.currentPage = 0
    }
    
    @IBAction func restaurantInfo(_ sender: Any) {
        delegate.didTapRestaurantInfoButton(on: self)
    }
    
    func set(photos: [String]) {
        self.photos = photos
        pageControl.numberOfPages = photos.count
        pageControl.isHidden = photos.count < 2
        photosCollectionView.reloadData()
    }
    
}

extension RestaurantCollectionViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        nameLabel.textColor = styleguide.labelTextColor
        distanceLabel.textColor = styleguide.labelTextColor
        likeCountLabel.textColor = styleguide.labelTextColor
        photosCollectionView.layer.cornerRadius = styleguide.cornerRadius
        containerView.layer.cornerRadius = styleguide.cornerRadius
        blurView.layer.cornerRadius = styleguide.cornerRadius
    }
    
}

//MARK: - UIScrollViewDelegate
extension RestaurantCollectionViewCell: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.x
        
        let pageWidth = photosCollectionView.bounds.width
        
        pageControl.currentPage = Int((currentOffset + pageWidth / 2) / pageWidth)
    }
    
}

//MARK: - UICollectionViewDataSource
extension RestaurantCollectionViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count > 0 ? photos.count : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: RestaurantImageCollectionViewCell.self)
        
        let defaultImage = UIImage(named: "rest_default")
        
        if photos.count == 0 {
            cell.imageView.image = defaultImage
        } else {
            cell.imageView.download(image: photos[indexPath.item], placeholderImage: defaultImage)
        }
        
        return cell
    }
    
}

//MARK: - UICollectionViewDelegateFlowLayout
extension RestaurantCollectionViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return containerView.bounds.size
    }
    
}

//MARK: - UICollectionViewDelegate
extension RestaurantCollectionViewCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.didSelectRestautant(cell: self)
    }
    
}

