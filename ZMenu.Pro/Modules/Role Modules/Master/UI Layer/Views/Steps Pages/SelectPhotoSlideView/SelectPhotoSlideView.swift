//
//  SelectPhotoSlideView.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit
import WebKit

protocol SelectPhotoSlideViewDelegate: class {
    
    func selectPhotoSlideView(_  selectPhotoSlideView: SelectPhotoSlideView, didChoose strenghtLevel: MixCategorizedData)
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didTapGalleryPhotoButton button: UIButton)
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didTapCameraButton button: UIButton)
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didTapPhotoBrowserButton button: UIButton)
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didFindImage image: UIImage)
    func didCloseWebView(_ selectPhotoSlideView: SelectPhotoSlideView)
    
}

final class SelectPhotoSlideView: UIView, NibLoadable, Slide, WKUIDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet var browserButtonContainer: UIView!
    @IBOutlet var galleryButtonContainer: UIView!
    @IBOutlet var cameraButtonContainer: UIView!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet var closeWebViewButton: UIButton!
    @IBOutlet var webViewContainer: UIView!
    private var webView: WKWebView?
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var stepLabel: UILabel!
    weak var delegate: SelectPhotoSlideViewDelegate?
    
    var currentStrenghtLevel: MixCategorizedData {
        return pickerData[pickerView.selectedRow(inComponent: 0)]
    }
    
    private let pickerData: [MixCategorizedData] = [("Легкий (Serbetly, Adalya)".localized(), "light"),
                                                    ("Средний (Serbetly + Nakhla)".localized(), "semiLight"),
                                         ("Полутяжелый (Nakhla, Fasil)".localized(), "middle"),
                                         ("Тяжелый (Tangiers, Dark Side)".localized(), "heavy"),
                                         ("Очень тяжелый (Tangiers F-line, overpack)".localized(), "superHeavy")]
    
    var isImageChanged = false
    
    var mixName: String?
    
    @IBAction func openPhotoBrowser(_ sender: UIButton) {
        delegate?.selectPhotoSlideView(self, didTapPhotoBrowserButton: sender)
        
        openWebView()
    }
    @IBAction func closeWebView(sender: AnyObject) {
        webView?.removeFromSuperview()
        closeWebViewButton.isHidden = true
        webViewContainer.isUserInteractionEnabled = false
        stepLabel.text = "5. Крепость микса".localized()
        delegate?.didCloseWebView(self)
    }
    
    @IBAction func openGalleryPhoto(sender: UIButton) {
        delegate?.selectPhotoSlideView(self, didTapGalleryPhotoButton: sender)
    }
    
    @IBAction func cameraPhoto(sender: UIButton) {
        delegate?.selectPhotoSlideView(self, didTapCameraButton: sender)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        print("tapped")
        
        if gestureRecognizer.isKind(of: UITapGestureRecognizer.self) {
            let touchPoint = touch.location(in: webView)
            
            let imgUrl = "document.elementFromPoint(\(touchPoint.x), \(touchPoint.y)).src"
            
            webView?.evaluateJavaScript(imgUrl) { (url, error) in
                guard let urlToSave = url as? String,
                    let imageUrl = URL(string: urlToSave),
                    let imageData = try? Data.init(contentsOf: imageUrl, options: Data.ReadingOptions.mappedIfSafe),
                    let image = UIImage(data: imageData) else { return }
                
                self.isImageChanged = true
                self.delegate?.selectPhotoSlideView(self, didFindImage: image)
            }
        }
        
        return true
    }
    
    func openWebView() {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: containerView.frame, configuration: webConfiguration)
        let gesture = UITapGestureRecognizer(target: webView, action: nil)
        
        gesture.delegate = self
        
        webView.addGestureRecognizer(gesture)
        
        webView.uiDelegate = self
        webViewContainer.addSubview(webView)
        webView.isHidden = false
        webViewContainer.isUserInteractionEnabled = true
        
        let mixNamePath = mixName?.replacingOccurrences(of: " ", with: "+") ?? ""
        
        let urlstr = "https://www.google.com/search?as_st=y&tbm=isch&as_q=\(mixNamePath)&as_epq=&as_oq=&as_eq=&cr=&as_sitesearch=&safe=images&tbs=sur:fc".encodeUrl
        let url = URL.init(string: urlstr)
        let request = URLRequest(url: url!)
        
        closeWebViewButton.isHidden = false
        containerView.isUserInteractionEnabled = true
        
        webView.load(request)
        
        self.webView = webView
        stepLabel.text = "1. Тапните на фото, что бы оно открылось снизу на веб-странице. 2. Если картинка подгрузится в лучшем качестве. 3. Тапните на нее снова.".localized()
    }
    
    func createURLWithComponents() -> URL {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.google.com"
        urlComponents.path = "/search?as_st=y&tbm=isch&as_q=яблочный+пирог&as_epq=&as_oq=&as_eq=&cr=&as_sitesearch=&safe=images&tbs=sur:fc"
        
        return urlComponents.url!
    }
    
    func configurate(with styleguide: DesignStyleGuide, delegate: UITextFieldDelegate) {
        stepLabel.textColor = styleguide.labelTextColor
        
        browserButtonContainer.layer.cornerRadius = 12
        browserButtonContainer.layer.borderColor = UIColor.white.cgColor
        browserButtonContainer.layer.borderWidth = 2
        
        cameraButtonContainer.layer.cornerRadius = 12
        cameraButtonContainer.layer.borderColor = UIColor.white.cgColor
        cameraButtonContainer.layer.borderWidth = 2
        
        galleryButtonContainer.layer.cornerRadius = 12
        galleryButtonContainer.layer.borderColor = UIColor.white.cgColor
        galleryButtonContainer.layer.borderWidth = 2
        
    }
    
}

extension SelectPhotoSlideView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel { label = view }
        else { label = UILabel() }
        
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17)
        label.adjustsFontSizeToFitWidth = true
        label.text = pickerData[row].categoryName
        label.textColor = .white
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].categoryName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        delegate?.selectPhotoSlideView(self, didChoose: pickerData[row])
    }
    
    func pickStrenghteLevel(_ strenghtLevel: String) {
        guard let index = pickerData.firstIndex(where: {$0.categoryId == strenghtLevel}) else { return }
        
        pickerView.selectRow(index, inComponent: 0, animated: true)
        delegate?.selectPhotoSlideView(self, didChoose: pickerData[index])
    }
    
}
