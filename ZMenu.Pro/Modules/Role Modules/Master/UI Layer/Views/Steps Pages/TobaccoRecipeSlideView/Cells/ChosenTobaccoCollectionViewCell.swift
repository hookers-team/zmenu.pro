//
//  ChosenTobaccoCollectionViewCell.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 30.12.2018.
//  Copyright © 2018 Skaix. All rights reserved.
//

import UIKit

protocol ChosenTobaccoCollectionViewCellDelegate: class {
    
    func chosenTobaccoCollectionViewCellDidTapRemoveButton(_ cell: ChosenTobaccoCollectionViewCell)
    
}

final class ChosenTobaccoCollectionViewCell: UICollectionViewCell, NibReusable {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    weak var delegate: ChosenTobaccoCollectionViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containerView.layer.cornerRadius = 6
    }
    
    @IBAction func removeButton(_ sender: Any) {
        delegate?.chosenTobaccoCollectionViewCellDidTapRemoveButton(self)
    }
    
}
