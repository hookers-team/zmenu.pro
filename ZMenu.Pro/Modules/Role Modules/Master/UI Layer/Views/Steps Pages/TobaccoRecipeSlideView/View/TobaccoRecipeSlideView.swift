//
//  TobaccoRecipeSlideView.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol TobaccoRecipeSlideViewDelegate: class {
    
    func flaworRecipeSlideView(_ flaworRecipeSlideView: TobaccoRecipeSlideView, updateTobaccos flawors: [DisplayableTobacco])
    
}

final class TobaccoRecipeSlideView: UIView, NibLoadable, Slide {
    
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var chosenTobaccoCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var chosenTobaccoCollectionView: UICollectionView!
    
    private var isSearching = false
    
    weak var delegate: TobaccoRecipeSlideViewDelegate?
    private var styleguide: DesignStyleGuide!
    
    private var searchController: UISearchBar!
    private var chosenTobaccoCollectionViewService: ChosenTobaccoCollectionViewService!
    
    fileprivate var sectionsState = [Int : Bool]()
    fileprivate var sectionRowsHeightAmount = [Int : CGFloat]()
    
    private var currentTobaccos: [DisplayableTobaccoSort] {
        return isSearching ? filtredTobaccos : flawors
    }
    
    private let flawors = Flawors.getTobaccos().map { flawors -> DisplayableTobaccoSort in
        DisplayableTobaccoSort(name: flawors.flaworBrand, items: flawors.flaworSorts.map{DisplayableTobacco(name: $0.name, detail: $0.enName, sort: flawors.flaworBrand)})
    }
    
    private var filtredTobaccos: [DisplayableTobaccoSort] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    private func configurateTableView() {
        let footerView = UIView()
        let backgroundView = UIView()
        
        backgroundView.backgroundColor = .clear
        backgroundView.layer.cornerRadius = styleguide.cornerRadius
        
        tableView.tableFooterView = footerView
        tableView.backgroundColor = .clear
        tableView.backgroundView = backgroundView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.layer.cornerRadius = styleguide.cornerRadius
        searchController = makeSearchController()
        
        tableView.tableHeaderView = searchController
        
        chosenTobaccoCollectionViewService = ChosenTobaccoCollectionViewService(colletionView: chosenTobaccoCollectionView)
        chosenTobaccoCollectionViewService.configurate(with: self)
    }
    
    private func makeSearchController() -> UISearchBar {
        let searchBar = UISearchBar()
        
        searchBar.delegate = self
        searchBar.showsCancelButton = true
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        searchBar.backgroundColor = .clear
        searchBar.placeholder = "Поиск табака".localized()
        searchBar.tintColor = styleguide.primaryColor
        searchBar.sizeToFit()
        
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        let clearButton = textFieldInsideSearchBar?.value(forKey: "clearButton") as! UIButton
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        
        textFieldInsideSearchBar?.textColor = styleguide.labelTextColor
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.tintColor = styleguide.primaryColor
        
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = styleguide.primaryColor
        
        return searchBar
    }
    
    func configurate(with styleguide: DesignStyleGuide, delegate: TobaccoRecipeSlideViewDelegate) {
        self.styleguide = styleguide
        self.delegate = delegate
        
        stepLabel.textColor = styleguide.labelTextColor
        
        configurateTableView()
    }
    
    func configurate(with flawors: [Mix.Flawor]) {
        let displayableTobaccos = flawors.map{DisplayableTobacco(name: $0.sort, detail: $0.sort, sort: $0.brand)}
        
        displayableTobaccos.forEach{
            addFlaworToRecipe(flawor: $0)
        }
    }
    
    func currentTobaccosList() -> [Mix.Flawor] {
        return chosenTobaccoCollectionViewService.flawors.map{Mix.Flawor(brand: $0.brand, sort: $0.name)}
    }
    
    func addFlaworToRecipe(flawor: DisplayableTobacco) {
        chosenTobaccoCollectionViewService.addTobacco(flawor)
        delegate?.flaworRecipeSlideView(self, updateTobaccos: chosenTobaccoCollectionViewService.flawors)
        
        guard chosenTobaccoCollectionViewHeightConstraint.constant < 60.0 else { return }
        
        updateChosenTobaccoCollectionViewHeight()
    }
    
    func updateChosenTobaccoCollectionViewHeight() {
        UIView.animate(withDuration: 0.3) {
            let coefficient: CGFloat = self.chosenTobaccoCollectionViewService.flawors.count > 3 ? 2 : 1.2
            
            let heightConstant: CGFloat = self.chosenTobaccoCollectionViewService.flawors.count == 0 ? 0 : 30.0
            
            self.chosenTobaccoCollectionViewHeightConstraint.constant = heightConstant * coefficient
            
            self.layoutIfNeeded()
        }
    }
    
}

extension TobaccoRecipeSlideView: ChosenTobaccoServiceDelegate {
 
    func chosenTobaccoCollectionViewService(_ chosenTobaccoCollectionViewService: ChosenTobaccoCollectionViewService, removeTobacco flawor: DisplayableTobacco) {
        updateChosenTobaccoCollectionViewHeight()
        
        delegate?.flaworRecipeSlideView(self, updateTobaccos: chosenTobaccoCollectionViewService.flawors)
    }
    
}

extension TobaccoRecipeSlideView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return currentTobaccos.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentTobaccos[section].collapsed ? 0 : currentTobaccos[section].items.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let flawor = currentTobaccos[indexPath.section].items[indexPath.row]
        
        addFlaworToRecipe(flawor: flawor)
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
            CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
        
        let flawor = currentTobaccos[indexPath.section].items[indexPath.row]
        
        cell.nameLabel.text = flawor.name
        cell.detailLabel.text = flawor.detail == flawor.name ? nil : flawor.detail
        cell.backgroundColor  = .clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = TobaccoCollapsedTableHeaderView.instantiateFromNib()
        
        header.nameLabel.text = currentTobaccos[section].name
        header.setCollapsed(currentTobaccos[section].collapsed)
        
        header.section = section
        header.delegate = self
        
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
}

extension TobaccoRecipeSlideView: TobaccoCollapsedTableHeaderViewDelegate {
    
    func toggleSection(_ header: TobaccoCollapsedTableHeaderView, section: Int) {
        let collapsed = !currentTobaccos[section].collapsed
        
        currentTobaccos[section].collapsed = collapsed
        header.setCollapsed(collapsed)
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}

extension TobaccoRecipeSlideView: ChosenTobaccoTableViewServiceDelegate {
    
    func chosenTobaccoServiceDidDeleteItem(_ service: ChosenTobaccoTableViewService, deletedItem item: String) {
        
    }
    
}

extension TobaccoRecipeSlideView: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        isSearching = !(searchBar.text?.isEmpty ?? true)
        endEditing(true)
    }

    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        isSearching = !(searchBar.text?.isEmpty ?? true)
        searchBar.text = nil
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        isSearching = false
        tableView.reloadData()
        endEditing(true)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
            if searchText.count > 0 {
                filtredTobaccos = flawors.map{$0.copy()} as? [DisplayableTobaccoSort] ?? []
                
                filtredTobaccos = filtredTobaccos.filter{
                    $0.items = $0.items.filter{$0.name.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil}
                    $0.collapsed = false
                    
                    return !$0.items.isEmpty
                }

                guard isSearching else {
                    isSearching = true
                    return
                }
            } else {
                isSearching = false
            }
            
            tableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = !(searchBar.text?.isEmpty ?? true)
    }
    
}

