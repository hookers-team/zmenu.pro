//
//  TobaccoTableViewService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 12.12.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol ChosenTobaccoTableViewServiceDelegate: class {
    
    func chosenTobaccoServiceDidDeleteItem(_ service: ChosenTobaccoTableViewService, deletedItem item: String)
    
}

final class ChosenTobaccoTableViewService: NSObject {
    
    private weak var delegate: ChosenTobaccoTableViewServiceDelegate!
    private weak var orderItemsTableView: UITableView?
    var chosenTobaccos: [DisplayableTobacco] = []
    
    init(tableView: UITableView) {
        orderItemsTableView = tableView
    }
    
    func configurate(with delegate: ChosenTobaccoTableViewServiceDelegate) {
        
        orderItemsTableView?.delegate = self
        orderItemsTableView?.dataSource = self
        
        self.delegate = delegate
    }
    
    func addTobaccoToOrder(_ mix: DisplayableTobacco) {
        chosenTobaccos.insert(mix, at: 0)
        orderItemsTableView?.reloadData()
    }
    
}

extension ChosenTobaccoTableViewService: UITableViewDataSource, UITableViewDelegate  {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chosenTobaccos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CollapsibleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as? CollapsibleTableViewCell ??
            CollapsibleTableViewCell(style: .default, reuseIdentifier: "cell")
        
        let item = chosenTobaccos[indexPath.row]
        
        cell.nameLabel.text = "\(item.brand)" + "(\(item.name))"
        cell.detailLabel.text = item.detail == item.name ? nil : item.detail
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .destructive, title: "Удалить".localized()) { (action, indexPath) in
            
            self.chosenTobaccos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .none)
        }
        
        return [delete]
    }
    
}

extension ChosenTobaccoTableViewService {
    
    struct Constants {
        
        static let cellHeight = CGFloat(52)
        
    }
    
}
