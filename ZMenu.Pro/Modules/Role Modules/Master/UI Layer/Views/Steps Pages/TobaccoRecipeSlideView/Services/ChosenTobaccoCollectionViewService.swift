//
//  ChosenTobaccoCollectionViewService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 30.12.2018.
//  Copyright © 2018 Skaix. All rights reserved.
//

import Foundation
import UIKit

protocol ChosenTobaccoServiceDelegate: class {
    
    func chosenTobaccoCollectionViewService(_ chosenTobaccoCollectionViewService: ChosenTobaccoCollectionViewService, removeTobacco flawor: DisplayableTobacco)
    
}

final class ChosenTobaccoCollectionViewService: NSObject {
    
    weak var delegate: ChosenTobaccoServiceDelegate!
    var flawors : [DisplayableTobacco] = []
    private weak var chosenTobaccoCollectionView: UICollectionView?
    
    init(colletionView: UICollectionView) {
        chosenTobaccoCollectionView = colletionView
    }
    
    func configurate(with delegate: ChosenTobaccoServiceDelegate) {
        chosenTobaccoCollectionView?.delegate = self
        chosenTobaccoCollectionView?.dataSource = self
        
        chosenTobaccoCollectionView?.registerReusableCell(cellType:
            ChosenTobaccoCollectionViewCell.self)
        
        self.delegate = delegate
    }
    
    func addTobacco(_ flawor: DisplayableTobacco) {
        self.flawors.insert(flawor, at: 0)

        chosenTobaccoCollectionView?.reloadData()
    }
    
    private func removeTobacco(at indexPath: IndexPath) {
        let flawor = flawors[indexPath.row]
        
        flawors.remove(at: indexPath.row)
        chosenTobaccoCollectionView?.deleteItems(at: [indexPath])
        
        delegate.chosenTobaccoCollectionViewService(self, removeTobacco: flawor)
    }
    
}

extension ChosenTobaccoCollectionViewService: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flawors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: ChosenTobaccoCollectionViewCell.self)
        
        cell.delegate = self
        
        let flawor = self.flawors[indexPath.row]
        
        cell.nameLabel.text = "\(flawor.name)"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        removeTobacco(at: indexPath)
    }
    
}

extension ChosenTobaccoCollectionViewService: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flawor = flawors[indexPath.row]
        let text = "\(flawor.name)"
        
        let textSize = text.size(withAttributes:[.font: UIFont.systemFont(ofSize:12.0)])
        
        return CGSize(width: textSize.width + 15 + textSize.height, height: textSize.height + 6)
    }

}

extension ChosenTobaccoCollectionViewService: ChosenTobaccoCollectionViewCellDelegate {
    
    func chosenTobaccoCollectionViewCellDidTapRemoveButton(_ cell: ChosenTobaccoCollectionViewCell) {
        guard let indexPath = chosenTobaccoCollectionView?.indexPath(for: cell) else { return }
        
        removeTobacco(at: indexPath)
    }

}
