//
//  MixNameSlideView.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol Slide where Self: UIView {
    
    var containerView: UIView! { get }
    
}

protocol MixNameSlideViewDelegate: class {
    
    func mixNameSlideView(_  mixNameSlideView: MixNameSlideView, didChoose mixCategory: MixCategorizedData)
    
}

extension Slide {
    
    func configurate() {
        containerView.layer.cornerRadius = 12
        containerView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
}

typealias MixCategorizedData = (categoryName: String, categoryId: String)

final class MixNameSlideView: UIView, NibLoadable, Slide {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var mixNameTextField: UITextField!
    @IBOutlet weak var categoryPicker: UIPickerView!
    
    private weak var delegate: MixNameSlideViewDelegate?
    
    let pickerData: [MixCategorizedData] = [("Сладкие".localized(), "sweet"),
                                           ("Кислые".localized(), "sour"),
                                           ("Мятные".localized(), "mint"),
                                           ("Фруктовые".localized(), "fruit"),
                                           ("Экзотичекие".localized(), "exotic")]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        categoryPicker.delegate = self
        categoryPicker.dataSource = self
    }

    func configurate(with styleguide: DesignStyleGuide, delegate: UITextFieldDelegate & MixNameSlideViewDelegate) {
        stepLabel.textColor = styleguide.labelTextColor
        mixNameTextField.attributedPlaceholder = NSAttributedString(string: "Введите название микса".localized(),
                                                                    attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        
        self.delegate = delegate
        mixNameTextField.delegate = delegate
    
    }
    
    func selectedCategoryId() -> String {
        let category = pickerData[categoryPicker.selectedRow(inComponent: 0)]
        return category.categoryId
    }
    
}

extension MixNameSlideView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: pickerData[row].categoryName, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        return attributedString
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].categoryName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        delegate?.mixNameSlideView(self, didChoose: pickerData[row])
    }
    
    func pickCategory(categoryId: String) {
        guard let index = pickerData.firstIndex(where: {$0.categoryId == categoryId}) else { return }
        
        categoryPicker.selectRow(index, inComponent: 0, animated: true)
        delegate?.mixNameSlideView(self, didChoose: pickerData[index])
    }
    
}
