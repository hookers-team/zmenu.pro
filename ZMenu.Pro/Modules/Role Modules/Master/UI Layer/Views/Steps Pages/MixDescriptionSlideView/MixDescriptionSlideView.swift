//
//  MixDescriptionSlideView.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class MixDescriptionSlideView: UIView, NibLoadable, Slide {

    @IBOutlet var containerView: UIView!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var mixDescriptionTextView: UITextView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var priceTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func configurate(with styleguide: DesignStyleGuide, delegate: UITextFieldDelegate & UITextViewDelegate) {
        
        stepLabel.textColor = styleguide.labelTextColor
        mixDescriptionTextView.text = "Введите описание микса. Например: Pear Darkside + American Pie Serbetli в пропорции 50/50. Крайне оригинальный вкус, который имеет богатый букет оттенков и является одновременно свежим, сладким и терпким.".localized()
        priceTextField.attributedPlaceholder = NSAttributedString(string: "250.00",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
       
        priceTextField.delegate = delegate
        
        mixDescriptionTextView.layer.borderColor = UIColor.black.cgColor
        mixDescriptionTextView.textColor = styleguide.senderTextColor
        mixDescriptionTextView.delegate = delegate
    }
    
}
