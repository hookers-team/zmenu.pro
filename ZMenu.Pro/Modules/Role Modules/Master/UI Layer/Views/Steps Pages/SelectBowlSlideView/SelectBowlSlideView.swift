//
//  SelectBowlSlideView.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class SelectBowlSlideView: UIView, NibLoadable, Slide {

    @IBOutlet var containerView: UIView!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var bowlCollectionViewService: BowlCollectionViewService!
    
    override func awakeFromNib() {
         super.awakeFromNib()
        
    }
    
    func configurate(with styleguide: DesignStyleGuide, delegate: BowlCollectionViewServiceDelegate) {
        stepLabel.textColor = styleguide.labelTextColor
        
        bowlCollectionViewService = BowlCollectionViewService(collectionView: collectionView)
        
        bowlCollectionViewService.configurate(with: delegate)
    }
    
    func currentHokahBowl() -> String? {
        return bowlCollectionViewService.currentBowl()
    }
    
    func configurateBowl(bowlType: String?) {
        bowlCollectionViewService.selectBowl(bowlType: bowlType ?? "Глиняная чаша".localized())
    }
    
}
