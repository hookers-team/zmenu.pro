//
//  MixListCollectionViewService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 10.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol MixListServiceDelegate: class {
    
    func serviceWantToEditMix(_ service: MixListCollectionViewService, editingMix mix: DisplayableMix)
    func serviceWantToRemoveMix(_ service: MixListCollectionViewService, removingMix mix: DisplayableMix)
    
}

final class MixListCollectionViewService: NSObject  {
 
    private weak var delegate: MixListServiceDelegate?
    private var mixes: [DisplayableMix] = []
    private weak var mixListCollectionView: UICollectionView?
    private var styleguide: DesignStyleGuide!
    
    init(collectionView: UICollectionView) {
        mixListCollectionView = collectionView
    }
    
    func configurate(with delegate: MixListServiceDelegate? = nil, styleguide: DesignStyleGuide) {
        mixListCollectionView?.delegate = self
        mixListCollectionView?.dataSource = self
        mixListCollectionView?.registerReusableCell(cellType: MixListCollectionViewCell.self)
        
        self.delegate = delegate
        self.styleguide = styleguide
    }
    
    func updateMixes(with newMixes: [DisplayableMix]) {
        mixes = newMixes
        mixListCollectionView?.reloadData()
    }
    
    func removeMix(_ mix: DisplayableMix) {
        guard let index = mixes.firstIndex(where: {$0.mixId == mix.mixId}) else { return }
        
        mixes.remove(at: index)
        mixListCollectionView?.deleteItems(at: [IndexPath(item: index, section: 0)])
    }
 
}

// MARK: - UICollectionViewDataSource
extension MixListCollectionViewService: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mixes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: MixListCollectionViewCell.self)
        
        let mix = mixes[indexPath.row]
        
        cell.styleguide = styleguide
        cell.delegate = self
        cell.editingMixDelegate = self
        
        cell.nameLabel.text = mix.name
        cell.mixImageView.download(image: mix.imageURL, placeholderImage: UIImage(named: "z_violet"))
        cell.priceLabel.text = String(mix.price) + " " + MenuViewController.Constants.grn
        
        cell.descriptionView.titleLabel.text = mix.name
        cell.descriptionView.descriptionLabel.attributedText = NSAttributedString.make(from: mix.descriptionStrings, font: styleguide.regularFont(ofSize: 12))
        cell.changeDescriptionViewHiddenStatus(mix.isFlipped)
        
        cell.editButtonBlurContainerView.isHidden = delegate == nil
        cell.removeMixButtonContainerView.isHidden = delegate == nil
        cell.likesCountLabel.text = String(mix.likes) + " ❤️"
        
        cell.refreshUI(withStyleguide: styleguide)


        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? MixListCollectionViewCell else { return }
        cell.flip()
        mixes[indexPath.item].isFlipped = !mixes[indexPath.item].isFlipped
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension MixListCollectionViewService: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: UIScreen.main.bounds.size.width / 2 - 10, height: UIScreen.main.bounds.size.height / 3)
        
        return size
    }
    
}

extension MixListCollectionViewService: EditingMixDelegate {
    
    func diTapRemoveMixButtonOnListCell(_ cell: MixListCollectionViewCell) {
        guard let indexPath = mixListCollectionView?.indexPath(for: cell) else { return }
        
        delegate?.serviceWantToRemoveMix(self, removingMix: mixes[indexPath.row])
    }
    
    func diTapEditMixButtonOnListCell(_ cell: MixListCollectionViewCell) {
        guard let indexPath = mixListCollectionView?.indexPath(for: cell) else { return }
        
        delegate?.serviceWantToEditMix(self, editingMix: mixes[indexPath.row])
    }
    
}

// MARK: - MixListCollectionViewCellDelegate
extension MixListCollectionViewService: MixListCollectionViewCellDelegate {
    
    func didFlip(_ cell: MixListCollectionViewCell) {
        guard let indexPath = mixListCollectionView?.indexPath(for: cell) else { return }
        
        mixes[indexPath.item].isFlipped = !mixes[indexPath.item].isFlipped
    }
    
}
