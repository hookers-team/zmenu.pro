//
//  MixListCollectionViewCell.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 10.10.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

protocol MixListCollectionViewCellDelegate: class {
    
    func didFlip(_ cell: MixListCollectionViewCell)
    
    
}

protocol EditingMixDelegate: class {
    
    func diTapRemoveMixButtonOnListCell(_ cell: MixListCollectionViewCell)
    func diTapEditMixButtonOnListCell(_ cell: MixListCollectionViewCell)
    
}

final class MixListCollectionViewCell: UICollectionViewCell, NibReusable {

    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var removeMixButton: UIButton!
    @IBOutlet weak var editMixButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mixImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var removeMixButtonBlurView: UIVisualEffectView!
    @IBOutlet weak var removeMixButtonContainerView: UIView!
    @IBOutlet weak var editButtonBlurContainerView: UIView!
    @IBOutlet weak var infoButtonBlurContainerView: UIView!
    @IBOutlet weak var priceLabelBlurView: UIVisualEffectView!
    @IBOutlet weak var mixNameLabelBlurView: UIVisualEffectView!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var likesBlurView: UIVisualEffectView!
    
    var styleguide: DesignStyleGuide!
    weak var delegate: MixListCollectionViewCellDelegate?
    weak var editingMixDelegate: EditingMixDelegate?
    
    lazy var descriptionView: MixDescriptionView = {
        let view = MixDescriptionView.instantiateFromNib()
        
        view.frame = contentView.bounds
        containerView.insertSubview(view, belowSubview: infoButton)
        
        view.layer.borderWidth = 0.5
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.cornerRadius = styleguide.cornerRadius
        
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mixNameLabelBlurView.layer.cornerRadius = 6
        priceLabelBlurView.layer.cornerRadius = 6
        removeMixButtonContainerView.layer.cornerRadius = 0.5 * removeMixButton.bounds.size.width
        editButtonBlurContainerView.layer.cornerRadius = 0.5 * removeMixButton.bounds.size.width
        infoButtonBlurContainerView.layer.cornerRadius = 0.5 * removeMixButton.bounds.size.width
        likesBlurView.layer.cornerRadius = 6
        
        prepareGestureRecognizers()
    }

    private func prepareGestureRecognizers() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(showInfo(_:)))

        leftSwipe.direction = [.left, .right]
        contentView.addGestureRecognizer(leftSwipe)
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    self.blurView.effect = UIBlurEffect(style: .dark)
                    self.descriptionView.blurEffect.effect = UIBlurEffect(style: .dark)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseOut, animations: {
                    self.transform = CGAffineTransform(scaleX: 1, y: 1)
                    self.blurView.effect = nil
                    self.descriptionView.blurEffect.effect = nil
                }, completion: nil)
            }
        }
    }

    // MARK: - Actions
    
    @IBAction private func removeMix(_ sender: Any) {
        editingMixDelegate?.diTapRemoveMixButtonOnListCell(self)
    }
    
    @IBAction private func editMix(_ sender: Any) {
        editingMixDelegate?.diTapEditMixButtonOnListCell(self)
    }
    
    @IBAction private func showInfo(_ sender: Any) {
        delegate?.didFlip(self)
        flip()
    }
    
    func flip() {
        let animateOption: UIView.AnimationOptions = .transitionFlipFromLeft
        
        UIView.transition(with: self.contentView, duration: 0.25, options: [animateOption], animations: {
            self.changeDescriptionViewHiddenStatus(!self.descriptionView.isHidden)
        }, completion: nil)
    }
    
    func changeDescriptionViewHiddenStatus(_ isHidden: Bool) {
        descriptionView.isHidden = isHidden
        
        if isHidden == true {
            contentView.removeGestureRecognizer(descriptionView.scrollView.panGestureRecognizer)
        } else {
            contentView.addGestureRecognizer(descriptionView.scrollView.panGestureRecognizer)
        }
    }

}

//MARK: - UIStyleGuideRefreshing
extension MixListCollectionViewCell: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        containerView.layer.cornerRadius = styleguide.cornerRadius
        nameLabel.textColor = styleguide.labelTextColor
        priceLabel.textColor = styleguide.labelTextColor
    }
    
}
