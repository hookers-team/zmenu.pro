//
//  MenuViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 30.09.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class MenuViewController: UIViewController {
    
    @IBOutlet private weak var ctaButtonContainerView: CTAButtonContainerView!
    @IBOutlet private weak var mixListCollectionView: UICollectionView!
    @IBOutlet private weak var categoryCollectionView: UICollectionView!
    
    fileprivate var categoryCollectionViewService: CategoryCollectionViewService!
    fileprivate var mixListCollectionViewService: MixListCollectionViewService!
    
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    var menu : [DisplayableCategory: [DisplayableMix]] = [:]
    var currentWorkPlace: UserInfo.WorkPlace!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        
        configurateMixListCollectionView()
        configurateMixCategoryCollectionView()
        
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(createNewMix), for: .touchUpInside)
        refreshUI(withStyleguide: styleguide)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ctaButtonContainerView.ctaButton.setTitle("Создать микс".localized(), for: .normal)
        
        employeeStore.getMenu(byRestaurantId: currentWorkPlace.restaurant.restaurantId)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
        
        navigationItem.setTitleView(withTitle: "Меню миксов".localized(),
                                    subtitle: "Редактирование".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }

    deinit {
        print("deinit RestaurantViewController")
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    
    @objc func createNewMix() {
        let value = MasterEvents.NavigationEvent.OpenMakeMixScreen.Value(workPlace: currentWorkPlace, animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenMakeMixScreen.self, result: Result(value: value))
    }
    
    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
}

extension MenuViewController {
    
    func configurateMixCategoryCollectionView() {
        categoryCollectionViewService = CategoryCollectionViewService(colletionView: categoryCollectionView)
        categoryCollectionViewService.configurate(with: self)
    }
    
    func configurateMixListCollectionView() {
        mixListCollectionViewService = MixListCollectionViewService(collectionView: mixListCollectionView)
        mixListCollectionViewService.configurate(with: self, styleguide: styleguide)
    }
    
}

extension MenuViewController: CategoryServiceDelegate {
    
    func serviceDidChoseCategory(_ service: CategoryCollectionViewService, chosenCategory category: DisplayableCategory) {
        guard let mixes = menu[category] else { return }
        
        categoryCollectionView.performBatchUpdates(nil, completion: nil)
        mixListCollectionViewService.updateMixes(with: mixes)
        
        mixListCollectionView.reloadData()
    }
    
}

extension MenuViewController: MixListServiceDelegate {
    
    func serviceWantToRemoveMix(_ service: MixListCollectionViewService, removingMix mix: DisplayableMix) {
        
        presentRemoveMixAlertController(mix: mix)
    }
    
    func serviceWantToEditMix(_ service: MixListCollectionViewService, editingMix mix: DisplayableMix) {
        guard let mix = employeeStore.menuData?.categories.first(where: {$0.categoryId == mix.categoryId})?.mixes.first(where: {$0.mixId == mix.mixId}) else { return }
        
        let value = MasterEvents.NavigationEvent.OpenEditMixScreen.Value(workPlace: currentWorkPlace, mix: mix)

        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenEditMixScreen.self, result: Result(value: value))
    }
    
}

extension MenuViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
    }
    
}


extension MenuViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
        
        if let change = change as? EmployeeStoreStateChangeWithData {
            employeeStoreStateChangeWithData(change: change)
        }
    }
    
    private func employeeStoreStateChangeWithData(change: EmployeeStoreStateChangeWithData) {
        switch change {
        case .removeMix(categoryId: let categoryId, mixId: let mixId):
            guard let category = menu.keys.first(where: {$0.categoryId == categoryId}),
                let mixes = menu[category],
                let mix = mixes.first(where: {$0.mixId == mixId}) else {
                return
                
            }
            
            menu.updateValue(mixes.filter{$0.mixId != mix.mixId}, forKey: category)
            
            self.mixListCollectionViewService.removeMix(mix)
            
        default: break
            
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? startCTAButtonLoadingAnimation() : stopCTAButtonLoadingAnimation()
            
            //KS: TODO: Show/hide skeleton
            //employeeStore.isLoading ? addSkeletonViewController() : hideSkeletonViewController()
        }
        
        if change.contains(.menu) {
            guard let categories = employeeStore.menuData?.categories, categories.count > 0 else { return }

            menu = categories.reduce([DisplayableCategory: [DisplayableMix]](), { (dict, mixCategory) -> [DisplayableCategory : [DisplayableMix]] in
                var dict = dict
                
                dict[DisplayableCategory(categoryId: mixCategory.categoryId, name: mixCategory.name, imageURL: mixCategory.imageURL)] = mixCategory.mixes.map{DisplayableMix(from: $0)}
                
                return dict
            })

            categoryCollectionViewService.updateCategories(categories: categories.map{DisplayableCategory(categoryId: $0.categoryId, name: $0.name, imageURL: $0.imageURL)})

            mixListCollectionViewService.updateMixes(with: categories.first?.mixes.compactMap({ DisplayableMix(from: $0) }) ?? [])
        }
    }
    
}

extension MenuViewController {
    
    func presentRemoveMixAlertController(mix: DisplayableMix) {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите удалить микс \"\(mix.name)\"?".localized(), preferredStyle: .alert)
        
        let attributedTitle = NSAttributedString(string: "Удаление микса".localized(), attributes:  [NSAttributedString.Key.font: styleguide.boldFont(ofSize: 17)])
        
        alert.setValue(attributedTitle, forKey: "attributedTitle")
        
        let cancelAction = UIAlertAction(title: "Отменить".localized(), style: .cancel)
        
        let leaveChannelAction = UIAlertAction(title: "Да, удалить".localized(), style: .default, handler: { _ in
            self.employeeStore.removeMix(categoryId: mix.categoryId, mixId: mix.mixId, imageUrl: mix.imageURL)
        })
        
        leaveChannelAction.setValue(styleguide.errorColor, forKey: "titleTextColor")
        
        [leaveChannelAction, cancelAction].forEach{alert.addAction($0)}
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension MenuViewController {
    
    struct Constants {
        
        static let grn = "₴"
        static let orderCellHeight = CGFloat(44.0)
        
    }
    
}
