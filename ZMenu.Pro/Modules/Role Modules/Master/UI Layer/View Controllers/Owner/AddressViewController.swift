//
//  AddressViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

struct AddressLocalizedStringConstants {
    
    static var errorTitle: String { return "Без паники 👻".localized() }
    static var validateCountryErrorMessage: String { return "Необходимо указать страну".localized() }
    static var validateCityErrorMessage: String { return "Необходимо указать город".localized() }
    static var validateStreetErrorMessage: String { return "Необходимо указать улицу".localized() }
    static var validateStreetNumberErrorMessage: String { return "Необходимо указать номер".localized() }
    
}

final class AddressViewController: UIViewController {

    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    var workPlace: RestaurantModel!
    
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var streetNumberTextField: UITextField!
    @IBOutlet weak var stageTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ctaButtonContainerView.ctaButton.setTitle("Сохранить".localized(), for: .normal)
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(save), for: .touchUpInside)

        configurateNavBar()
        prepareForEdit()
        refreshUI(withStyleguide: styleguide)
    }
    

    @objc func save() {
        guard validateAddress() else { return }
        
        let address = Address.init(city: cityTextField.text ?? "", country: countryTextField.text ?? "", street: streetTextField.text ?? "", house: streetNumberTextField.text ?? "", floor: stageTextField.text)
        
        var photos: [String] = []
        
        if let photo = workPlace.photos?.first {
            photos = [photo]
        }
        
        employeeStore.editRestaurant(restaurantId: workPlace.restaurantId, name: workPlace.name, instagram: workPlace.instagram, address: address, schedule: workPlace.schedule, description: workPlace.description, photos: photos)
    }
    
    private func configurateNavBar() {
        navigationItem.addCloseButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Редактирование адреса".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }

    private func prepareForEdit() {
        countryTextField.text = workPlace.address?.country
        cityTextField.text = workPlace.address?.city
        streetTextField.text = workPlace.address?.street
        streetNumberTextField.text = workPlace.address?.house
        stageTextField.text = workPlace.address?.floor
    }
    
    private func validateAddress() -> Bool {
        return validateIsEmtyTextField(countryTextField, errorMessage: AddressLocalizedStringConstants.validateCountryErrorMessage) &&
        validateIsEmtyTextField(cityTextField, errorMessage: AddressLocalizedStringConstants.validateCityErrorMessage) &&
        validateIsEmtyTextField(streetTextField, errorMessage: AddressLocalizedStringConstants.validateStreetErrorMessage) &&
        validateIsEmtyTextField(streetNumberTextField, errorMessage: AddressLocalizedStringConstants.validateCountryErrorMessage)
    }
    
    
    private func validateIsEmtyTextField(_ textField: UITextField, errorMessage: String) -> Bool {
        let isValid = !(textField.text ?? "").isEmpty
        
        if !isValid {
            presentError(withTitle: AddressLocalizedStringConstants.errorTitle, subtitle: errorMessage)
        }
        
        return isValid
    }
    
    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }

}

extension AddressViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
    }
    
}

extension AddressViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }

        if change.contains(.editRestaurantProfile) {
            back()
        }
    }
    
}
