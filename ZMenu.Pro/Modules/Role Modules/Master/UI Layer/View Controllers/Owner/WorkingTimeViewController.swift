//
//  WorkingTimeViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

final class WorkingTimeViewController: UIViewController {
    
    private var currentTextField: UITextField!
    
    @IBOutlet weak var mondayTextField: UITextField!
    @IBOutlet weak var tuesdayTextField: UITextField!
    @IBOutlet weak var wednesdayTextField: UITextField!
    @IBOutlet weak var thursdayTextField: UITextField!
    @IBOutlet weak var fridayTextField: UITextField!
    @IBOutlet weak var saturdayTextField: UITextField!
    @IBOutlet weak var sundayTextField: UITextField!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    
    private var accessoryToolbar: UIToolbar {
        get {
            let toolbarFrame = CGRect(x: 0, y: 0,
                                      width: view.frame.width, height: 44)
            let accessoryToolbar = UIToolbar(frame: toolbarFrame)
            
            let doneButton = UIBarButtonItem(title: "Готово".localized(), style: .plain, target: self, action: #selector(done))
            
            doneButton.tintColor = styleguide.primaryColor
            
            let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                                target: nil,
                                                action: nil)
            accessoryToolbar.items = [flexibleSpace, doneButton]
            accessoryToolbar.barTintColor = UIColor.white
            return accessoryToolbar
        }
    }
    
    private var pickerView: UIPickerView!
    
    var workPlace: RestaurantModel!
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
         ctaButtonContainerView.ctaButton.setTitle("Сохранить".localized(), for: .normal)
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(save), for: .touchUpInside)
        
        configurateNavBar()
        configurateTextFields()
        refreshUI(withStyleguide: styleguide)
        prepareForEdit()
        hideKeyboardWhenTappedAround()
    }
    
    private func configurateTextFields() {
        pickerView = UIPickerView()
        pickerView.delegate = self
        
        let textFields = [mondayTextField, tuesdayTextField, wednesdayTextField, thursdayTextField, fridayTextField, saturdayTextField, sundayTextField]
        
        textFields.forEach{
            $0?.inputView = pickerView
            $0?.delegate = self
            $0?.inputAccessoryView = accessoryToolbar
        }
    }
    
    private func configurateNavBar() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Время работы заведения".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        navigationItem.addCloseButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
    }
    
    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
    @objc func save() {
        guard validateSchedule() else { return }
        
        let schedule = Schedule(monday: mondayTextField.text ?? "", tuesday: tuesdayTextField.text ?? "", wednesday: wednesdayTextField.text ?? "", thursday: thursdayTextField.text ?? "", friday: fridayTextField.text ?? "", saturday: saturdayTextField.text ?? "", sunday: sundayTextField.text ?? "")

        var photos: [String] = []
        
        if let photo = workPlace.photos?.first {
            photos = [photo]
        }
        
        employeeStore.editRestaurant(restaurantId: workPlace.restaurantId, name: workPlace.name, instagram: workPlace.instagram, address: workPlace.address, schedule: schedule, description: workPlace.description, photos: photos)
    }
    
    func prepareForEdit() {
        mondayTextField.text = workPlace.schedule?.monday
        tuesdayTextField.text = workPlace.schedule?.tuesday
        wednesdayTextField.text = workPlace.schedule?.wednesday
        thursdayTextField.text = workPlace.schedule?.thursday
        fridayTextField.text = workPlace.schedule?.friday
        saturdayTextField.text = workPlace.schedule?.saturday
        sundayTextField.text = workPlace.schedule?.sunday
    }
    
    func validateSchedule() -> Bool {
        return validateIsEmtyTextField(mondayTextField, errorMessage: "понедельник".localized()) &&
            validateIsEmtyTextField(tuesdayTextField, errorMessage: "вторник".localized()) &&
        validateIsEmtyTextField(wednesdayTextField, errorMessage: "среду".localized()) &&
        validateIsEmtyTextField(thursdayTextField, errorMessage: "четверг".localized()) &&
        validateIsEmtyTextField(fridayTextField, errorMessage: "пятницу".localized()) &&
        validateIsEmtyTextField(saturdayTextField, errorMessage: "субботу".localized()) &&
            validateIsEmtyTextField(sundayTextField, errorMessage: "воскресенье".localized())
    }
    
    private func validateIsEmtyTextField(_ textField: UITextField, errorMessage: String) -> Bool {
        var isValid = !(textField.text ?? "").isEmpty
        
        if textField.text?.contains(Constants.dayOff) ?? false {
            isValid = textField.text == "\(Constants.dayOff) - \(Constants.dayOff)"
        }
        
        let message = "Необходимо указать корректное время работы в " + errorMessage
        
        if !isValid {
            presentError(withTitle: "Без паники 👻".localized(), subtitle: message)
        }
        
        return isValid
    }
    
    @objc func back() {
        view.endEditing(true)
        
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    @objc func done() {
        view.endEditing(true)
    }
    
}

extension WorkingTimeViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.isEmpty ?? true {
            let start = Constants.pickerData[pickerView.selectedRow(inComponent: 0)]
            
            let end = Constants.pickerData[pickerView.selectedRow(inComponent: 1)]
            
            textField.text = "\(start) - \(end)"
        }
        
        currentTextField = textField
    }
    
}

extension WorkingTimeViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
        mondayTextField.attributedPlaceholder = NSAttributedString(string: "Выходной - Выходной".localized(),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        tuesdayTextField.attributedPlaceholder = NSAttributedString(string: "10:00 - 23:00".localized(),
                                                                   attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        wednesdayTextField.attributedPlaceholder = NSAttributedString(string: "10:00 - 23:00".localized(),
                                                                    attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        thursdayTextField.attributedPlaceholder = NSAttributedString(string: "10:00 - 00:00".localized(),
                                                                    attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        fridayTextField.attributedPlaceholder = NSAttributedString(string: "10:00 - 01:00".localized(),
                                                                    attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        sundayTextField.attributedPlaceholder = NSAttributedString(string: "10:00 - 02:00".localized(),
                                                                    attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        tuesdayTextField.attributedPlaceholder = NSAttributedString(string: "10:00 - 23:00".localized(),
                                                                    attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])

    }
    
}

extension WorkingTimeViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }
        
        if change.contains(.editRestaurantProfile) {
            back()
        }
        
    }
    
}

extension WorkingTimeViewController:  UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Constants.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return Constants.pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let anotherComponent = component == 0 ? 1 : 0
        
        let pickedData = Constants.pickerData[row]
        
        if pickedData == Constants.dayOff {
            pickerView.selectRow(Constants.pickerData.firstIndex(of: Constants.dayOff) ?? 0, inComponent: anotherComponent, animated: true)
        }
        
        let start = Constants.pickerData[pickerView.selectedRow(inComponent: 0)]
        
        let end = Constants.pickerData[pickerView.selectedRow(inComponent: 1)]
        
        currentTextField.text = "\(start) - \(end)"
    }
    
}

extension WorkingTimeViewController {
    
    struct Constants {
        
        static let pickerData = [dayOff, "00:00", "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30", "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00", "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00", "21:30", "22:00", "22:30", "23:00", "23:30"]
        
        static var dayOff: String { return "Выходной".localized() }
        
    }
    
}
