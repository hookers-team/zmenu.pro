//
//  RestarauntMainViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

final class RestaurantMainViewController: UIViewController {
    
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var rstaurantPreviewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var restaurantPreviewContainerView: UIView!
    
    @IBOutlet weak var orderListContainerView: UIView!
    @IBOutlet weak var createMixContainerView: UIView!
    @IBOutlet weak var editMenuContainerView: UIView!
    @IBOutlet weak var addAdministratorContainerView: UIView!
    @IBOutlet weak var addMasterContainerView: UIView!
    @IBOutlet weak var employeeListContainerView: UIView!
    @IBOutlet weak var editButtonBlurView: UIVisualEffectView!
    @IBOutlet weak var likesBlurView: UIVisualEffectView!
    @IBOutlet weak var nameBlurView: UIVisualEffectView!
    
    
    private var onceLayoutSubviews = false
    
    var currentWorkPlace: UserInfo.WorkPlace!
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshUI(withStyleguide: styleguide)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            
            rstaurantPreviewHeightConstraint.constant = RestaurantsCollectionViewService.Constants.cellSize.height
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        restaurantNameLabel.text = currentWorkPlace.restaurant.name
        restaurantImageView.download(image: currentWorkPlace.restaurant.photos?.first ?? "", placeholderImage: UIImage(named: "rest_default"))
        likesCountLabel.text = currentWorkPlace.restaurant.rating
        
        configurateNavBar()
        tabBarController?.tabBar.isHidden = true
    }
    
    private func configurateNavBar() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Заведение".localized(),
                                    subtitle: "Главное меню".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    @IBAction func editMenu(_ sender: UIButton) {
        let value = MasterEvents.NavigationEvent.OpenEditMenuScreen.Value(workPlace: currentWorkPlace, animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenEditMenuScreen.self, result: Result(value: value))
    }
    
    @IBAction func createMix(_ sender: Any) {
        let value = MasterEvents.NavigationEvent.OpenMakeMixScreen.Value(workPlace: currentWorkPlace, animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenMakeMixScreen.self, result: Result(value: value))
    }
    
    @IBAction func editProfile(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenEditProfileScreen.Value(workPlace: currentWorkPlace, animated: true)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenEditProfileScreen.self, result: Result(value: value))
    }
    
    @IBAction func addMaster(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenQrScannerScreen.Value(animated: true, role: .master, restaurantId: currentWorkPlace.restaurant.restaurantId)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenQrScannerScreen.self, result: Result(value: value))
    }
    
    @IBAction func orderList(_ sender: Any) {
        let value = OrdersEvent.NavigationEvent.ShowOrderList.Value(workPlace: currentWorkPlace)
        
        dispatcher.dispatch(type: OrdersEvent.NavigationEvent.ShowOrderList.self, result: Result(value: value))
    }
    
    @IBAction func addAdministator(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenQrScannerScreen.Value(animated: true, role: .admin, restaurantId: currentWorkPlace.restaurant.restaurantId)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenQrScannerScreen.self, result: Result(value: value))
    }
    
    @IBAction func stuffList(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenEmployeeList.Value(animated: true, workPlace: currentWorkPlace)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenEmployeeList.self, result: Result(value: value))
    }

}

extension RestaurantMainViewController: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        
        view.backgroundColor = styleguide.backgroundScreenColor
        
        editButtonBlurView.layer.cornerRadius = editButtonBlurView.frame.height / 2
        nameBlurView.layer.cornerRadius = 6
        likesBlurView.layer.cornerRadius = 6
        let views = [addMasterContainerView, addAdministratorContainerView, editMenuContainerView, employeeListContainerView, createMixContainerView, orderListContainerView]
        
        views.forEach{
            $0?.layer.cornerRadius = styleguide.cornerRadius
            $0?.layer.borderWidth = 1
        }
        
        
        restaurantPreviewContainerView.layer.cornerRadius = styleguide.cornerRadius
        restaurantPreviewContainerView.layer.borderWidth = 1
        restaurantPreviewContainerView.layer.borderColor = styleguide.backgroundCardColor.cgColor
    }
    
}

extension RestaurantMainViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? showSpinner() : hideSpinner()
        }
    }
    
}
