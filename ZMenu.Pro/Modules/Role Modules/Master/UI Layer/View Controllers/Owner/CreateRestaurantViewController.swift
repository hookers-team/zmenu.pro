//
//  CreateRestaurantViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 29.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

final class CreateRestaurantViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var streetNumberTextField: UITextField!
    @IBOutlet weak var stageTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ctaButtonContainerView.ctaButton.setTitle("Сохранить".localized(), for: .normal)
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(save), for: .touchUpInside)
        
        configurateNavBar()
        refreshUI(withStyleguide: styleguide)
        
        hideKeyboardWhenTappedAround()
    }
    
    @objc func save() {
        guard validateAddress(), validateNameTextField() else { return }
        
        let address = Address.init(city: cityTextField.text ?? "", country: countryTextField.text ?? "", street: streetTextField.text ?? "", house: streetNumberTextField.text ?? "" , floor: stageTextField.text ?? "")
        
        employeeStore.createRestaurant(withName: nameTextField.text ?? "", address: address)
    }
    
    private func configurateNavBar() {
        navigationItem.addCloseButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Создание заведения".localized(),
                                    subtitle: "Добавьте навзание и адрес".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    private func validateAddress() -> Bool {
        return validateIsEmtyTextField(countryTextField, errorMessage: AddressLocalizedStringConstants.validateCountryErrorMessage) &&
            validateIsEmtyTextField(cityTextField, errorMessage: AddressLocalizedStringConstants.validateCityErrorMessage) &&
            validateIsEmtyTextField(streetTextField, errorMessage: AddressLocalizedStringConstants.validateStreetErrorMessage) &&
            validateIsEmtyTextField(streetNumberTextField, errorMessage: AddressLocalizedStringConstants.validateCountryErrorMessage)
    }
    
    private func validateNameTextField() -> Bool {
        return validateIsEmtyTextField(nameTextField, errorMessage: "Название заведения - это обязательный параметр".localized())
    }
    
    private func validateIsEmtyTextField(_ textField: UITextField, errorMessage: String) -> Bool {
        let isValid = !(textField.text ?? "").isEmpty
        
        if !isValid {
            presentError(withTitle: AddressLocalizedStringConstants.errorTitle, subtitle: errorMessage)
        }
        
        return isValid
    }
    
    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
}

extension CreateRestaurantViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        nameTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
        countryTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
        cityTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
        streetTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
        streetNumberTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
        stageTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
        
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
    }
    
}

extension CreateRestaurantViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard textField != nameTextField else { return }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard textField != nameTextField else { return }
    }
    
}


extension CreateRestaurantViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }
        
        if change.contains(.userInfo) {
            back()
        }
    }
    
}
