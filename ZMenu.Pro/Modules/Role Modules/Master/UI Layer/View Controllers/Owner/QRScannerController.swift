import UIKit
import AVFoundation

final class QRScannerViewController: UIViewController {

    //MARK: - Outlets
    
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet weak var qrOverlayView: UIView!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var service: QRService!
    
    private var onceLayoutSubviews = false
    
    // MARK: - Properties
    
    var completion: ((String) -> ())?

    var isScanningEnd = false
    var captureSession = AVCaptureSession()
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?

    private let supportedCodeTypes = [AVMetadataObject.ObjectType.qr]
   
    override func viewDidLoad() {
        super.viewDidLoad()

        configurateScanner()
        
        tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        navigationItem.setTitleView(withTitle: service.makeViewTitle(),
                                    subtitle: "Наведите на его QR-код".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        messageLabel.text = "QR-код не найден".localized()
        navigationItem.addCloseButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            
            view.bringSubviewToFront(qrOverlayView)
        }
    }
    
    @objc private func back() {
        let value = OwnerEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
}

// MARK: - ConfigurateScanner
extension QRScannerViewController {
    
    func configurateScanner() {
        // Get the back-facing camera for capturing videos
        let devices: [AVCaptureDevice]?
        
        if #available(iOS 10.0, *) {
            let deviceDiscoverySession: AVCaptureDevice.DiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInTelephotoCamera], mediaType: .video, position: .back)
            devices = deviceDiscoverySession.devices
        } else {
            // Fallback on earlier versions
            devices = AVCaptureDevice.devices(for: .video)
        }
        
        guard let captureDevice = devices?.first(where: {$0.position == .back }) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = supportedCodeTypes
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
//            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
        // Move the message label and top bar to the front
        view.bringSubviewToFront(messageLabel)
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
}

//MARK: - AVCaptureMetadataOutputObjectsDelegate
extension QRScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        guard captureSession.isRunning else { return }
        
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
            messageLabel.text = "QR-код не найден".localized()
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if supportedCodeTypes.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if let dataString = metadataObj.stringValue {
                if let decodedUserId = service.decode(dataString: dataString) {
                    captureSession.stopRunning()
                    
                    service.addEmployeeWith(employeeUserId: decodedUserId)
                    
                    let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
                    dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
                    
                } else {
                    messageLabel.text = "QR-код не верный".localized()
                }
            }
        }
    }
    
}
