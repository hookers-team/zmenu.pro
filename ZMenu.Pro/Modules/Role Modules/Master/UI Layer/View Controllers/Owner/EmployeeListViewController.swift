//
//  EmployeeListViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 03.02.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation
import UIKit

final class EmployeeListViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    var currentWorkPlace: UserInfo.WorkPlace!
    var dispatcher: Dispatcher!
    var styleguide: DesignStyleGuide!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        let attributes = [ NSAttributedString.Key.foregroundColor: UIColor.white ]
        
        refreshControl.attributedTitle = NSAttributedString(string: "Получаем список сотрудников...".localized(), attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(getEmployeeList), for: .valueChanged)
        
        refreshControl.tintColor = .white
        
        return refreshControl
    }()
    
    
    private var rolesList : [UserRole]?
    private var employeeList: [UserRole: [EmployeeInfo]]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateNavBar()
        
        configurateTableView()
        getEmployeeList()
        handleEmptyEmployeeList([:])
    }
    
    private func configurateTableView() {
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        tableView.registerReusableCell(cellType: EmployeeTableViewCell.self)
    }
    
    private func configurateNavBar() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Список сотрудников".localized(),
                                    subtitle: "Редактирование".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    @objc func back() {
        let value = OwnerEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    @objc func getEmployeeList() {
        employeeStore.getEmployeeList(restaurantId: currentWorkPlace.restaurant.restaurantId)
    }
    
    private func handleEmptyEmployeeList(_ employeeList: [UserRole: [EmployeeInfo]]) {
        let checkArray = employeeList.values.map{$0.isEmpty}.filter{ $0 == false }
    
        if checkArray.isEmpty {
            self.tableView?.showEmptyListMessage("Список Ваших сотрудников пуст \nПотяните вниз, что бы обновить 👆 \n \nИли добавьте сотрудника 🤳".localized(), textColor: .white)
        } else {
            self.tableView?.showEmptyListMessage("", textColor: .white)
        }
    }
    
}

extension EmployeeListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(indexPath, cellType: EmployeeTableViewCell.self)
        
        if let role = rolesList?[indexPath.section], let employee = employeeList?[role]?[indexPath.row] {
            cell.avatarImageView.download(image: employee.profile.imageURL ?? "", placeholderImage: UIImage(named: "ava_default"))
            cell.nameLabel.text = employee.profile.name
            cell.descriptionLabel.text = employee.profile.name
            cell.roleLabel.text = UserRole.displayableRole(role)
            cell.roleLabel.textColor = employee.workingTime == nil ? styleguide.disabledTextColor : styleguide.accentTextColor
            cell.likeCountLabel.text = employee.profile.rating ?? "0"
            cell.containerView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            cell.containerView.layer.cornerRadius = styleguide.cornerRadius
            cell.deleteButton.tintColor = styleguide.errorColor
            cell.delegate = self
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let checkArray = employeeList?.values.map{$0.isEmpty}.filter{ $0 == false }
        guard let rolesList = rolesList, !(checkArray?.isEmpty ?? false) else { return 0 }
        
        return rolesList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let role = rolesList?[section], let employees = employeeList?[role] else { return 0 }
        
        return employees.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = TobaccoCollapsedTableHeaderView.instantiateFromNib()
        
        let role = rolesList?[section]
        
        let manyPostfix = role == .admin ? "ы".localized() : "и".localized()
        
        header.nameLabel.text = UserRole.displayableRole(role) + manyPostfix
        header.arrowLabel.isHidden = true
        header.containerView.backgroundColor = styleguide.bubbleColor
        
        return header
    }
    
}

extension EmployeeListViewController {
    
    func showDeleteEmployeeAlert(employeeName: String, employeeId: String?, role: String, restaurantId: String) {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что  хотите уволить сотрудника \(employeeName)?".localized(), preferredStyle: .alert)
        
        let attributedTitle = NSAttributedString(string: "Увольнение сотрудника 😔".localized(), attributes:  [NSAttributedString.Key.font: styleguide.boldFont(ofSize: 17)])
        
        alert.setValue(attributedTitle, forKey: "attributedTitle")
        
        let cancelAction = UIAlertAction(title: "Отменить".localized(), style: .cancel)
        
        let leaveChannelAction = UIAlertAction(title: "Да, уволить".localized(), style: .default, handler: { _ in
            self.employeeStore.deleteEmployee(role: role, employeeId: employeeId, restaurantId: restaurantId)
        })
        
        leaveChannelAction.setValue(styleguide.errorColor, forKey: "titleTextColor")
        
        [leaveChannelAction, cancelAction].forEach{alert.addAction($0)}
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension EmployeeListViewController: EmployeeTableViewCellDelegate {
    
    func employeeTableViewCellDidTapRemoveButton(_ cell: EmployeeTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell),
            let role = rolesList?[indexPath.section],
            let employee = employeeList?[role]?[indexPath.row] else { return }
        
        showDeleteEmployeeAlert(employeeName: employee.profile.name, employeeId: employee.employeeId, role: role.rawValue, restaurantId: currentWorkPlace.restaurant.restaurantId)
    }
    
}

extension EmployeeListViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        
    }
    
}

extension EmployeeListViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        } else if let change = change as? EmployeeStoreStateChangeWithData {
            employeeStoreStateChangeWithData(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            refreshControl.tintColor = .white
            
            if !employeeStore.isLoading {
                self.refreshControl.endRefreshing()
                self.hideSpinner()
            } else {
                guard !isSpinerPresented else { return }
                
                self.refreshControl.beginRefreshingManually()
            }
        }
    }
    
    private func employeeStoreStateChangeWithData(change: EmployeeStoreStateChangeWithData) {
        
        switch change {
        case .employeeList(let employeeList):
            var list: [UserRole : [EmployeeInfo]] = [:]
            
            if !employeeList.admin.isEmpty {
                list.updateValue(employeeList.admin, forKey: UserRole.admin)
            }
            
            if !employeeList.master.isEmpty {
                list.updateValue(employeeList.master, forKey: UserRole.master)
            }
            
            self.employeeList = [UserRole.admin : employeeList.admin, UserRole.master : employeeList.master]
            self.rolesList = Array(list.keys)
            self.handleEmptyEmployeeList(list)
            self.tableView.reloadData()
        case .deleteEmployee(let deletedEmployee):
            guard let employees = employeeList?[deletedEmployee.role],
                let employee = employees.first(where: { $0.employeeId == deletedEmployee.employeeId}),
                let section = rolesList?.firstIndex(of:deletedEmployee.role),
                let row = employees.firstIndex(of: employee) else { return }
            
            employeeList?[deletedEmployee.role]?.remove(at: row)
            
            if (employeeList?[deletedEmployee.role]?.isEmpty ?? true) {
                rolesList?.remove(at: section)
                tableView.deleteSections([section], with: .automatic)
                handleEmptyEmployeeList(employeeList ?? [:])
            } else {
                tableView.deleteRows(at: [IndexPath(row: row, section: section)], with: .automatic)
            }
            
        default:
            break
        }
        
    }
    
}
