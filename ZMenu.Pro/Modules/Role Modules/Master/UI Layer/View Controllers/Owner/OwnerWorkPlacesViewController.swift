//
//  OwnerWorkPlacesViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 28.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

fileprivate struct LocalizedStringConstants {
    
    static var createRestaurantButtonTitle: String { return "Создать кальянную ".localized() }
    static var chooseRestaurantButtonTittle: String { return "Выбрать кальянную".localized() }
    
}

final class OwnerWorkPlacesViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    @IBOutlet weak var globalBlurView: UIVisualEffectView!
    @IBOutlet weak var backgroundView: UIView!
    
    private var onceLayoutSubviews = false
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        let attributes = [ NSAttributedString.Key.foregroundColor: UIColor.white ]
        
        refreshControl.attributedTitle = NSAttributedString(string: "Получаем список Ваших заведений...".localized(), attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(getUserInfo), for: .valueChanged)
        
        refreshControl.tintColor = .white
        
        return refreshControl
    }()
    
    private var restaurantCollectionViewService: RestaurantsCollectionViewService!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateCTAButton()
        configurateCollectionView()
        configurateNavBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            refreshUI(withStyleguide: styleguide)
            handleEmptyWorkPlaces([])
            getUserInfo()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
            self.globalBlurView.effect = UIBlurEffect(style: .dark)
        }, completion: nil)
    }
    
    
    private func configurateCTAButton() {
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(addRestaurant), for: .touchUpInside)
    ctaButtonContainerView.ctaButton.setTitle(LocalizedStringConstants.createRestaurantButtonTitle, for: .normal)
    }
    
    private func configurateNavBar() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Создайте или выберите заведение".localized(),
                                    subtitle: "Ваши владения".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
        navigationItem.addLogoutButton(with: self, action: #selector(showLogoutAlert), tintColor: styleguide.primaryColor)
    }
    
    @objc func showLogoutAlert() {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите выйти из аккаунта?".localized(), preferredStyle: .alert)
        
        let attributedTitle = NSAttributedString(string: "Выход".localized(), attributes:  [NSAttributedString.Key.font: styleguide.boldFont(ofSize: 17)])
        
        alert.setValue(attributedTitle, forKey: "attributedTitle")
        
        let cancelAction = UIAlertAction(title: "Отменить".localized(), style: .cancel)
        
        let leaveChannelAction = UIAlertAction(title: "Да, выйти".localized(), style: .default, handler: { _ in
            self.employeeStore.logout()
            self.showSpinner()
        })
        
        leaveChannelAction.setValue(styleguide.errorColor, forKey: "titleTextColor")
        
        [leaveChannelAction, cancelAction].forEach{alert.addAction($0)}
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc func addRestaurant() {
        let value = OwnerEvents.NavigationEvent.OpenCreateRestaurantScreen.Value(animated: true)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenCreateRestaurantScreen.self, result: Result(value: value))
    }

    @objc func getUserInfo() {
        employeeStore.getUserInfo()
    }
    
}

extension OwnerWorkPlacesViewController: RestaurantsCollectionViewServiceDelegate {
    
    private func configurateCollectionView() {
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        restaurantCollectionViewService = RestaurantsCollectionViewService(collectionView: collectionView, styleguide: styleguide)
        
        restaurantCollectionViewService.configurate(with: self)
    }
    
    private func handleUserInfo(_ userInfo: UserInfo) {
        handleEmptyWorkPlaces(userInfo.workPlaces)
        
        restaurantCollectionViewService.updateRestaurantRoles(restaurantsRoles: userInfo.workPlaces, workDay: userInfo.workDay)
    }
    
    private func handleEmptyWorkPlaces(_ workPlaces: [UserInfo.WorkPlace]) {
        if workPlaces.count == 0 {
            self.collectionView?.showEmptyListMessage("Список Ваших заведений пуст \nПотяните вниз, что бы обновить 👆 \n \nИли создайте заведение 🏗".localized(), textColor: .white)
        } else {
            self.collectionView?.showEmptyListMessage("", textColor: .white)
        }
    }
    
    
    func restaurantsCollectionViewService(_ service: RestaurantsCollectionViewService, didChoseWorkPlace workPlace: UserInfo.WorkPlace) {
        let value = OwnerEvents.NavigationEvent.OpenWorkPlaceMainScreen.Value(workPlace: workPlace, animated: true)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenWorkPlaceMainScreen.self, result: Result(value: value))
    }
    
    func restaurantsCollectionViewService(_ service: RestaurantsCollectionViewService, didTapWorkPlaceInfoButton didChoseWorkPlace: UserInfo.WorkPlace) {
        
    }

}


extension OwnerWorkPlacesViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
    }
    
}

extension OwnerWorkPlacesViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            refreshControl.tintColor = .white
            
            if !employeeStore.isLoading {
                self.refreshControl.endRefreshing()
                self.hideSpinner()
            } else {
                guard !isSpinerPresented else { return }
                
                self.refreshControl.beginRefreshingManually()
            }
        }
        
        if change.contains(.userInfo) {
            guard let userInfo = employeeStore.userInfo else { return }
            
            handleUserInfo(userInfo)
        }
    }
    
}

