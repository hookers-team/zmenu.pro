//
//  MakeMixViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit
import Photos

//KS: TODO: This class shoud be refactored!

final class MakeMixViewController: UIViewController {

    @IBOutlet weak var globalScrollView: UIScrollView!
    @IBOutlet weak var stepsScrollView: UIScrollView!
    @IBOutlet weak var mixDescriptionScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var stepsScrollContainerView: UIView!
    @IBOutlet weak var mixFaceContainerView: UIView!
    @IBOutlet weak var mixBackContainerView: UIView!
    @IBOutlet weak var priceBlurView: UIVisualEffectView!
    
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    @IBOutlet weak var continueButtonContainer: UIView!
    
    @IBOutlet private weak var strenghtLevelLabel: UILabel!
    @IBOutlet private weak var mixNameBackBlurView: UIVisualEffectView!
    @IBOutlet private weak var mixNameBackLabel: UILabel!
    @IBOutlet private weak var mixNameFrontLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var flaworLabel: UILabel!
    @IBOutlet private weak var mixImageView: UIImageView!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var bowlLabel: UILabel!
    @IBOutlet private weak var categoryLabel: UILabel!
    @IBOutlet private weak var globalContainerStackView: UIStackView!
    
    var editedMix: Mix?
    var currentWorkPlace: RestaurantModel!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    let mixNameSlideView = MixNameSlideView.loadFromNib()
    let selectPhotoSlideView = SelectPhotoSlideView.loadFromNib()
    let flaworSlideView = TobaccoRecipeSlideView.loadFromNib()
    let descriptionSlideView = MixDescriptionSlideView.loadFromNib()
    let selectBowlSlideView = SelectBowlSlideView.loadFromNib()
    
    @IBOutlet weak var mixNameLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewStaticHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topPreviewImageConstraint: NSLayoutConstraint!
    
    var slides: [UIView & Slide] = [];
    var onceLayoutSubviews = false

    @objc func nextStep() {
        view.endEditing(true)
        
        var contentOffset = stepsScrollView.contentOffset
        
        guard contentOffset.x < stepsScrollContainerView.frame.width * CGFloat(slides.count - 1) else {
            
            guard let mixName = mixNameSlideView.mixNameTextField.text, !mixName.isEmpty else {
                contentOffset.x = 0
                stepsScrollView.setContentOffset(contentOffset, animated: true)
                showAlertWithMessage("Нужно дать миксу название. У тебя же есть имя?".localized())
                return
            }
            
            guard !flaworSlideView.currentTobaccosList().isEmpty else {
                contentOffset.x = stepsScrollView.frame.width
                stepsScrollView.setContentOffset(contentOffset, animated: true)
                showAlertWithMessage("Укажи состав табаков или выбери в разделе \"Другие\" \"Секретный рецепт\"".localized())
                return
            }
            
            guard let image = mixImageView.image, image != UIImage(named: "z_violet") else {
                contentOffset.x = 3 * stepsScrollView.frame.width
                stepsScrollView.setContentOffset(contentOffset, animated: true)
                showAlertWithMessage("Миксу необходима фото-ассоциация, что бы клиент сделал эмоциональный выбор.".localized())
                return
            }
            
            guard let price = descriptionSlideView.priceTextField.text, !price.isEmpty else {
                showAlertWithMessage("Укажи цену микса, а если он бесплатный, укажи 0.01.".localized())
                return
            }
            
            guard descriptionSlideView.mixDescriptionTextView.textColor == .white else { //This workaround with color for placeholder in textView.
                showAlertWithMessage("Добавь описание микса, почувствуй себя маркетологом.".localized())
                return
            }
            
            var imageFile: Data? = nil
            
            if selectPhotoSlideView.isImageChanged {
                let pngData = image.pngData()
                let iamgeData = Double(pngData?.count ?? 0) / 1024.0 > 2000 ? image.jpegData(compressionQuality: 0.5) : pngData
                
                imageFile = iamgeData
            }
            
            //KS: TODO: Should configurate it for concrete master.
            
            let mix = MixRequest(mixId: editedMix?.mixId, name: mixName, imageURL: editedMix?.imageURL, price: price, categoryId: mixNameSlideView.selectedCategoryId(), restaurantId: currentWorkPlace.restaurantId, strength: MixStrenghtLevel(rawValue: selectPhotoSlideView.currentStrenghtLevel.categoryId) ?? .light, likes: editedMix?.likes ?? "0", flawor: flaworSlideView.currentTobaccosList(), description: descriptionSlideView.mixDescriptionTextView.text, bowl: selectBowlSlideView.currentHokahBowl(), filling: "Вода".localized(), imageFile: imageFile)
            
            //KS: TODO: Filling should be locolized right for server, maybe need to use fillingType.
            
            employeeStore.createOrUpdateMix(mix)
            
            return
        }
        
        contentOffset.x = CGFloat(pageControl.currentPage + 1) * stepsScrollView.frame.width
        
        stepsScrollView.setContentOffset(contentOffset, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateNavBar()
        hideKeyboardWhenTappedAround()
        
        ctaButtonContainerView.ctaButton.setTitle("Следующий шаг".localized(), for: .normal)
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(nextStep), for: .touchUpInside)
        
        refreshUI(withStyleguide: styleguide)
    }

    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }

    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            
            self.view.layoutIfNeeded()
            
            configurateSlides()
            configurateUI()
            setupSlideScrollView(slides: slides)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        globalScrollView.contentOffset.y = 0
        globalScrollView.contentSize = globalContainerStackView.frame.size
        mixNameSlideView.pickCategory(categoryId: MixCategoryType.mint.rawValue)
        selectBowlSlideView.configurateBowl(bowlType: editedMix?.bowl)
        
        self.view.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configurateNavBar()
    }
    
    private func showAlertWithMessage(_ message: String) {
        presentError(withTitle: "\(employeeStore.userInfo?.profile?.name ?? "Красавчик".localized()), не халтурь".localized(), subtitle: message)
    }
    
    private func configurateUI() {
        stepsScrollView.delegate = self
        
        mixBackContainerView.layer.cornerRadius = 12
        mixFaceContainerView.layer.cornerRadius = 12
        
        mixNameFrontLabel.addDefaultSoftShadow()
        mixNameBackLabel.addDefaultSoftShadow()
        mixNameBackBlurView.layer.cornerRadius = 6
        priceBlurView.layer.cornerRadius = 6
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        
        stackViewStaticHeightConstraint.constant = mixImageView.frame.width * 1.3
        view.bringSubviewToFront(pageControl)
    }
    
    private func configurateSlides() {
        selectBowlSlideView.configurate(with: styleguide, delegate: self)
        selectPhotoSlideView.configurate(with: styleguide, delegate: self)
        selectPhotoSlideView.delegate = self
        flaworSlideView.configurate(with: styleguide, delegate: self)
        descriptionSlideView.configurate(with: styleguide, delegate: self)
        mixNameSlideView.configurate(with: styleguide, delegate: self)
        
        slides = [mixNameSlideView, flaworSlideView, selectBowlSlideView, selectPhotoSlideView, descriptionSlideView]
       
        slides.forEach {
            $0.configurate()
            $0.frame = stepsScrollView.frame
            
            $0.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }
        
        mixNameSlideView.pickCategory(categoryId: MixCategoryType.mint.rawValue)
        selectPhotoSlideView.pickStrenghteLevel(MixStrenghtLevel.semiLight.rawValue)
        
        if let editedMix = editedMix {
            prepareForEdit(editedMix)
        }
    }
    
    private func prepareForEdit(_ mix: Mix) {
        mixNameSlideView.mixNameTextField.text = mix.name
        mixNameFrontLabel.text = mix.name
        mixNameBackLabel.text = mix.name
        
        mixNameSlideView.pickCategory(categoryId: mix.categoryId)
        
        selectPhotoSlideView.mixName = editedMix?.name
        
        flaworSlideView.configurate(with: mix.flawor)
        
        mixImageView.download(image: mix.imageURL ?? "", placeholderImage: UIImage(named: "z_violet"))
        
        priceLabel.text = String(mix.price)
        descriptionSlideView.priceTextField.text = String(mix.price)
        
        descriptionSlideView.mixDescriptionTextView.text = mix.description
        descriptionSlideView.mixDescriptionTextView.textColor = .white
        
        descriptionLabel.text = (descriptionLabel.text ?? "") + mix.description
    }
    
    private func configurateNavBar() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        tabBarController?.tabBar.isHidden = true
        
        navigationItem.setTitleView(withTitle: "Создание микса".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
}

extension MakeMixViewController {
    
    func setupSlideScrollView(slides : [Slide & UIView]) {
        stepsScrollView.frame = CGRect(x: 0, y: 0, width: stepsScrollContainerView.frame.width, height: stepsScrollContainerView.frame.height)
        stepsScrollView.contentSize = CGSize(width: stepsScrollContainerView.frame.width * CGFloat(slides.count), height: stepsScrollContainerView.frame.height)
        stepsScrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: stepsScrollContainerView.frame.width * CGFloat(i), y: 0, width: stepsScrollContainerView.frame.width, height: stepsScrollContainerView.frame.height)
            stepsScrollView.addSubview(slides[i])
        }
        self.view.layoutSubviews()
    }
    
    func indexOfSlideView(slideView: UIView & Slide) -> Int {
        return slides.index(where: { $0 === slideView }) ?? 0
    }
 
}

extension MakeMixViewController: UITextFieldDelegate, UITextViewDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == styleguide.senderTextColor {
            textView.text = ""
            textView.textColor = UIColor.white
        }
        
        
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            
            textView.text = "Введите описание микса. Например: Pear Darkside + American Pie Serbetli в пропорции 50/50. Крайне оригинальный вкус, который имеет богатый букет оттенков и является одновременно свежим, сладким и терпким.".localized()
            textView.textColor = styleguide.senderTextColor
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            
            return true
        }
        
        guard var textViewText = textView.text else { return false }
        
        if textViewText.count > 0 {
            textViewText = text == "" ? String(textViewText[..<textViewText.index(before: textViewText.endIndex)]) : textViewText //for spaceback.
        }
        
        descriptionLabel.text = "• Описание: ".localized() + textViewText + text
        
        let diff = mixDescriptionScrollView.contentSize.height - mixBackContainerView.frame.height
        
        guard diff > 0 else { return true }
        
        mixDescriptionScrollView.contentOffset.y = diff //scrolling to bottom when content bigger then container.

        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard var text = textField.text else { return false }
        
        text = string == "" ? String(text[..<text.index(before: text.endIndex)]) : text //for spaceback.
        
        if textField == mixNameSlideView.mixNameTextField {
            mixNameFrontLabel.text = text + string
            mixNameBackLabel.text = text + string
            
            selectPhotoSlideView.mixName = mixNameBackLabel.text
        } else if textField == descriptionSlideView.priceTextField {
            priceLabel.text = text + string + " ₴"
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
}

extension MakeMixViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
        let maximumHorizontalOffset: CGFloat = scrollView.contentSize.width - scrollView.frame.width
        let currentHorizontalOffset: CGFloat = scrollView.contentOffset.x
        
        let maximumVerticalOffset: CGFloat = scrollView.contentSize.height - scrollView.frame.height
        let currentVerticalOffset: CGFloat = scrollView.contentOffset.y
        
        let percentageHorizontalOffset: CGFloat = currentHorizontalOffset / maximumHorizontalOffset
        let percentageVerticalOffset: CGFloat = currentVerticalOffset / maximumVerticalOffset
        
        let percentOffset: CGPoint = CGPoint(x: percentageHorizontalOffset, y: percentageVerticalOffset)
        
        if(percentOffset.x > 0 && percentOffset.x <= 0.25) {

            slides[0].containerView.transform = CGAffineTransform(scaleX: (0.25-percentOffset.x)/0.25, y: (0.25-percentOffset.x)/0.25)
            slides[1].containerView.transform = CGAffineTransform(scaleX: percentOffset.x/0.25, y: percentOffset.x/0.25)

        } else if(percentOffset.x > 0.25 && percentOffset.x <= 0.50) {
            slides[1].containerView.transform = CGAffineTransform(scaleX: (0.50-percentOffset.x)/0.25, y: (0.50-percentOffset.x)/0.25)
            slides[2].containerView.transform = CGAffineTransform(scaleX: percentOffset.x/0.50, y: percentOffset.x/0.50)

        } else if(percentOffset.x > 0.50 && percentOffset.x <= 0.75) {
            slides[2].containerView.transform = CGAffineTransform(scaleX: (0.75-percentOffset.x)/0.25, y: (0.75-percentOffset.x)/0.25)
            slides[3].containerView.transform = CGAffineTransform(scaleX: percentOffset.x/0.75, y: percentOffset.x/0.75)

        } else if(percentOffset.x > 0.75 && percentOffset.x <= 1) {
            slides[3].containerView.transform = CGAffineTransform(scaleX: (1-percentOffset.x)/0.25, y: (1-percentOffset.x)/0.25)
            slides[4].containerView.transform = CGAffineTransform(scaleX: percentOffset.x, y: percentOffset.x)
        }
        
        
        if pageControl.currentPage == indexOfSlideView(slideView: flaworSlideView) {
            hideMixesPreview()
        } else if stackViewStaticHeightConstraint.constant == 0 {
            showMixesPreview()
        }
        
    }
    
    func showMixesPreview() {
        UIView.animate(withDuration: 0.2) {
            self.stackViewStaticHeightConstraint.constant = self.mixImageView.frame.width * 1.3
            //self.topPreviewImageConstraint.constant = 5
            self.view.layoutIfNeeded()
        }
    }
    
    func hideMixesPreview() {
        UIView.animate(withDuration: 0.2) {
            self.stackViewStaticHeightConstraint.constant = 0
            //self.topPreviewImageConstraint.constant = 5
            self.view.layoutIfNeeded()
        }
    }
    
}

extension MakeMixViewController: TobaccoRecipeSlideViewDelegate {
    
    func flaworRecipeSlideView(_ flaworRecipeSlideView: TobaccoRecipeSlideView, didSelectTobacco flawor: DisplayableTobacco) {
        guard let currentText = flaworLabel.text else { return }
        
        let flaworDescriprion = "\(flawor.brand)" + "(\(flawor.name))"
        
        if currentText == "• Вкусы:".localized() {
            flaworLabel.text = currentText + " " + flaworDescriprion
        } else {
            flaworLabel.text = currentText + ", " + flaworDescriprion
        }
        
    }
    
    func flaworRecipeSlideView(_ flaworRecipeSlideView: TobaccoRecipeSlideView, didRemoveTobacco flawor: DisplayableTobacco) {
        guard let currentText = flaworLabel.text else { return }
        
        let flaworDescriprion = "\(flawor.brand)" + "(\(flawor.name))"
        
        if currentText == "• Вкусы:".localized() {
            flaworLabel.text = currentText + " " + flaworDescriprion
        } else {
            flaworLabel.text = currentText + ", " + flaworDescriprion
        }
    }
    
    func flaworRecipeSlideView(_ flaworRecipeSlideView: TobaccoRecipeSlideView, updateTobaccos flawors: [DisplayableTobacco]) {
        let textTobaccos = flawors.reduce([String: String](), { (dict, flawor) -> [String: String] in
            var dict = dict
            let key = flawor.brand
            
            if var existingTobaccos = dict[key] {
                existingTobaccos.append(", \(flawor.name)")
                dict.updateValue(existingTobaccos, forKey: key)
            } else {
                dict.updateValue(flawor.name, forKey: key)
            }
            
            return dict
        })
        
        var resultText = ""
        
        textTobaccos.keys.forEach { key in
            guard let names = textTobaccos[key] else { return }
            
            resultText.isEmpty ? resultText.append("\(key)(\(names))") : resultText.append(", \(key)(\(names))")
        }
        
        flaworLabel.text = "• Вкусы: ".localized() + resultText
    }
    
}

extension MakeMixViewController: BowlCollectionViewServiceDelegate {
    
    func serviceDidChoseBowl(_ service: BowlCollectionViewService, chosenBowl bowl: Bowl) {
        let currentTextLabel = "• Чаша: "
        
        bowlLabel.text = currentTextLabel + bowl.name
    }
    
}

extension MakeMixViewController: MixNameSlideViewDelegate {
    
    func mixNameSlideView(_ mixNameSlideView: MixNameSlideView, didChoose mixCategory: MixCategorizedData) {
        let currentTextLabel = "• Категория: "
        
        categoryLabel.text = currentTextLabel + mixCategory.categoryName
    }
    
}

extension MakeMixViewController: SelectPhotoSlideViewDelegate {
    
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didChoose strenghtLevel: MixCategorizedData) {
        strenghtLevelLabel.text = "• Крепость: " + strenghtLevel.categoryName.components(separatedBy: "(")[0]
    }
    
    func didCloseWebView(_ selectPhotoSlideView: SelectPhotoSlideView) {
        showMixesPreview()
    }
    
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didTapGalleryPhotoButton button: UIButton) {
        openGallery()
    }
    
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didTapCameraButton button: UIButton) {
        openCamera()
    }
    
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didTapPhotoBrowserButton button: UIButton) {
        hideMixesPreview()
    }
    
    func selectPhotoSlideView(_ selectPhotoSlideView: SelectPhotoSlideView, didFindImage image: UIImage) {
        
        mixImageView.image = image
        editedMix?.imageURL = nil
        showMixesPreview()
    }
    
}

extension MakeMixViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //KS: TODO: think about separate service for image picker later
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[.editedImage] as? UIImage {
            mixImageView.image = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage {
            mixImageView.image = originalImage
        }
        
        selectPhotoSlideView.isImageChanged = true
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        
        dismiss(animated: true, completion: nil)
    }
    
    private func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            self.employeeStore.permissionService.setupPermissionService(withPermissionType: .photos)
            
            let photoLibraryPermissionStatus = employeeStore.photoLibraryPermissionStatus
            
            let picker = self.createAndConfigureImagePickerController()
            picker.sourceType = .photoLibrary
            
            //KS: TODO: нужно, чтобы правильно красить barButtonItem по всему приложению. Позже обсудить и уйти от этого решения
            UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: self.styleguide.primaryColor], for: .normal)
            
            switch photoLibraryPermissionStatus {
            case .authorized:
                self.present(picker, animated: true)
            case .notDetermined, .denied, .disabled: //KS: TODO: study what .disabled status means later
                self.employeeStore.permissionService.photoLibraryService.requestAuthorization(completion: { [weak self] status in
                    if status == .authorized {
                        self?.present(picker, animated: true)
                    }
                })
            }
        } else {
            presentError(message: "Галерея недоступна".localized())
        }
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.employeeStore.permissionService.setupPermissionService(withPermissionType: .camera)
            
            let cameraPermissionStatus = employeeStore.cameraPermissionStatus
            
            let picker = createAndConfigureImagePickerController()
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
            
            switch cameraPermissionStatus {
            case .authorized:
                self.present(picker, animated: true)
            case .notDetermined, .denied, .disabled:
                self.employeeStore.permissionService.cameraService.requestAuthorization(completion: { [weak self] status in
                    if status == .authorized {
                        self?.present(picker, animated: true)
                    }
                })
            }
        } else {
            presentError(message:"Камера недоступна".localized())
        }
    }
    
    private func createAndConfigureImagePickerController() -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        
        return picker
    }
    
    
}

extension MakeMixViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
    }
    
}


extension MakeMixViewController {
    
    func cameraAuthorizationStatusAlert() -> UIAlertController {
        let alert = UIAlertController(title: "Доступ к камере ограничен".localized(), message: "Разрешить приложению использовать камеру".localized(), preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Настройки".localized(), style: .default, handler: { _ in
            if #available(iOS 10.0, *) {
                let settingsUrl = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(settingsUrl)
            } else {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Отмена".localized(), style: .cancel)
        
        alert.addAction(settingsAction)
        alert.addAction(cancelAction)
        
        return alert
    }
    
}

extension MakeMixViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? startCTAButtonLoadingAnimation() : stopCTAButtonLoadingAnimation()
        }
    }
    
}
