//
//  EditEmployeeProfileViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

fileprivate struct LocalizedStringConstants {
    
    static var navigationItemTitle: String { return "Редактирование профиля".localized() }
    static var actionSheetTitle: String { return "Фотография".localized() }
    static var actionSheetMessage: String { return "Вы можете выбрать фото из галереи, сделать новое фото либо удалить текущую фотографию".localized() }
    static var pickPhotoFromGallery: String { return "Выбрать из галереи".localized() }
    static var takePhoto: String { return "Сделать фото".localized() }
    static var deletePhoto: String { return "Удалить фото".localized() }
    static var cancel: String { return "Отмена".localized() }
    static var change: String { return "Изменить".localized() }
    static var cameraUnavailable: String { return "Камера недоступна".localized() }
    static var galleryUnavailable: String { return "Галерея недоступна".localized() }
    static var errorTitle: String { return "Без паники 👻".localized() }
    static var descriptionExample: String { return "Добавьте описание. Например: Кальянщик в 3 поколении, мой дед забивал кальян Фиделю Кастра на Кубе.".localized() }
    
}

final class EditEmployeeProfileViewController: UIViewController {

    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    var profile: UserInfo.Profile? {
        return employeeStore.userInfo?.profile
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var instagramTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var avatarButton: BouncesButton!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageContainerView: UIView!
    
    private var onceLayoutSubviews = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ctaButtonContainerView.ctaButton.setTitle("Сохранить".localized(), for: .normal)
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(save), for: .touchUpInside)
        
        configurateNavBar()
        prepareForEdit()
        avatarButton.imageView?.contentMode = UIView.ContentMode.scaleAspectFill
        refreshUI(withStyleguide: styleguide)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            
            imageHeightConstraint.constant = scrollView.frame.height / 4
            imageContainerView.layer.cornerRadius = imageHeightConstraint.constant / 2
        }
    }

    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }

    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
    private func configurateNavBar() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Профиль сотрудника".localized(),
                                    subtitle: "Редактирование".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    private func prepareForEdit() {
        nameTextField.text = profile?.name
        instagramTextField.text = profile?.instagram
        
        if let description = profile?.description {
            //For textView workarounded placeholder.
            descriptionTextView.text = description
            descriptionTextView.textColor = .white
        } else {
            descriptionTextView.text = LocalizedStringConstants.descriptionExample
            descriptionTextView.textColor = styleguide.senderTextColor
        }
        
        avatarButton.sd_setImage(with: URL(string: profile?.imageURL ?? ""), for: .normal, placeholderImage: UIImage(named: "ava_default"))
    }
 
    @IBAction func editPhoto(_ sender: Any) {
        let actionSheet = UIAlertController(title: LocalizedStringConstants.actionSheetTitle, message: LocalizedStringConstants.actionSheetMessage, preferredStyle: .actionSheet)
        
        let openGalleryAction = UIAlertAction(title: LocalizedStringConstants.pickPhotoFromGallery, style: .default, handler: { action in
            self.openGallery()
        })
        let openCameraAction = UIAlertAction(title: LocalizedStringConstants.takePhoto, style: .default, handler: { action in
            self.openCamera()
        })
        let deleteAction = UIAlertAction(title: LocalizedStringConstants.deletePhoto, style: .destructive, handler: { action in
            self.employeeStore.editEmployeePhoto(imageFile: nil, oldImageURL: self.profile?.imageURL)
        })
        let cancelAction = UIAlertAction(title: LocalizedStringConstants.cancel, style: .cancel, handler: nil)
        
        [openGalleryAction, openCameraAction, deleteAction, cancelAction].forEach {
            actionSheet.addAction($0)
        }
        
        actionSheet.view.tintColor = styleguide.primaryColor
        
        if let presenter = actionSheet.popoverPresentationController {
            presenter.sourceView = avatarButton
            presenter.sourceRect = avatarButton.bounds
            presenter.permittedArrowDirections = .up
        }
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    @objc func save() {
        guard validateNameTextField(), let name = nameTextField.text, let profile = profile else { return }
        
        let descriptionText = descriptionTextView.text == LocalizedStringConstants.descriptionExample ? nil : descriptionTextView.text
        
        employeeStore.editProfile(name: name, instagram: instagramTextField.text, address: profile.address, scedule: profile.schedule, description: descriptionText, imageURL: profile.imageURL)
    }
    
}

extension EditEmployeeProfileViewController {
    
    private func validateNameTextField() -> Bool {
        return validateIsEmtyTextField(nameTextField, errorMessage: "Вам необходимо ввести имя или nickname".localized())
    }
    
    private func validateIsEmtyTextField(_ textField: UITextField, errorMessage: String) -> Bool {
        let isValid = !(textField.text ?? "").isEmpty
        
        if !isValid {
            presentError(withTitle: LocalizedStringConstants.errorTitle, subtitle: errorMessage)
        }
        
        return isValid
    }

}

extension EditEmployeeProfileViewController: UITextViewDelegate {
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == styleguide.senderTextColor {
            textView.text = ""
            textView.textColor = UIColor.white
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            
            textView.text = LocalizedStringConstants.descriptionExample
            textView.textColor = styleguide.senderTextColor
        }
    }
    
}

extension EditEmployeeProfileViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        
        return true
    }
    
}

extension EditEmployeeProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //KS: TODO: think about separate service for image picker later
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            let pngData = pickedImage.pngData()
            let imageData = Double(pngData?.count ?? 0) / 1024.0 > 2000 ? pickedImage.jpegData(compressionQuality: 0.5) : pngData
            employeeStore.editEmployeePhoto(imageFile: imageData, oldImageURL: profile?.imageURL)
        }
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        
        dismiss(animated: true, completion: nil)
    }
    
    private func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            self.employeeStore.permissionService.setupPermissionService(withPermissionType: .photos)
            
            let photoLibraryPermissionStatus = employeeStore.photoLibraryPermissionStatus
            
            let picker = self.createAndConfigureImagePickerController()
            picker.sourceType = .photoLibrary
            
            //KS: TODO: нужно, чтобы правильно красить barButtonItem по всему приложению. Позже обсудить и уйти от этого решения
            UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: self.styleguide.primaryColor], for: .normal)
            
            switch photoLibraryPermissionStatus {
            case .authorized:
                self.present(picker, animated: true)
            case .notDetermined, .denied, .disabled: //KS: TODO: study what .disabled status means later
                self.employeeStore.permissionService.photoLibraryService.requestAuthorization(completion: { [weak self] status in
                    if status == .authorized {
                        self?.present(picker, animated: true)
                    }
                })
            }
        } else {
            presentError(message: LocalizedStringConstants.galleryUnavailable)
        }
    }
    
    private func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.employeeStore.permissionService.setupPermissionService(withPermissionType: .camera)
            
            let cameraPermissionStatus = employeeStore.cameraPermissionStatus
            
            let picker = createAndConfigureImagePickerController()
            picker.sourceType = .camera
            picker.cameraCaptureMode = .photo
            
            switch cameraPermissionStatus {
            case .authorized:
                self.present(picker, animated: true)
            case .notDetermined, .denied, .disabled:
                self.employeeStore.permissionService.cameraService.requestAuthorization(completion: { [weak self] status in
                    if status == .authorized {
                        self?.present(picker, animated: true)
                    }
                })
            }
        } else {
            presentError(message: LocalizedStringConstants.cameraUnavailable)
        }
    }
    
    private func createAndConfigureImagePickerController() -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        
        return picker
    }
    
}

extension EditEmployeeProfileViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
        
        nameTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
        instagramTextField.setValue(styleguide.senderTextColor, forKeyPath: "_placeholderLabel.textColor")
    }
    
}

extension EditEmployeeProfileViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
        
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? startCTAButtonLoadingAnimation() : stopCTAButtonLoadingAnimation()
        }
        
        if change.contains(.userProfile) {
            prepareForEdit()
        }
    }
    
}
