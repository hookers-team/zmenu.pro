//
//  EmployeeMainViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 25.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import UIKit

final class EmployeeMainViewController: UIViewController {
    
    var currentWorkPlace: UserInfo.WorkPlace!
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var averageRatingLabel: UILabel!
    @IBOutlet weak var totalOrderLabel: UILabel!
    
    @IBOutlet weak var ratingContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adminsContainerView: UIView!
    @IBOutlet weak var addMasterContainerView: UIView!
    @IBOutlet weak var addAdministratorContainerView: UIView!
    @IBOutlet weak var editMenuContainerView: UIView!
    @IBOutlet weak var employeeListContainerView: UIView!
    @IBOutlet weak var createMixContainerView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var avatarMaskImageView: UIImageView!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    @IBOutlet weak var adminActionsViewHeightcConstraint: NSLayoutConstraint!
    @IBOutlet weak var editRestaurantContainerView: UIView!
    @IBOutlet weak var leaveRestaurantContainerView: UIView!
    @IBOutlet weak var leaveRestaurantContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var orderListContainerView: UIView!
    
    private var onceLayoutSubviews = false
    private var profile: UserInfo.Profile? {
        return employeeStore.userInfo?.profile
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = styleguide.backgroundScreenColor
        avatarMaskImageView.tintColor = styleguide.backgroundScreenColor
    
        refreshUI(withStyleguide: styleguide)
        configurateIdentity()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            
            ctaButtonContainerView.ctaButton.cornerRadius = 15
            avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameLabel.text = profile?.name
        descriptionLabel.text = profile?.description
        avatarImageView.download(image: profile?.imageURL ?? "", placeholderImage: UIImage(named: "ava_default"))
        
        configurateNavBar()
        
        employeeStore.userInfo?.workDay == nil ? startWorkDayButton() : endWorkDayButton()
        tabBarController?.tabBar.isHidden = true
    }
    
    private func configurateNavBar() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: currentWorkPlace.restaurant.name,
                                    subtitle: UserRole.displayableRole(currentWorkPlace.role).localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
    }
    
    private func configurateIdentity() {
        if currentWorkPlace.role == UserRole.master {
            adminActionsViewHeightcConstraint.constant = 0
            leaveRestaurantContainerTopConstraint.constant = 0
            adminsContainerView.isHidden = true
        }
        
        if let averageRate = profile?.averageRate, let totalOrders = profile?.ordersCount, let rating = profile?.rating {
            ratingContainerViewHeight.constant = 100 //KS: This constants from height of 3 labels in storyboard.
            ratingLabel.text = "Рейтинг:" + " " + rating + " ⭐️"
            averageRatingLabel.text = "Средняя оценка:".localized() + " " + averageRate
            totalOrderLabel.text = "Забил кальянов: ".localized() + " " + totalOrders
        } else {
            ratingContainerViewHeight.constant = 0
        }
    }
    
    private func startWorkDayButton() {
        ctaButtonContainerView.ctaButton.setTitle("Начать рабочий день".localized(), for: .normal)
        ctaButtonContainerView.ctaButton.removeTarget(self, action: #selector(endWorkDay), for: .touchUpInside)
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(startWorkDay), for: .touchUpInside)
         view.layoutIfNeeded()
    }
    
    private func endWorkDayButton() {
        ctaButtonContainerView.ctaButton.setTitle("Закончить рабочий день".localized(), for: .normal)
        
        ctaButtonContainerView.ctaButton.removeTarget(self, action: #selector(startWorkDay), for: .touchUpInside)
        
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(endWorkDay), for: .touchUpInside)
        view.layoutIfNeeded()
    }
    
    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation(with completion: @escaping (() -> Void)) {
        ctaButtonContainerView.stopAnimation {
            completion()
        }
        view.isUserInteractionEnabled = true
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    @IBAction func editRestaurant(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenEditProfileScreen.Value(workPlace: currentWorkPlace, animated: true)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenEditProfileScreen.self, result: Result(value: value))
    }
    
    @IBAction func editMenu(_ sender: UIButton) {
        let value = MasterEvents.NavigationEvent.OpenEditMenuScreen.Value(workPlace: currentWorkPlace, animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenEditMenuScreen.self, result: Result(value: value))
    }
    
    @IBAction func orderList(_ sender: Any) {
        let value = OrdersEvent.NavigationEvent.ShowOrderList.Value(workPlace: currentWorkPlace)
        
        dispatcher.dispatch(type: OrdersEvent.NavigationEvent.ShowOrderList.self, result: Result(value: value))
    }
    
    @objc func startWorkDay() {
        let value = MasterEvents.NavigationEvent.OpenStartWorkDayScreen.Value(animated: true, workPlace: currentWorkPlace)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenStartWorkDayScreen.self, result: Result(value: value))
    }
    
    @objc func endWorkDay() {
        employeeStore.endWorkDay()
    }
    
    @IBAction func leave(_ sender: Any) {
        showLeaveEmployeeAlert(restaurantName: currentWorkPlace.restaurant.name, employeeId: nil, role: currentWorkPlace.role.rawValue, restaurantId: currentWorkPlace.restaurant.restaurantId)
    }
    
    @IBAction func createMix(_ sender: Any) {
        let value = MasterEvents.NavigationEvent.OpenMakeMixScreen.Value(workPlace: currentWorkPlace, animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenMakeMixScreen.self, result: Result(value: value))
    }
    
    @IBAction func editProfile(_ sender: Any) {
        let value = MasterEvents.NavigationEvent.OpenEditProfileScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenEditProfileScreen.self, result: Result(value: value))
    }
    
    @IBAction func addMaster(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenQrScannerScreen.Value(animated: true, role: .master, restaurantId: currentWorkPlace.restaurant.restaurantId)

        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenQrScannerScreen.self, result: Result(value: value))
    }
    
    @IBAction func addAdministator(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenQrScannerScreen.Value(animated: true, role: .admin, restaurantId: currentWorkPlace.restaurant.restaurantId)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenQrScannerScreen.self, result: Result(value: value))
    }
    
    @IBAction func stuffList(_ sender: Any) {
        let value = OwnerEvents.NavigationEvent.OpenEmployeeList.Value(animated: true, workPlace: currentWorkPlace)
        
        dispatcher.dispatch(type: OwnerEvents.NavigationEvent.OpenEmployeeList.self, result: Result(value: value))
    }
    
}

extension EmployeeMainViewController {
    
    @objc func showLeaveEmployeeAlert(restaurantName: String, employeeId: String?, role: String, restaurantId: String) {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что  хотите покинуть заведение \(restaurantName)?".localized(), preferredStyle: .alert)
        
        let attributedTitle = NSAttributedString(string: "Увольнение из заведения 😔".localized(), attributes:  [NSAttributedString.Key.font: styleguide.boldFont(ofSize: 17)])
        
        alert.setValue(attributedTitle, forKey: "attributedTitle")
        
        let cancelAction = UIAlertAction(title: "Отменить".localized(), style: .cancel)
        
        let leaveChannelAction = UIAlertAction(title: "Да, покинуть".localized(), style: .default, handler: { _ in
            self.employeeStore.leaveRestaurant(role: role, restaurantId: restaurantId)
        })
        
        leaveChannelAction.setValue(styleguide.errorColor, forKey: "titleTextColor")
        
        [leaveChannelAction, cancelAction].forEach{alert.addAction($0)}
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension EmployeeMainViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        
        let views = [addMasterContainerView, addAdministratorContainerView, editMenuContainerView, employeeListContainerView, createMixContainerView, editRestaurantContainerView, leaveRestaurantContainerView, orderListContainerView]
        
        views.forEach{
            $0?.layer.cornerRadius = styleguide.cornerRadius
            $0?.layer.borderWidth = 1
        }
        
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
    }
    
}


extension EmployeeMainViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation(with: {
                if change.contains(.workDay) {
                    self.employeeStore.userInfo?.workDay == nil ? self.startWorkDayButton() : self.endWorkDayButton()
                    //This is for correct update title on ctaButton
                }
            })
        }
        
        if change.contains(.leaveRestaurant) {
            back()
        }
        
    }
    
}
