//
//  SelectWorkingPlaceViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

final class SelectWorkingPlaceViewController: UIViewController {

    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backgrounView: UIView!
    @IBOutlet weak var globalBlurView: UIVisualEffectView!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    
    private var onceLayoutSubviews = false
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        let attributes = [ NSAttributedString.Key.foregroundColor: UIColor.white ]
        
        refreshControl.attributedTitle = NSAttributedString(string: "Получаем список Ваших заведений...".localized(), attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(getUserInfo), for: .valueChanged)
        
        refreshControl.tintColor = .white
        
        return refreshControl
    }()
    
    private var restaurantCollectionViewService: RestaurantsCollectionViewService!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(showQr), for: .touchUpInside)
        ctaButtonContainerView.ctaButton.setTitle("Показать QR-код".localized(), for: .normal)
        
        configurateCollectionView()
        configurateNavBar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if !onceLayoutSubviews {
            onceLayoutSubviews = true
            refreshUI(withStyleguide: styleguide)
            handleEmptyWorkPlaces([])
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
            self.globalBlurView.effect = UIBlurEffect(style: .dark)
        }, completion: nil )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getUserInfo()
    }
    
    private func configurateNavBar() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        navigationController?.tabBarController?.tabBar.isHidden = true
        
        navigationItem.setTitleView(withTitle: "Где вы сегодня работаете?".localized(),
                                    subtitle: "Выбор рабочего пространства".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
         navigationItem.addLogoutButton(with: self, action: #selector(showLogoutAlert), tintColor: styleguide.primaryColor)
    }
    
    @objc func showLogoutAlert() {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите выйти из аккаунта?".localized(), preferredStyle: .alert)
        
        let attributedTitle = NSAttributedString(string: "Выход".localized(), attributes:  [NSAttributedString.Key.font: styleguide.boldFont(ofSize: 17)])
        
        alert.setValue(attributedTitle, forKey: "attributedTitle")
        
        let cancelAction = UIAlertAction(title: "Отменить".localized(), style: .cancel)
        
        let leaveChannelAction = UIAlertAction(title: "Да, выйти".localized(), style: .default, handler: { _ in
            self.employeeStore.logout()
            self.showSpinner()
        })
        
        leaveChannelAction.setValue(styleguide.errorColor, forKey: "titleTextColor")
        
        [leaveChannelAction, cancelAction].forEach{alert.addAction($0)}
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func showQr() {
        let value = MasterEvents.NavigationEvent.OpenMyQrScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenMyQrScreen.self, result: Result(value: value))
    }
    
    @objc func getUserInfo() {
        employeeStore.getUserInfo()
    }
        
    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }

}

extension SelectWorkingPlaceViewController: RestaurantsCollectionViewServiceDelegate {
    
    private func configurateCollectionView() {
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        
        restaurantCollectionViewService = RestaurantsCollectionViewService(collectionView: collectionView, styleguide: styleguide)
        
        restaurantCollectionViewService.configurate(with: self)
    }
    
    private func handleUserInfo(_ userInfo: UserInfo) {
        handleEmptyWorkPlaces(userInfo.workPlaces)
        
        restaurantCollectionViewService.updateRestaurantRoles(restaurantsRoles: userInfo.workPlaces, workDay: userInfo.workDay)
    }
    
    private func handleEmptyWorkPlaces(_ workPlaces: [UserInfo.WorkPlace]) {
        if workPlaces.count == 0 {
            self.collectionView?.showEmptyListMessage("Список Ваших заведений пуст \nПотяните вниз, что бы обновить 👆 \n \nАдминистратор или владелец могут добавить Вас в заведение  \nПросто покажите им свой QR-код 🤳", textColor: .white)
        } else {
            self.collectionView?.showEmptyListMessage("", textColor: .white)
        }
    }
    
    func restaurantsCollectionViewService(_ service: RestaurantsCollectionViewService, didChoseWorkPlace workPlace: UserInfo.WorkPlace) {
        let value = MasterEvents.NavigationEvent.OpenMasterMainScreen.Value(workPlace: workPlace, animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.OpenMasterMainScreen.self, result: Result(value: value))
    }
    
    func restaurantsCollectionViewService(_ service: RestaurantsCollectionViewService, didTapWorkPlaceInfoButton didChoseWorkPlace: UserInfo.WorkPlace) {
        
    }
    
}


extension SelectWorkingPlaceViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
    }
    
}

extension SelectWorkingPlaceViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            refreshControl.tintColor = .white
            
            if !employeeStore.isLoading {
                self.refreshControl.endRefreshing()
                self.hideSpinner()
            } else {
                guard !isSpinerPresented else { return }
                
                self.refreshControl.beginRefreshingManually()
            }
        }
        
        if change.contains(.userInfo) {
            guard let userInfo = employeeStore.userInfo else { return }
            
            handleUserInfo(userInfo)
        }
    }
    
}
