//
//  QRViewController.swift
//  ZMenu.Pro
//
//  Created by Stas Chelak on 1/21/19.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

final class QRViewController: UIViewController {
    
    @IBOutlet weak var qrLabel: UILabel!
    @IBOutlet weak var qrCodeImageView: UIImageView!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var employeeStore: EmployeeStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurateViews()
    }
    
    private func configurateViews() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = .clear
        
        navigationItem.addCloseButton(with: self, action: #selector(close), tintColor: styleguide.primaryColor)
        
        view.backgroundColor = styleguide.backgroundScreenColor
        qrLabel.textColor = styleguide.primaryTextColor
        

        qrCodeImageView.image = QRService.makeQRImageWith(userId: employeeStore.userSessionService.accountModel?.userId ?? "", size: qrCodeImageView.frame.size)
    }
    
    @objc func close() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
}

