//
//  DatePickerViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 17.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

final class StartWorkDayViewController: UIViewController {
    
    var employeeStore: EmployeeStore! {
        didSet {
            employeeStore.addDataStateListener(self)
        }
    }
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var currentWorkPlace: UserInfo.WorkPlace!
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var cityContainerView: UIView!
    @IBOutlet private weak var datePicker: UIDatePicker!
    @IBOutlet private weak var dueDateButton: UIButton!
    @IBOutlet private weak var datePickerHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var dueDateContainerView: UIView!
    @IBOutlet private weak var ctaButtonContainerView: CTAButtonContainerView!
    
    private var dueDate: Date!

    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ctaButtonContainerView.ctaButton.setTitle("Начать рабочий день".localized(), for: .normal)
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(accept), for: .touchUpInside)
        
        configurateNavBar()
        refreshUI(withStyleguide: styleguide)
        configurateDatePicker()
        updateDueDateLabel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
  
       
        
        
    }
    
    private func configurateNavBar() {
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "\(currentWorkPlace.restaurant.name)".localized(),
                                    subtitle: "Рабочий день".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
        
        navigationItem.addCloseButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
    }
    
    @objc func back() {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    private func configurateDatePicker() {
        dueDate = Date().adding(hours: 1)

        
        datePicker.minimumDate = dueDate
        datePicker.maximumDate = Date().adding(hours: 24)
        
        datePicker.setValue(UIColor.white, forKeyPath: "textColor")
    }
    
    private func updateDueDateLabel() {
        let formatter = DateFormatter()
        
        formatter.timeStyle = .short
        
        let dueDateString = formatter.string(from: dueDate) == formatter.string(from: Date()) ? "Cейчас".localized() : formatter.string(from: dueDate)
        
        let attributeTitle: NSMutableAttributedString =  NSMutableAttributedString(string: dueDateString)
        attributeTitle.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: NSMakeRange(0, attributeTitle.length))
        
        dueDateButton.setAttributedTitle(attributeTitle, for: .normal)
    }
    
}

extension StartWorkDayViewController {
    
    @objc func accept() {
        guard let restaurantId = currentWorkPlace?.restaurant.restaurantId else {
            employeeStore.showErrorMessage("Ойой, произошла ошибка, обновите список заведений и попробуйте снова".localized())
            return
        }
        
        employeeStore.startWorkDay(restaurantId: restaurantId, role: currentWorkPlace.role, endTime:  String(dueDate.millisecondsSince1970 / 1000))
    }
    
    @IBAction func dateChanged(_ datePicker: UIDatePicker) {
        dueDate = datePicker.date
        updateDueDateLabel()
    }
    
    @IBAction func chooseDueDate(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            if self.datePickerHeightConstraint.constant == 0 {
                self.datePickerHeightConstraint.constant = Constants.dataPickerHeight
                self.datePicker.isHidden = false
            } else {
                self.datePicker.isHidden = true
                self.datePickerHeightConstraint.constant = 0
            }
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
}

extension StartWorkDayViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
        dueDateContainerView.layer.cornerRadius = styleguide.cornerRadius
        dueDateContainerView.layer.borderWidth = 1
        
        cityContainerView.layer.cornerRadius = styleguide.cornerRadius
        cityContainerView.layer.borderWidth = 1
    }
    
}

extension StartWorkDayViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EmployeeStoreStateChange {
            employeeStoreStateChange(change: change)
        }
    }
    
    private func employeeStoreStateChange(change: EmployeeStoreStateChange) {
        if change.contains(.isLoadingState) {
            employeeStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }
        
        if change.contains(.workDay) {
            back()
        }
    }

}

extension StartWorkDayViewController {
    
    struct Constants {
        
        static let dataPickerHeight = CGFloat(140.0)
        
    }
    
}

