//
//  CreatedMixViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 27.12.2018.
//  Copyright © 2018 Skaix. All rights reserved.
//

import UIKit

class CreatedMixViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!

    private var collectionViewService: MixListCollectionViewService!
    @IBOutlet weak var congratulationLabel: UILabel!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var createdMix: Mix!
    var employeeStore: EmployeeStore!
    
    @IBOutlet weak var superButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionViewService = MixListCollectionViewService(collectionView: collectionView)
        collectionView.addShadowView(radius: 30, color: .white)
        collectionViewService.configurate(styleguide: styleguide)
        collectionViewService.updateMixes(with: [DisplayableMix.init(from: createdMix)])
        
        superButton.backgroundColor = .clear
        superButton.layer.borderColor = UIColor.white.cgColor
        superButton.layer.borderWidth = 1
        superButton.layer.cornerRadius = superButton.frame.height / 2
        
        congratulationLabel.text = "Поздравляем, \(employeeStore.userInfo?.profile?.name ?? "Красавчик".localized()), твой микс успешно создан 🤩🔥💥"
        
        navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func `super`(_ sender: Any) {
        let value = MasterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: MasterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    

}

