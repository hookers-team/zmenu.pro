//
//  BowlCollectionViewService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 27.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation

import UIKit

typealias Bowl = (imageName: String, name: String)

protocol BowlCollectionViewServiceDelegate: class {
    
    func serviceDidChoseBowl(_ service: BowlCollectionViewService, chosenBowl bowl: Bowl)
    
}

final class BowlCollectionViewService: NSObject  {
    
    var selectedMaster: Bowl?
    var bowls: [Bowl] = []
    
    private weak var collectionView: UICollectionView?
    private weak var delegate: BowlCollectionViewServiceDelegate?
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
    }
    
    func configurate(with delegate: BowlCollectionViewServiceDelegate) {
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.allowsSelection = true
        collectionView?.registerReusableCell(cellType: BowlCollectionViewCell.self)
        
        let clayBowl = Bowl(imageName: "clay", name: "Глиняная чаша".localized())
        let siliconBowl = Bowl(imageName: "silicon", name: "Силиконовая чаша".localized())
        
        bowls = [clayBowl, siliconBowl]
        
        self.delegate = delegate
    }
    
    func updateMasters(masters: [Bowl]) {
        self.bowls = masters
        
        collectionView?.reloadData()
    }
    
}

extension BowlCollectionViewService: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bowls.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: BowlCollectionViewCell.self)
        
        let bowl = bowls[indexPath.row]
        
        cell.imageView.image = UIImage(named: bowl.imageName)
        cell.nameLabel.text = bowl.name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedBowl = bowls[indexPath.row]
        
        selectedMaster = selectedBowl
        delegate?.serviceDidChoseBowl(self, chosenBowl: selectedBowl)
        
        collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
    }
    
    func selectBowl(bowlType: String) {
        let index = bowls.firstIndex(where: {$0.name == bowlType}) ?? 1
        
        collectionView?.selectItem(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        
        delegate?.serviceDidChoseBowl(self, chosenBowl: bowls[index])
    }
    
    func currentBowl() -> String? {
        guard let indexPath = collectionView?.indexPathsForSelectedItems?.first else { return nil }
        return bowls[indexPath.row].name
    }
    
    
}

extension BowlCollectionViewService: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let size = CGSize(width: collectionView.frame.width/2 - 30, height: collectionView.frame.height/1.1 - 20)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let size = CGSize(width: collectionView.frame.width/2 - 20, height: collectionView.frame.height/1.1 - 20)
        
        guard bowls.count == 1 else {
            return UIEdgeInsets(top: 20, left: Constants.inset, bottom: 0, right: Constants.inset) }
        
        let totalCellWidth = size.width * CGFloat(collectionView.numberOfItems(inSection: 0)) - Constants.spaceBetweenCells * CGFloat(bowls.count)
        
        let totalSpacingWidth = Constants.spaceBetweenCells * CGFloat(bowls.count - 1)
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
}

extension BowlCollectionViewService {
    
    struct Constants {
        
        static let cellSize = CGSize(width: UIScreen.main.bounds.size.width/2 - 10, height: UIScreen.main.bounds.size.height/3)
        static let spaceBetweenCells = CGFloat(20)
        static let inset = CGFloat(20)
        
    }
    
}
