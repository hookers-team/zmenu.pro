//
//  RestaurantsCollectionViewService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 01.11.2018.
//  Copyright © 2018 Kirill Sokolov. All rights reserved.
//

import Foundation
import UIKit

protocol RestaurantsCollectionViewServiceDelegate: class {
    
    func restaurantsCollectionViewService(_ service: RestaurantsCollectionViewService, didChoseWorkPlace workPlace: UserInfo.WorkPlace)
    func restaurantsCollectionViewService(_ service: RestaurantsCollectionViewService, didTapWorkPlaceInfoButton: UserInfo.WorkPlace)
    
}

final class RestaurantsCollectionViewService: NSObject  {
    
    var workPlaces: [UserInfo.WorkPlace] = []
    var workDay: UserInfo.WorkDay?
    
    private weak var mastersCollectionView: UICollectionView?
    private weak var delegate: RestaurantsCollectionViewServiceDelegate?
    private let styleguide: DesignStyleGuide
    
    init(collectionView: UICollectionView, styleguide: DesignStyleGuide) {
       mastersCollectionView = collectionView
        self.styleguide = styleguide
    }
    
    func configurate(with delegate: RestaurantsCollectionViewServiceDelegate) {
       mastersCollectionView?.delegate = self
       mastersCollectionView?.dataSource = self
       mastersCollectionView?.allowsSelection = true
       mastersCollectionView?.registerReusableCell(cellType: RestaurantCollectionViewCell.self)
        
        self.delegate = delegate
       
    }
    
    func updateRestaurantRoles(restaurantsRoles: [UserInfo.WorkPlace], workDay: UserInfo.WorkDay?) {
        self.workPlaces = restaurantsRoles
        self.workDay = workDay
       mastersCollectionView?.reloadData()
    }
    
}

extension RestaurantsCollectionViewService: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return workPlaces.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(indexPath, cellType: RestaurantCollectionViewCell.self)
        
        let workPlace = workPlaces[indexPath.row]
        let restaurant = workPlace.restaurant
        
        cell.nameLabel.text = restaurant.name
        
        if let rating = restaurant.rating {
            cell.likeCountLabel.text = rating + " ⭐️"
        } else {
            cell.likeCountLabel.text = nil
        }
        
        cell.distanceLabel.text = UserRole.displayableRole(workPlace.role)
        cell.set(photos: restaurant.photos ?? [])
        cell.refreshUI(withStyleguide: styleguide)
        
        if let workDay = workDay?.workPlace, workPlace.role == workDay.role, workDay.restaurant.restaurantId == workPlace.restaurant.restaurantId {
             cell.distanceLabel.textColor = styleguide.accentTextColor
        }
       
        
        cell.delegate = self
        
        return cell
    }
    
}

extension RestaurantsCollectionViewService: RestaurantCollectionViewCellDelegate {
    
    func didTapRestaurantInfoButton(on cell: UICollectionViewCell) {
        guard let indexPath = mastersCollectionView?.indexPath(for: cell) else { return }
        
        delegate?.restaurantsCollectionViewService(self, didChoseWorkPlace:  workPlaces[indexPath.row])
    }
    
    func didSelectRestautant(cell: UICollectionViewCell) {
        guard let indexPath = mastersCollectionView?.indexPath(for: cell) else { return }
        
        delegate?.restaurantsCollectionViewService(self, didChoseWorkPlace:  workPlaces[indexPath.row])
        
       mastersCollectionView?.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    
}

extension RestaurantsCollectionViewService: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return Constants.cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      
        guard workPlaces.count == 1 else {
            return UIEdgeInsets(top: Constants.inset, left: Constants.inset, bottom: Constants.inset, right: Constants.inset) }
        
        let totalCellWidth = Constants.cellSize.width * CGFloat(collectionView.numberOfItems(inSection: 0)) - Constants.spaceBetweenCells * CGFloat(workPlaces.count)
        
        let totalSpacingWidth = Constants.spaceBetweenCells * CGFloat(workPlaces.count - 1)
        
        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: Constants.inset, left: leftInset, bottom: Constants.inset, right: rightInset)
    }
    
}

extension RestaurantsCollectionViewService {
    
    struct Constants {
        
        static let cellSize = CGSize(width: UIScreen.main.bounds.size.width - inset * 2, height: UIScreen.main.bounds.size.height/4.5)
        static let spaceBetweenCells = CGFloat(10)
        static let inset = CGFloat(10)
        
    }
    
}
