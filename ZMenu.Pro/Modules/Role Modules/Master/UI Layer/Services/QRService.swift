//
//  QRService.swift
//  ZMenu.Pro
//
//  Created by Chelak Stas on 1/29/19.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

fileprivate let userKey = "userId"
fileprivate let bundleKey = "bundle"
fileprivate let secureIdentifier = "ZMenu.ProQRServiceSecureIdentifier"

final class QRService {
    
    enum Role: String {
        case admin = "admin"
        case master = "master"
    }
    
    private let role: Role
    private let restaurantId: String
    private let employeeStore: EmployeeStore
    
    init(role: Role, employeeStore: EmployeeStore, restaurantId: String) {
        self.role = role
        self.restaurantId = restaurantId
        self.employeeStore = employeeStore
    }
    
    func makeViewTitle() -> String {
        return role == .admin ? "Добавление администратора".localized() : "Добавление кальянщика".localized()
    }
    
    func addEmployeeWith(employeeUserId: String) {
        employeeStore.addEmployee(employeeUserId: employeeUserId, role: role, restaurantId: restaurantId)
    }
    
    func decode(dataString: String) -> String? {
        let data = dataString.data(using: .utf8) ?? Data()
        guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: String],
            json?[bundleKey] == secureIdentifier, let userId = json?[userKey] else { return nil }
        
        let regex = "^[0-9]\\d*$"
        if userId.count > 0, NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: userId) {
            return userId
        }
        
        return nil
    }
    
}

extension QRService {
    
    class func makeQRImageWith(userId: String, size: CGSize) -> UIImage? {
        let json = [
            userKey: userId,
            bundleKey: secureIdentifier
        ]
        guard let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted) else { return nil }
        let string = String(data: data, encoding: .utf8) ?? ""
        
        return makeImageFrom(string: string, withSize: size)
    }
    
    private class func makeImageFrom(string: String, withSize size: CGSize) -> UIImage? {
        let data = string.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        
        guard let image = filter?.outputImage else { return nil }
        
        let scaleX = size.width / image.extent.size.width
        let scaleY = size.height / image.extent.size.height
        let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)
        
        return UIImage(ciImage: image.transformed(by: transform))
    }
    
}
