//
//  ProfileNetworkResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 20.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct ProfileNetworkResponse: Decodable {
    
    struct Data: Decodable {
        
        let user: UserInfo
        
    }
    
    let data: Data
    
}
