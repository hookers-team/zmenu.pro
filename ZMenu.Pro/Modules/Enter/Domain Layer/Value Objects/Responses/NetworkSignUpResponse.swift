//
//  NetworkSignUpResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 11.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct NetworkSignUpResponse: Decodable {
    
    struct Data: Decodable {
        
         let expires: String
        
    }
    
    let action: String
    let reqId: String
    let data: Data
    
}
