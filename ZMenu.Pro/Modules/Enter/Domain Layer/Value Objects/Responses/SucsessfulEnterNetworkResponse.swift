//
//  SucsessfulSignUpNetworkResponse.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 11.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct SucsessfulEnterNetworkResponse: Decodable {
    
    struct Data: Decodable {
        let user: SessionModel
    }
    
    let reqId: String
    let action: String
    let data: Data
    
}
