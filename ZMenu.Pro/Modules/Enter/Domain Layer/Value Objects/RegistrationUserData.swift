//
//  RegistrationUserData.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 11.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct RegistrationUserData {
    
    let name: String
    let phone: String
    let password: String
    
}
