//
//  EnterStore.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 07.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct EnterStoreStateChange: OptionSet, DataStateChange {
    
    let rawValue: Int
    
    static let isLoadingState = EnterStoreStateChange(rawValue: Int.bit1)
    static let enter = EnterStoreStateChange(rawValue: Int.bit2)
    static let signUp = EnterStoreStateChange(rawValue: Int.bit3)
    static let otp = EnterStoreStateChange(rawValue: Int.bit4)
    
}

enum EnterStoreStateChangeWithData: DataStateChange {
    
    case sucessEnter(accountModel: SessionModel)
    
}

final class EnterStore: DomainModel {
    
    private(set) var isLoading = false
    
    let networkService: EnterNetwork
    let dispatcher: Dispatcher
    
    var sessionModel: SessionModel?
    
    init(networkService: EnterNetwork, dispatcher: Dispatcher) {
        self.networkService = networkService
        self.dispatcher = dispatcher
    }
    
}

extension EnterStore {
    
    func logIn(phoneNumber: String, password: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.logIn(phoneNumber: phoneNumber, password: password, with: { (networkErrorResponse, sucsessfulEnterResponse) in
                
                if let networkSignUpResponse = sucsessfulEnterResponse {
                    self?.changeDataStateOf(EnterStoreStateChangeWithData.sucessEnter(accountModel: networkSignUpResponse.data.user)) {
                        self?.changeDataStateOf(EnterStoreStateChange.isLoadingState, work: {
                            self?.isLoading = false
                        })
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
    }
    
    func signUp(phone: String, name: String, password: String, userType: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.signUp(name: name, userType: userType, password: password, phoneNumber: phone, with: { (networkErrorResponse, networkSignUpResponse) in
                if networkSignUpResponse?.data != nil {
                    self?.changeDataStateOf([.isLoadingState, .otp] as EnterStoreStateChange) {
                        self?.isLoading = false
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
    }
    
    func checkAuth(code: String, phone: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.check(code: code, for: phone, completion: { (networkErrorResponse, sucsessfulEnterResponse) in
                
                if let response = sucsessfulEnterResponse {
                    self?.changeDataStateOf(EnterStoreStateChangeWithData.sucessEnter(accountModel: response.data.user)) {
                        self?.changeDataStateOf(EnterStoreStateChange.isLoadingState, work: {
                            self?.isLoading = false
                        })
                    }
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
    }
    
    func resendAuthCode(phoneNumber: String) {
        startLoading()
        
        taskDispatcher.dispatch { [weak self] in
            self?.networkService.resendCode(to: phoneNumber, completion: { (networkErrorResponse, resendCodeResponse) in
                if resendCodeResponse != nil {
                    self?.changeDataStateOf([.isLoadingState] as EnterStoreStateChange, work: {
                        self?.isLoading = false
                    })
                } else {
                    self?.showErrorMessage(networkErrorResponse?.serverError?.errorText)
                }
            })
        }
    }
    
        
}

extension EnterStore {
    
    func startLoading() {
        changeDataStateOf(EnterStoreStateChange.isLoadingState) {
            isLoading = true
        }
    }
    
    func showSuccsessMessage(_ message: String) {
        let value = AlertEvent.ShowSuccessAlert.Value(message: message)
        
        dispatcher.dispatch(type: AlertEvent.ShowSuccessAlert.self, result: Result(value: value))
    }
    
    func showErrorMessage(_ message: String? = nil) {
        changeDataStateOf(EnterStoreStateChange.isLoadingState) {
            isLoading = false
            
            let value = AlertEvent.ShowErrorAlert.Value(message: message ?? LocalizedAlertStringConstants.defaultErrorMessage)
            
            dispatcher.dispatch(type: AlertEvent.ShowErrorAlert.self, result: Result(value: value))
        }
    }
    
}

