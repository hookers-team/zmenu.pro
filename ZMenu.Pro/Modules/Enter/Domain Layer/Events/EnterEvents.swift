//
//  EnterEvents.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 07.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

struct EnterEvents {
    
    struct NavigationEvent {
        
        struct CloseScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenAuthorizationScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenRegistrationScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
        struct OpenOTPScreen: Event {
            typealias Payload = Value
            
            struct Value {
                let phoneNumber: String
            }
        }
        
        struct ShowPrivacyController: Event {
            typealias Payload = Value
            
            struct Value {
                let animated: Bool
            }
        }
        
    }
}

