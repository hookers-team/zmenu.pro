//
//  AuthorizationCoordinator.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 07.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation
import UIKit

protocol EnterCoordinatorDelegate: class {
    
    func enterCoordintator(_ enterCoordinator: EnterCoordinator, didFinishAccountAuthentification accountModel: SessionModel)
    
}

final class EnterCoordinator: TabBarEmbedCoordinator {
    
    fileprivate var enterStore: EnterStore! {
        didSet {
            enterStore.addDataStateListener(self)
        }
    }
    
    fileprivate var dispatcher: Dispatcher {
        return context.dispatcher
    }
    
    weak var delegate: EnterCoordinatorDelegate?
    
    fileprivate var root: UINavigationController!
    
    init(context: CoordinatingContext) {
        let homeImage = UIImage(named: "home")
        let homeImageActive = UIImage(named: "home_active")
        let tabItemInfo = TabBarItemInfo(
            title: nil,
            icon: homeImage,
            highlightedIcon: homeImageActive)
        
        super.init(context: context, tabItemInfo: tabItemInfo)
    }
    
    override func prepareForStart() {
        super.prepareForStart()
        
        register()
        
        let enterNetworkService = EnterNetworkService(networkService: context.networkService)
        enterStore = EnterStore(networkService: enterNetworkService, dispatcher: context.dispatcher)
        openEnterViewController()
    }
    
    override func createFlow() -> UIViewController {
        return root
    }
    
}

extension EnterCoordinator {
    
    private func openEnterViewController() {
        let enterViewController = UIStoryboard.Enter.enterViewController
        
        enterViewController.styleguide = context.styleguide
        enterViewController.dispatcher = context.dispatcher
        
        root = UINavigationController(rootViewController: enterViewController)
    }
    
    private func openRegistrationViewController(animated: Bool) {
        let controller = UIStoryboard.Enter.registrationViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.enterStore = enterStore
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openAuthorizathionViewController(animated: Bool) {
        let controller = UIStoryboard.Enter.authorizationViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.enterStore = enterStore
        
        root.pushViewController(controller, animated: animated)
    }
    
    private func openOTPViewController(withPhoneNumber phoneNumber: String) {
        let controller = UIStoryboard.Enter.verificationCodeViewController
        
        controller.dispatcher = context.dispatcher
        controller.styleguide = context.styleguide
        controller.enterStore = enterStore
        controller.phoneNumber = phoneNumber
        
        root.tabBarController?.tabBar.isHidden = true
        root.pushViewController(controller, animated: true)
    }
    
    private func openPrivacyViewController(animated: Bool) {
        let controller = UIStoryboard.Enter.privacyViewController
        
        controller.dispatcher = dispatcher
        controller.styleguide = context.styleguide
        
        let nav = UINavigationController(rootViewController: controller)
        
        root.present(nav, animated: true, completion: nil)
    }
    
}

extension EnterCoordinator: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EnterStoreStateChangeWithData {
            enterStoreStateChangeWithData(change: change)
        }
    }
    
    private func enterStoreStateChangeWithData(change: EnterStoreStateChangeWithData) {
        switch change {
        case .sucessEnter(accountModel: let accountModel):
            delegate?.enterCoordintator(self, didFinishAccountAuthentification: accountModel)
        }
    }
    
}

extension EnterCoordinator {
    
    private func register() {
        registerCloseScreen()
        registerOpenRegistrationScreen()
        registerOpenAuthorizationScreen()
        registerOpenOTPScreen()
        registerShowPrivacy()
    }
    
    private func registerCloseScreen() {
        dispatcher.register(type: EnterEvents.NavigationEvent.CloseScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                if self?.root.presentedViewController != nil {
                    self?.root.dismiss(animated: box.animated, completion: nil)
                } else {
                    self?.root.popViewController(animated: true)
                }
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenAuthorizationScreen() {
        dispatcher.register(type: EnterEvents.NavigationEvent.OpenAuthorizationScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openAuthorizathionViewController(animated: box.animated)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenOTPScreen() {
        dispatcher.register(type: EnterEvents.NavigationEvent.OpenOTPScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
                self?.openOTPViewController(withPhoneNumber: box.phoneNumber)
            case .failure(_):
                break
            }
        }
    }
    
    private func registerShowPrivacy() {
        dispatcher.register(type: EnterEvents.NavigationEvent.ShowPrivacyController.self) { [weak self] (result, _) in
            switch result {
            case .success(let box):
                self?.openPrivacyViewController(animated: box.animated)
                
            case .failure(_):
                break
            }
        }
    }
    
    private func registerOpenRegistrationScreen() {
        dispatcher.register(type: EnterEvents.NavigationEvent.OpenRegistrationScreen.self) { [weak self] result, _ in
            switch result {
            case .success(let box):
               self?.openRegistrationViewController(animated: box.animated)
            case .failure(_):
                break
            }
        }
    }
    
}
