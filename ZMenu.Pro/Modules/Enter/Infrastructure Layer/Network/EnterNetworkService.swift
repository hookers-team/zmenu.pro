//
//  EnterNetworkService.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 08.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

protocol EnterNetwork {
    
    func signUp(name: String, userType: String, password: String, phoneNumber: String, with completion: @escaping ((NetworkErrorResponse?, NetworkSignUpResponse?) -> Void))
    func logIn(phoneNumber: String,  password: String, with completion: @escaping ((NetworkErrorResponse?, SucsessfulEnterNetworkResponse?) -> Void))
    func check(code: String, for phone: String, completion: @escaping ((NetworkErrorResponse?, SucsessfulEnterNetworkResponse?) -> Void))
    func resendCode(to phone: String, completion: @escaping ((NetworkErrorResponse?, ResendVerificationCodeNetworkResponse?) -> Void))
    
}

final class EnterNetworkService: BaseNetworkService, EnterNetwork {

    let networkService: HTTPNetworkService
    
    init(networkService: HTTPNetworkService) {
        self.networkService = networkService
    }

    func check(code: String, for phone: String, completion: @escaping ((NetworkErrorResponse?, SucsessfulEnterNetworkResponse?) -> Void)) {
        let parameters = [
            "phone": phone,
            "confirmationCode": code
            ] as Parameters
        
        networkService.executeRequest(endpoint: "checkConfirmationCodeBusiness", method: .post, parameters: parameters, headers: nil, completion: completion)
    }
    
    func resendCode(to phone: String, completion: @escaping ((NetworkErrorResponse?, ResendVerificationCodeNetworkResponse?) -> Void)) {
        let parameters = [
            "phone": phone
            ] as Parameters
        
        networkService.executeRequest(endpoint: "resendConfirmationCode", method: .post, parameters: parameters, headers: nil, completion: completion)
    }
    
    func signUp(name: String, userType: String, password: String, phoneNumber: String, with completion: @escaping ((NetworkErrorResponse?, NetworkSignUpResponse?) -> Void)) {
        let parametrs = ["name" : name,
                         "phone" : phoneNumber,
                         "type" : userType,
                         "password" : password]
        
        networkService.executeRequest(endpoint: "registration", method: .post, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func logIn(phoneNumber: String, password: String, with completion: @escaping ((NetworkErrorResponse?, SucsessfulEnterNetworkResponse?) -> Void)) {
        let parametrs = ["phone" : phoneNumber,
                         "password" : password]
        
        networkService.executeRequest(endpoint: "auth", method: .post, parameters: parametrs, headers: nil, completion: completion)
    }
    
    func checkOTP(code: String) {
        
    }
    
}

