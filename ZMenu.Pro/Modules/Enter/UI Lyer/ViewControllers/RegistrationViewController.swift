//
//  RegistrationViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 07.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

final class RegistrationViewController: UIViewController {

    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordAgainTextField: UITextField!
    @IBOutlet weak var userTypePicker: UIPickerView!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    
    @IBOutlet weak var privacyTextView: UITextView!
    @IBOutlet weak var privacyButton: UIButton!
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var enterStore: EnterStore! {
        didSet {
            enterStore.addDataStateListener(self)
        }
    }
    
    private var phoneNumber: String {
        return "380" + (phoneNumberTextField.text ?? "").components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    let pickerData: [MixCategorizedData] = [("Выберите тип аккаунта".localized(), "unpicked"),
                                         ("Заведение или Владелец".localized(), AccountType.owner.rawValue),
                                         ("Кальянщик или Администратор".localized(), AccountType.employee.rawValue)]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userTypePicker.delegate = self
        userTypePicker.dataSource = self
        
        configurateNavigationBar()
        hideKeyboardWhenTappedAround()
        refreshUI(withStyleguide: styleguide)
        
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(registration), for: .touchUpInside)
        ctaButtonContainerView.ctaButton.setTitle("Зарегистрироваться".localized(), for: .normal)
    }

    func configurateNavigationBar() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Привет, давай познакомимся?".localized(),
                                    subtitle: "Заполни поля и следуй инструкциям".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    
    @objc func back() {
        let value = EnterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: EnterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    @IBAction func showPrivacy(_ sender: Any) {
        let value = EnterEvents.NavigationEvent.ShowPrivacyController.Value(animated: true)
        dispatcher.dispatch(type: EnterEvents.NavigationEvent.ShowPrivacyController.self, result: Result(value: value))
    }
    
    @objc func registration() {
        if validatePhoneTextField() &&
            validateIsEmtyTextField(passwordTextField, errorMessage: "Поле пароль не может быть пустым".localized()) &&
            validateIsEmtyTextField(passwordAgainTextField, errorMessage: "Поле подтверждение пароля не может быть пустым".localized()) &&
            validateIsEmtyTextField(passwordAgainTextField, errorMessage: "Поле подтверждение пароля не может быть пустым".localized()) &&
            validatePasswords() &&
            validateUserTypePicker() {
            enterStore.signUp(phone: phoneNumber, name: "", password: passwordTextField.text ?? "", userType: pickerData[userTypePicker.selectedRow(inComponent: 0)].categoryId)
        }
    }
    
    private func validatePasswords() -> Bool {
        let isEquel = passwordAgainTextField.text == passwordTextField.text
        
        if !isEquel {
            showAlertWithMessage("Пароли не совпадают".localized())
        }
        
        let regex = "^.\\S{3,30}$"
        let isValid = NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: passwordTextField.text ?? "")
        
        if !isValid {
            showAlertWithMessage("Длинна пароля должна быть от 4-30 символов и без специфических знаков".localized())
        }
        
        return isEquel && isValid
    }
    
    private func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    private func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }
    
    private func validatePhoneTextField() -> Bool {
        let regex = "^380(\\d{9})$"
        let isValid = NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: phoneNumber)
        
        if !isValid {
            showAlertWithMessage("Некорректный номер телефона".localized())
        }
        
        return isValid
    }
    
    private func validateUserTypePicker() -> Bool {
        let isValid = userTypePicker.selectedRow(inComponent: 0) != 0
        
        if !isValid {
            showAlertWithMessage("Выберите тип аккаунта".localized())
        }
        
        return isValid
    }
    
    private func validateIsEmtyTextField(_ textField: UITextField, errorMessage: String) -> Bool {
        let isValid = !(textField.text ?? "").isEmpty
        
        if !isValid {
            showAlertWithMessage(errorMessage)
        }
        
        return isValid
    }
    
    private func showAlertWithMessage(_ message: String) {
        presentError(withTitle: "Без паники 👻".localized(), subtitle: message)
    }
    
}

extension RegistrationViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EnterStoreStateChange {
            enterStoreStateChange(change: change)
        }
    }
    
    private func enterStoreStateChange(change: EnterStoreStateChange) {
        if change.contains(.isLoadingState) {
            enterStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }
        
        if change.contains(.otp) {
            let value = EnterEvents.NavigationEvent.OpenOTPScreen.Value(phoneNumber: phoneNumber)
            
            dispatcher.dispatch(type: EnterEvents.NavigationEvent.OpenOTPScreen.self, result: Result(value: value))
        }
    }
    
}

extension RegistrationViewController: UIStyleGuideRefreshing {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        ctaButtonContainerView.ctaButton.layer.borderWidth = 1
        ctaButtonContainerView.ctaButton.layer.borderColor = UIColor.white.cgColor
        
        phoneNumberTextField.attributedPlaceholder = NSAttributedString(string: "(00) 00 00 000".localized(),
                                                                        attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Введите пароль".localized(),
                                                                     attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])
        
        passwordAgainTextField.attributedPlaceholder = NSAttributedString(string: "Подтвердите пароль еще раз".localized(),
                                                                     attributes: [NSAttributedString.Key.foregroundColor: styleguide.senderTextColor])

        userTypePicker.tintColor = .white
        
        let privacyText = "политикой конфиденциальности".localized()
        let text = "Нажимая \"Зарегистрироваться\" Вы соглашаетесь с нашей".localized() + " " + privacyText
        
        let attributedString = NSMutableAttributedString(string: text)
        
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: styleguide.primaryTextColor,
                                        NSAttributedString.Key.font: styleguide.regularFont(ofSize: 14)],
                                       range: attributedString.mutableString.range(of: text))
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: styleguide.accentTextColor,
                                        NSAttributedString.Key.font: styleguide.regularFont(ofSize: 14)],
                                       range: attributedString.mutableString.range(of: privacyText))
        
        privacyTextView.attributedText = attributedString
    }
    
}

extension RegistrationViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTextField {
            guard textField.text?.count ?? 0 < 14 || string.isBackSpace() else { return false }
            
            textField.text = (textField.text ?? "").formattedPhoneNumber()
        }
        
        return true
    }
    
}

extension RegistrationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSAttributedString(string: pickerData[row].categoryName, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row].categoryName
    }
    
}
