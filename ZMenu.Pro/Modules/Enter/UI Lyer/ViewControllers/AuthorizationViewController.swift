//
//  AuthorizationViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 07.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

class AuthorizationViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var phoneNubmerTextField: UITextField!
    @IBOutlet weak var ctaButtonContainerView: CTAButtonContainerView!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    var enterStore: EnterStore! {
        didSet {
            enterStore.addDataStateListener(self)
        }
    }
    
    private var phoneNumber: String {
        return "380" + (phoneNubmerTextField.text ?? "").components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneNubmerTextField.delegate = self

        hideKeyboardWhenTappedAround()
        
        ctaButtonContainerView.ctaButton.addTarget(self, action: #selector(enter), for: .touchUpInside)
        ctaButtonContainerView.ctaButton.setTitle("Войти".localized(), for: .normal)
        
        refreshUI(withStyleguide: styleguide)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configurateNavigationBar()
    }
    
    @objc func enter() {
        if validatePhone() && validatePassword() {
            enterStore.logIn(phoneNumber: phoneNumber, password: passwordTextField.text ?? "")
        }
    }
    
    private func validatePassword() -> Bool {
        let regex = "^.\\S{3,30}$"
        let isValid = NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: passwordTextField.text ?? "")
        
        if !isValid {
            showAlertWithMessage("Длинна пароля должна быть от 4-30 символов и без специфических знаков".localized())
        }
        
        return isValid
    }
    
    private func validatePhone() -> Bool {
        let regex = "^380(\\d{9})$"
        let isValid = NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: phoneNumber)
        
        if !isValid {
            showAlertWithMessage("Номер телефона введен некорректно".localized())
        }
        
        return isValid
    }
    
    private func showAlertWithMessage(_ message: String) {
        presentError(withTitle: "Без паники 👻".localized(), subtitle: message)
    }
    
    func configurateNavigationBar() {
        navigationItem.addBackButton(with: self, action: #selector(back), tintColor: styleguide.primaryColor)
        
        navigationController?.navigationBar.barStyle = .default
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = false
        
        navigationItem.setTitleView(withTitle: "Привет, мы, кажется, знакомы?".localized(),
                                    subtitle: "Заходи и чувствуй себя как дома".localized(),
                                    titleColor: styleguide.primaryTextColor,
                                    titleFont: styleguide.regularFont(ofSize: 17),
                                    subtitleColor: styleguide.secondaryTextColor,
                                    subtitleFont: styleguide.regularFont(ofSize: 12))
    }
    
    
    @objc func back() {
        let value = EnterEvents.NavigationEvent.CloseScreen.Value(animated: true)
        
        dispatcher.dispatch(type: EnterEvents.NavigationEvent.CloseScreen.self, result: Result(value: value))
    }
    
    func startCTAButtonLoadingAnimation() {
        ctaButtonContainerView.startAnimation()
        view.isUserInteractionEnabled = false
    }
    
    func stopCTAButtonLoadingAnimation() {
        ctaButtonContainerView.stopAnimation()
        view.isUserInteractionEnabled = true
    }

}

extension AuthorizationViewController: DataStateListening {
    
    func domainModel(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        DispatchQueue.updateUI {
            self.domainModelChanged(model, didChangeDataStateOf: change)
        }
    }
    
    private func domainModelChanged(_ model: DomainModel, didChangeDataStateOf change: DataStateChange) {
        if let change = change as? EnterStoreStateChange {
            enterStoreStateChange(change: change)
        }
    }
    
    private func enterStoreStateChange(change: EnterStoreStateChange) {
        if change.contains(.isLoadingState) {
            enterStore.isLoading ? self.startCTAButtonLoadingAnimation() : self.stopCTAButtonLoadingAnimation()
        }
    }
    
}

extension AuthorizationViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneNubmerTextField {
            guard textField.text?.count ?? 0 < 14 || string.isBackSpace() else { return false }
            
            textField.text = (textField.text ?? "").formattedPhoneNumber()
        }
        
        return true
    }
    
}

extension AuthorizationViewController: UIStyleGuideRefreshing, CTAButtonConfiguring {
    
    func refreshUI(withStyleguide styleguide: DesignStyleGuide) {
        
        refreshEnterCTAButton(ctaButtonContainerView.ctaButton, withStyleguide: styleguide)
        
        ctaButtonContainerView.ctaButton.layer.borderWidth = 1
        ctaButtonContainerView.ctaButton.layer.borderColor = UIColor.white.cgColor
        
        view.backgroundColor = styleguide.backgroundScreenColor
        
        phoneNubmerTextField.attributedPlaceholder = NSAttributedString(string: "(00) 00 00 000".localized(),
                                                                        attributes: [NSAttributedString.Key.foregroundColor: styleguide.secondaryTextColor])
        
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Пароль".localized(),
                                                                     attributes: [NSAttributedString.Key.foregroundColor: styleguide.secondaryTextColor])
    }
    
}
