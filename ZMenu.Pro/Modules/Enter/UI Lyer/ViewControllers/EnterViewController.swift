//
//  EnterViewController.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 07.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import UIKit

class EnterViewController: UIViewController {

    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var registrationBlurView: UIVisualEffectView!
    @IBOutlet weak var enterBlurView: UIVisualEffectView!
    @IBOutlet weak var globalBlurView: UIVisualEffectView!
    @IBOutlet weak var registrationButton: UIButton!
    
    var styleguide: DesignStyleGuide!
    var dispatcher: Dispatcher!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        enterBlurView.layer.cornerRadius = enterButton.frame.height / 2
        enterButton.layer.cornerRadius = enterButton.frame.height / 2
        
        registrationBlurView.layer.cornerRadius = registrationButton.frame.height / 2
        registrationButton.layer.cornerRadius = registrationButton.frame.height / 2
        registrationBlurView.layer.borderColor = UIColor.white.cgColor
        registrationBlurView.layer.borderWidth = 1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configurateNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1.5, delay: 0, options: .curveEaseOut, animations: {
            self.globalBlurView.effect = UIBlurEffect(style: .dark)
        }, completion: nil)
    }
    
    
    
    func configurateNavigationBar() {
        navigationController?.navigationBar.barStyle = .blackTranslucent
        navigationController?.navigationBar.barTintColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func enter(_ sender: Any) {
        let value = EnterEvents.NavigationEvent.OpenAuthorizationScreen.Value(animated: true)
        
        dispatcher.dispatch(type: EnterEvents.NavigationEvent.OpenAuthorizationScreen.self, result: Result(value: value))
    }
    
    @IBAction func registration(_ sender: Any) {
        let value = EnterEvents.NavigationEvent.OpenRegistrationScreen.Value(animated: true)
        
        dispatcher.dispatch(type: EnterEvents.NavigationEvent.OpenRegistrationScreen.self, result: Result(value: value))
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
