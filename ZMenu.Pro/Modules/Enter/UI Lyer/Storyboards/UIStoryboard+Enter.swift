//
//  UIStoryboard+Authorization.swift
//  ZMenu.Pro
//
//  Created by Kirill Sokolov on 07.01.2019.
//  Copyright © 2019 Skaix. All rights reserved.
//

import Foundation

import UIKit

fileprivate enum EnterStoryboardControllerId: String {
    
    case authorization = "AuthorizationViewControllerStoryboarId"
    case enter = "EnterViewControllerStoryboarId"
    case registration = "RegistrationViewControllerStoryboarId"
    case verificationCode = "VerificationCodeViewController"
    case privacy = "PrivacyViewControllerStoryboardId"
    
}

extension UIStoryboard {
    
    static var enterStoryboard: UIStoryboard {
        return UIStoryboard(name: "Enter", bundle: nil)
    }
    
    struct Enter {
        
        static var enterViewController: EnterViewController {
            return UIStoryboard.enterStoryboard.instantiateViewController(withIdentifier: EnterStoryboardControllerId.enter.rawValue) as! EnterViewController
        }
        
        static var registrationViewController: RegistrationViewController {
            return UIStoryboard.enterStoryboard.instantiateViewController(withIdentifier: EnterStoryboardControllerId.registration.rawValue) as! RegistrationViewController
        }
        
        static var authorizationViewController: AuthorizationViewController {
            return UIStoryboard.enterStoryboard.instantiateViewController(withIdentifier: EnterStoryboardControllerId.authorization.rawValue) as! AuthorizationViewController
        }
        
        static var verificationCodeViewController: VerificationCodeViewController {
            return UIStoryboard.enterStoryboard.instantiateViewController(withIdentifier: EnterStoryboardControllerId.verificationCode.rawValue) as! VerificationCodeViewController
        }
        
        static var privacyViewController: PrivacyViewController {
            return UIStoryboard.enterStoryboard.instantiateViewController(withIdentifier: EnterStoryboardControllerId.privacy.rawValue) as! PrivacyViewController
        }
        
    }
    
}
